package wehyphens.com.satyaconnect.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;


public class SmsReceiverM extends BroadcastReceiver {

    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();
//        Log.e("message","recieved");

        Object[] pdus = (Object[]) data.get("pdus");

        if(pdus==null){
            return;
        }
        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String update,updateOne,wholeString;
            String sender = smsMessage.getDisplayOriginatingAddress();
            String[] updateString = sender.split("[\\s@&.?$+-]+");
             update=updateString[0];
             updateOne=updateString[1];
             wholeString=update+updateOne;

             Log.e("otpReceviver","otpReceviver"+wholeString);

            if(sender.trim().contains("HPSATYAA")){
                String messageBody = smsMessage.getMessageBody();
                if(mListener!=null){
                    mListener.messageReceived(filterMsg(messageBody));
                }
            }
            else   if(sender.trim().contains("HP-SATYAA")){
                String messageBody = smsMessage.getMessageBody();
                if(mListener!=null){
                    mListener.messageReceived(filterMsg(messageBody));
                }
            }else if(sender.trim().contains("IG-SATYAA")){
                String messageBody = smsMessage.getMessageBody();
                if(mListener!=null){
                    mListener.messageReceived(filterMsg(messageBody));
                }
            }
        }

    }

    public interface SmsListener{
        void messageReceived(String msg);
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

    public String filterMsg(String msg) {
        msg=msg.replaceAll("\\D+"," ");
        String[] arr=msg.split(" ");

        String otp="";
        for(String str:arr){
            if(str.trim().length()==6){
                otp=str.trim();
            }
        }

        return otp;
    }
}
