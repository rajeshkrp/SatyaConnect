package wehyphens.com.satyaconnect.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import wehyphens.com.satyaconnect.Interface.IConstants;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.models.ChatMessageDto;


public class NotifiactionReciever extends BroadcastReceiver implements IConstants.IFcm, IConstants.INotification {

    ExecutorService mExecutor;
    private boolean isDBUpdate;
    private Flowable<List<ChatMessageDto>> chat;
    private List<ChatMessageDto> chatList = new ArrayList<>();
    private boolean isMsgRead;


    @Override
    public void onReceive(Context context, Intent intent) {
        /*here we get the json we send form the user side*/
        String msg = intent.getStringExtra(IConstants.IApp.PARAM_1);
        //Toast.makeText(context, "notification", Toast.LENGTH_SHORT).show();
        String reply = intent.getStringExtra(IConstants.IApp.PARAM_2);
        if (reply != null && !reply.trim().equals("")) {
            sendNotification(context, reply, IConstants.IFcm.FCM_REPLY);
            return;
        }



        handleMsg(context, msg);


}

    private void handleMsg(Context context, String msg) {
        if (msg != null) {
            ChatMessageDto chat = new Gson().fromJson(msg, ChatMessageDto.class);

            handleChatMessage(context, chat);

        }
    }


    /****************
     * Handling Type of Messages
     * ******************/
    private void handleChatMessage(Context context, ChatMessageDto chat) {


        String notMsg = "";
        ChatMessageDto cloned = chat.clone();
        if (chatList.size() > 0) {
            for (ChatMessageDto dto : chatList) {
                notMsg = dto.getToName() + " : " + dto.getMsg() + "\n" + notMsg;
            }
            notMsg += chat.getMyName() + " : " + chat.getMsg();

            cloned.setMsg(notMsg);
        }

        if (AppUtils.isAppRunning(context)) {
            Log.e("package", AppUtils.isRunning(context) + "\n" + ChatActivity.class.getSimpleName());
            if (!AppUtils.isRunning(context).contains(ChatActivity.class.getSimpleName())) {
                sendNotification(context, new Gson().toJson(cloned), chat.getMsgType());
            }
        } else {
            sendNotification(context, new Gson().toJson(cloned), chat.getMsgType());
        }
    }













    /****************
     * Handling Type of Messages
     * ******************/


    /*****************************
     * showing notification on message recieced
     * **************************/
    private void sendNotification(Context context, String remoteMessage, int type) {


        int count = type;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        NotificationDto dto = new NotificationDto();
        dto.setType(type);
        dto.setMsg(remoteMessage);
        dto.setDate(formattedDate);
        dto.setReadStatus(0);
        dto.setId(0);

        AppUtils.showNotification2(context, dto, count);
    }


}
