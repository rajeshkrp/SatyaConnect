package wehyphens.com.satyaconnect.reciever;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;
import wehyphens.com.satyaconnect.Interface.IConstants;
import wehyphens.com.satyaconnect.MainActivity;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;


/**
 * Created by abul on 28/10/17.
 */

public class AppUtils implements IConstants.IFcm {

    public static String NOTIF_MSG = "HideAll has new msg";
    public static final String NOTIF_MSG_HEADEING = "HideAll";

    /*****************************
     * Notification
     * ****************************/





    public void createNotify(Context context, NotificationDto dto, int count) {
        Intent activityIntent = new Intent(context, ChatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(context)
                .setContentTitle("SatyaConnect")
                .setContentText(dto.getMsg())
                .setContentIntent(pendingIntent)
                .setVibrate(new long[] { 1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true);
        notificationBuilder.setSmallIcon(R.drawable.logo);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(5);
        notificationManager.notify(5, notificationBuilder.build());
    }





















    public static void showNotification2(Context context, NotificationDto dto, int count) {




        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
       // Notification not=buildNotification(context, dto, count).build();

       Notification not=buildNotification(context, dto, count).build();



       // NotificationCompat.Builder notificationManager = new NotificationCompat.Builder(context);



        if(not==null){
            return;
        }
        notificationManager.notify(count,not);
    }


    /*************
     * custom
     * ***************/
    protected static NotificationCompat.Builder buildNotification(Context context, NotificationDto dto, int count) {


        // Open NotificationView.java Activity
        Intent intent = null;

      return null;
    }

    public static void saveImage(Context context, Bitmap bitmap, String name) {
        File outputDir = new File(Environment.getExternalStorageDirectory() + "/abul"); // context being the Activity pointer
        File tempFile = null;
        try {
            tempFile = File.createTempFile(name, ".jpg", outputDir);
            FileOutputStream outputStream = new FileOutputStream(tempFile);
            boolean compress = bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
            Toast.makeText(context, "no error", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }


    public static boolean isAppRunning(final Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(context.getApplicationContext().getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }


    public static String isRunning(Context ctx) {
        ActivityManager mActivityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);

        return mActivityManager.getRunningTasks(1).get(0).topActivity.getClassName();

    }


}
