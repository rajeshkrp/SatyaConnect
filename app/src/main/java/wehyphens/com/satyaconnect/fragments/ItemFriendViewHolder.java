package wehyphens.com.satyaconnect.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;

class ItemFriendViewHolder extends RecyclerView.ViewHolder{
    public CircleImageView avata,userStatus;
    public TextView txtName, txtTime, txtMessage;
    private Context context;

    ItemFriendViewHolder(Context context, View itemView) {
        super(itemView);
        avata = (CircleImageView) itemView.findViewById(R.id.icon_avata);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
        userStatus = (CircleImageView) itemView.findViewById(R.id.userStatus);
        this.context = context;
    }
}

/**/