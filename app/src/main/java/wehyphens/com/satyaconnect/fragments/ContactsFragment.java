package wehyphens.com.satyaconnect.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.GroupDeatailsModel;
import wehyphens.com.satyaconnect.models.ListFriend;
import wehyphens.com.satyaconnect.models.RecentchatModel;

public class ContactsFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    List<RecentchatModel> arrayList;
    RecentchatModel model;
    RecentchatAdapter recentchatAdapter;
    TextView  tv_toolbar_title;
    private ListFriend dataListFriend = null;
    public static int ACTION_START_CHAT = 1;
    private ListFriendsAdapter adapter;
    public static final String ACTION_DELETE_FRIEND = "com.android.rivchat.DELETE_FRIEND";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_chats, container, false);
        recy_chat_list=view.findViewById(R.id.recy_chat_list);

        tv_toolbar_title=view.findViewById(R.id.tv_toolbar_title);

        context=getActivity();
        arrayList=new ArrayList<>();

        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);



        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("Rajesh Patel");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("Anushka sharma");
        arrayList.add(model);



        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


        model=new RecentchatModel();
        model.setUser_pic(R.drawable.james);
        model.setChattitle("hey! i am using satya too");
        model.setLastSeen("01:25 P.M");
        model.setUserName("James Andersion");
        arrayList.add(model);


       /* recentchatAdapter = new RecentchatAdapter(arrayList, context);
        recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
        recy_chat_list.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recy_chat_list.setAdapter(recentchatAdapter);*/

        tv_toolbar_title.setText("Contacts");






    /*    if (arrayList!= null) {

            if (arrayList.size() > 0) {

                for (ListFriend friend:arrayList) {
                    dataListFriend = friend;

                }

            }
        }
        if(dataListFriend!=null) {

            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recy_chat_list.setLayoutManager(linearLayoutManager);
            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            recy_chat_list.setAdapter(adapter);
        }*/


        return view;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }






}
