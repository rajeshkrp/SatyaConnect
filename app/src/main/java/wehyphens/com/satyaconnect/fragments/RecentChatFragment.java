package wehyphens.com.satyaconnect.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.GroupchatAdapter;
import wehyphens.com.satyaconnect.adapter.MsgForwordAdapter;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.models.DepartmentPojo;
import wehyphens.com.satyaconnect.models.Group;
import wehyphens.com.satyaconnect.models.GroupDeatailsModel;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.models.UserModelPoJo;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class RecentChatFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    RelativeLayout rl_root;
    ArrayList<RecentchatModel> arrayList;
    RecentchatModel model;
    RecentchatAdapter recentchatAdapter;
    EditText et_search;
    TextView tv_toolbar_title,tv_content;
    private String token="";
    private ImageView iv_profile_pic,ivProgress;
    List<DepartmentListDetailPojo> departmentList;
    List<DepartmentListDetailPojo> departmentSearchList;
    private  String  profile_pic="";
    BottomNavigationView bottomNavigationView;
    private String fireBaseKey="";
    private TextView  tvGroup;
    SharedPreferences sharedPreferences ;
    static SharedPreferences.Editor editor;
    boolean isSearch=false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_groups, container, false);
        context=getActivity();
        getToken();
        CommonUtils.hideKeyPad(getActivity());
        departmentList =new ArrayList<>();
        recy_chat_list=view.findViewById(R.id.recy_chat_list);
        tvGroup=view.findViewById(R.id.tvGroup);
      //  tvGroup.setText("Your Contact list will come here!");

        tv_toolbar_title=view.findViewById(R.id.tv_toolbar_title);
        tv_content=view.findViewById(R.id.tv_content);
        iv_profile_pic=view.findViewById(R.id.iv_profile_pic);
        et_search=view.findViewById(R.id.et_search);
        ivProgress=view.findViewById(R.id.ivProgress);
        Animation animation1 =
                AnimationUtils.loadAnimation(context, R.anim.move);
        ivProgress.startAnimation(animation1);
        tv_toolbar_title.setText("Contacts");
        rl_root=view.findViewById(R.id.rl_root);




        sharedPreferences=context.getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        bottomNavigationView=getActivity().findViewById(R.id.navigation);
      //  checkKeyBoardUp();
        profile_pic= CommonUtils.getPreferences(context, AppConstants.PROFILE_PIC);

        setupUI(rl_root);
        et_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (b==true){
                    et_search.setCursorVisible(true);}else {
                    et_search.setCursorVisible(false);
                }
            }
        });

        if(profile_pic!=null&&!TextUtils.isEmpty(profile_pic)){

            Picasso.with(context).load(profile_pic).error(R.drawable.green_user).fit().centerCrop()
                    .into(iv_profile_pic);

        }
        else {
            iv_profile_pic.setImageResource(R.drawable.green_user);
        }

        token= CommonUtils.getPreferences(context, AppConstants.LOGIN_AUTHENTICATE);


        String serializedObject = sharedPreferences.getString("DEPARTMENTDATA", null);
        if(serializedObject!=null){
            Gson gson1 = new Gson();
            Type type=new TypeToken<List<DepartmentListDetailPojo>>(){}.getType();
            departmentList=gson1.fromJson(serializedObject, type);
            String imageurl=CommonUtils.getPreferencesString(getActivity(),AppConstants.DEPARTMENT_IMAGE_URL);
            recentchatAdapter = new RecentchatAdapter(departmentList, context,imageurl);
            recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
            recy_chat_list.setAdapter(recentchatAdapter);

        }



        if (token != null && !token.equalsIgnoreCase("")){
            Log.e("DepartmentToken","DepartmentToken::"+token);
           // CommonUtils.showProgress(context);

            if(departmentList.size()==0){
                ivProgress.setVisibility(View.VISIBLE);
            }else {
                ivProgress.setVisibility(View.GONE);
            }

            if(CommonUtils.isInternetAvailable(getActivity()))
            {
                getDepartmentData(token);
            }else {
                CommonUtils.noInternetConnection(getActivity());
            }
        }

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                et_search.setCursorVisible(true);
                if (charSequence.toString().isEmpty()) {
                    isSearch = false;
                    setAdapterr(departmentList);
                    recentchatAdapter.notifyDataSetChanged();
                } else {
                    isSearch = true;
                    departmentSearchList = getSearchContactList(charSequence.toString());
                    setAdapterr(departmentSearchList);
                    recentchatAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
             //   callupdate();
                //after the change calling the method and passing the search input
               // filter(editable.toString());
            }
        });


        return view;
    }


    public List<DepartmentListDetailPojo> getSearchContactList(String charString) {
        List<DepartmentListDetailPojo> filteredContactList = new ArrayList<>();
        for (int i = 0; i <= departmentList.size() - 1; i++) {
            DepartmentListDetailPojo obj = new DepartmentListDetailPojo();
            obj = departmentList.get(i);
            if (departmentList.get(i).getUser().getFirstName() != null) {
                if (departmentList.get(i).getUser().getFirstName().toLowerCase().contains(charString.toLowerCase())) {
                    filteredContactList.add(obj);
                }
            }
        }
      //  searchContactList = filteredContactList;


        return filteredContactList;
    }

    public void setAdapterr(List<DepartmentListDetailPojo> deptList) {
        String imageurl = CommonUtils.getPreferencesString(getActivity(), AppConstants.DEPARTMENT_IMAGE_URL);
        recentchatAdapter = new RecentchatAdapter(deptList, context,imageurl);
        recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
        recy_chat_list.setHasFixedSize(true);
        recy_chat_list.setAdapter(recentchatAdapter);

    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {

                  //  hideSoftKeyboard(getActivity());
                    CommonUtils.hideKeyPad(getActivity());
                    // et_search.setCursorVisible(false);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);


            }
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(context, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(context, AppConstants.FIREBASE_KEY, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }
    public void checkKeyBoardUp(){
        rl_root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rl_root.getWindowVisibleDisplayFrame(r);
                int heightDiff = rl_root.getRootView().getHeight() - (r.bottom - r.top);

                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    //ok now we know the keyboard is up...
                    bottomNavigationView.setVisibility(View.INVISIBLE);

                }else{
                    //ok now we know the keyboard is down...
                    bottomNavigationView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void callupdate() {

        for (int i = 0; i < departmentList.size(); i++) {

            if(departmentList.get(i).getUser().getId()==2){
                Toast.makeText(context, "id::"+departmentList.get(i).getUser().getId(), Toast.LENGTH_SHORT).show();

              //  Toast.makeText(context, "position::"+i, Toast.LENGTH_SHORT).show();
                Log.e("latestMessageList", "latestMessageList::" + i);

                Log.e("latestMessageList", "ifIDDD::" + departmentList.get(i).getUser().getId());


                 DepartmentListDetailPojo  detailPojo=new DepartmentListDetailPojo();
                UserModelPoJo userModelPoJo=new UserModelPoJo();
                userModelPoJo.setFirstName("chaudhari");
                   detailPojo.setUser(userModelPoJo);
                    Toast.makeText(context, "message::"+detailPojo.getUser().getUsername(), Toast.LENGTH_LONG).show();
                    departmentList.set(i,detailPojo);
                    recentchatAdapter.notifyDataSetChanged();
                }
                else {
                Toast.makeText(context, "else::"+ departmentList.get(i).getUser().getId(), Toast.LENGTH_LONG).show();
                }
            }

        }

    private void getDepartmentData(String token) {
        tvGroup.setVisibility(View.VISIBLE);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getDepartment(token,FirebaseInstanceId.getInstance().getToken(),"m");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

               // CommonUtils.dismissProgress();

                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);
                tvGroup.setVisibility(View.GONE);
                String  str = null;
                try {
                    if(response!=null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                   Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if(jObjError.getString("message").equalsIgnoreCase("inactive")) {

                         CommonUtils.inActivieDailog(context,jObjError.getString("message"),"inactive");
                        //CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,false);
                        //   CommonUtils.inActivieDailog(context);
                    }
                } catch (Exception e) {
                   // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg="";
                JSONObject jsonTop=null;
                int status = 0;

                try {

                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status==420){
                    CommonUtils.logoutNotificationApi(context);
                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");

                }

                if(status==409){
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(context,msg,"inactive");
                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");

                }

                if(status==421){
                    if((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(context, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }
                }

               // DepartmentPojo DepartmentPojo = response.body();

                try {


                    try {

                        if(str!=null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    DepartmentPojo DepartmentPojo =new DepartmentPojo();
                    Gson gson=new Gson();
                    DepartmentPojo= gson.fromJson(jsonTop.toString(),DepartmentPojo.class);





                    if (DepartmentPojo != null) {
                        if (DepartmentPojo.getResponse()!=null) {

                            ivProgress.clearAnimation();
                            ivProgress.setVisibility(View.GONE);
                            tvGroup.setVisibility(View.GONE);

                            if (DepartmentPojo.getResponse().getSuccess()) {

                                String imageurl=DepartmentPojo.getResponse().getImageurl();
                                CommonUtils.savePreferencesString(getActivity(),AppConstants.DEPARTMENT_IMAGE_URL,imageurl);
                                Log.e("MSG",DepartmentPojo.getResponse().getMessage());

                                departmentList =DepartmentPojo.getResponse().getData();
                                //save list into sharepreferences..
                                setList("DEPARTMENTDATA",departmentList);


                                if(departmentList !=null&& departmentList.size()>0) {
                                    Log.e("LIST_SIZE",departmentList.size()+"");
                                //    Toast.makeText(context, departmentList.size()+"", Toast.LENGTH_SHORT).show();
                                    recentchatAdapter = new RecentchatAdapter(departmentList, context,imageurl);
                                    recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
                                   // recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                                 //  recy_chat_list.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
                                    recy_chat_list.setAdapter(recentchatAdapter);

                                 //   CommonUtils.snackBar(DepartmentPojo.getResponse().getMessage(), recy_chat_list);
                                   // Toast.makeText(context, DepartmentPojo.getResponse().getMessage() + "", Toast.LENGTH_SHORT).show();

                                }
                                else {
                                    tv_content.setVisibility(View.VISIBLE);
                                }

                            } else {
                              //  CommonUtils.snackBar(DepartmentPojo.getResponse().getMessage(), recy_chat_list);
                            }

                        }

                        else {

                            CommonUtils.snackBar(msg, recy_chat_list);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public<T> void setList(String key,List<DepartmentListDetailPojo> list)
    {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }
    public static void set(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        List<DepartmentListDetailPojo>  filterList = new ArrayList();

        //looping through existing elements
        for (DepartmentListDetailPojo s : departmentList) {
            //if the existing elements contains the search input
            if (s.getUser().getFirstName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list

        if(filterList!=null&&filterList.size()>0) {

            recentchatAdapter.filterList(filterList);
            recentchatAdapter.notifyDataSetChanged();

        }
    }

}
