package wehyphens.com.satyaconnect.fragments;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.AboutUsActivity;
import wehyphens.com.satyaconnect.activities.HelpActivity;
import wehyphens.com.satyaconnect.activities.LoginActivity;
import wehyphens.com.satyaconnect.activities.NotificationActivity;
import wehyphens.com.satyaconnect.activities.ProfileActivity;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.models.MutableImage;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class SettingsFragment extends Fragment {
    RelativeLayout rl_help,rl_about_us,rel_notification,rl_profile,rl_logout;
    private ImageView civ_user_pic;
    private  String profile_pic="";
    private  Context mContext;
    TextView tv_userName,tv_status;
    private MutableImage mutableImage;
    String  image="";
    String user_name="";
    String status="";
    private String fireBaseKey="";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_settings, container, false);
         mContext=getActivity();
         profile_pic= CommonUtils.getPreferences(mContext, AppConstants.PROFILE_PIC);
         user_name=CommonUtils.getPreferencesString(mContext,AppConstants.USER_NAME);
         status=CommonUtils.getPreferencesString(mContext,AppConstants.USER_STATUS);
        getToken();

        rl_about_us=view.findViewById(R.id.rl_about_us);
        civ_user_pic=view.findViewById(R.id.civ_user_pic);
        rl_help=view.findViewById(R.id.rl_help);
        rel_notification=view.findViewById(R.id.rel_notification);
        rl_profile=view.findViewById(R.id.rl_profile);
        rl_logout=view.findViewById(R.id.rl_logout);
        tv_status=view.findViewById(R.id.tv_status);
        tv_userName=view.findViewById(R.id.tv_userName);
       // mutableImage=new MutableImage();


        return view;
    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        profile_pic= CommonUtils.getPreferences(mContext, AppConstants.PROFILE_PIC);
        user_name=CommonUtils.getPreferencesString(mContext,AppConstants.USER_NAME);
        status=CommonUtils.getPreferencesString(mContext,AppConstants.USER_STATUS);

        tv_userName.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(mContext,AppConstants.USER_NAME)));
        tv_status.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(mContext,AppConstants.USER_STATUS )));

        if(profile_pic!=null&&!TextUtils.isEmpty(profile_pic)){
            Picasso.with(mContext).load(profile_pic).error(R.drawable.green_user).fit().noFade().centerCrop().into(civ_user_pic);
        }
        else { civ_user_pic.setImageResource(R.drawable.green_user);

        }


        }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        profile_pic= CommonUtils.getPreferences(mContext, AppConstants.PROFILE_PIC);

        if(profile_pic!=null&&!TextUtils.isEmpty(profile_pic)){
            Picasso.with(mContext).load(profile_pic).error(R.drawable.green_user).into(civ_user_pic);
        }
        else { civ_user_pic.setImageResource(R.drawable.green_user);

        }
        rl_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(mContext,AppConstants.VIEW_TYPE,"help");
                startActivity(new Intent(getActivity(),HelpActivity.class));
            }
        });

        rl_about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(mContext,AppConstants.VIEW_TYPE,"about_us");
                startActivity(new Intent(getActivity(),AboutUsActivity.class));
            }
        });

        rel_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),NotificationActivity.class));
            }
        });

        rl_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }
        });
        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.showProgress(mContext);
                calLogoutApi(CommonUtils.getPreferences(mContext,AppConstants.LOGIN_AUTHENTICATE));


            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
    private void calLogoutApi(String login_key) {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        Log.e("tokenLogin","tokenLogin"+login_key);
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<ResponseBody> call = service.getLogout(login_key,FirebaseInstanceId.getInstance().getToken(),"logout");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {


                CommonUtils.dismissProgress();

                String str="",msg="";
                boolean status = false;
                try {
                    if(response.body()!=null) {
                        str = response.body().string();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {

                    if(str!=null) {
                        JSONObject jsonTop = new JSONObject(str);
                        JSONObject jsonObject = jsonTop.getJSONObject("response");
                        status = jsonObject.getBoolean("success");
                        msg = jsonObject.getString("message");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(status){
                    CommonUtils.snackBar(msg, tv_status);

                    CommonUtils.savePreferencesBoolean(mContext,AppConstants.FIRST_TIME_LOGIN,false);
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();


                }
                else {
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                    CommonUtils.snackBar(msg, tv_status);


                }



               // ResponseBody logoutData = response.body();
          /*      LoginModel loginModel = response.body();
                //countryLis
                t1.clear();
                // countryCodeList.clear();
                if (loginModel != null) {
                    if (loginModel.getResponse()=null) {

                        if (loginModel.getResponse().getSuccess()) {

                            CommonUtils.savePreferencesBoolean(mContext,AppConstants.FIRST_TIME_LOGIN,false);
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                            Log.e("Authentication :: ",CommonUtils.getPreferences(mContext,AppConstants.LOGIN_AUTHENTICATE));
                        } else {

                            CommonUtils.snackBar("Not Valid User.", tv_status);

                        }}
                        else {
                        CommonUtils.snackBar("Response null", tv_status);

                    }
                }*/
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }


}
