package wehyphens.com.satyaconnect.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ProfileActivity;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.adapter.StatusAdapter;
import wehyphens.com.satyaconnect.models.DepartmentPojo;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.models.MyProfilePOJO;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.models.StatusData;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.MultipartFileUploader;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;
import wehyphens.com.satyaconnect.utils.RecyclerTouchListener;
import wehyphens.com.satyaconnect.utils.SeparatorDecoration;

public class UpdateProfileFragment extends Fragment {


    TextView tv_title_name, tv_cancel, tv_ok;
    EditText et_fname;
    RecyclerView recyview_status;
    List<StatusData> statusDataList;
    LinearLayout ll_select_status, ll_cancel_ok;

    StatusData data;
    String status = "";
    ImageView iv_edit_status;
    UpdateProfileFragment updateProfileFragment;
    StatusAdapter statusAdapter;
    String name = "";
    private String fireBaseKey = "";
    private Context mContext;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_update_profile, container, false);
        mContext = getActivity();

        getToken();
        tv_title_name = view.findViewById(R.id.tv_title_name);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_ok = view.findViewById(R.id.tv_ok);
        et_fname = view.findViewById(R.id.et_fname);
        recyview_status = view.findViewById(R.id.recyview_status);
        ll_select_status = view.findViewById(R.id.ll_select_status);
        iv_edit_status = view.findViewById(R.id.iv_edit_status);
        ll_cancel_ok = view.findViewById(R.id.ll_cancel_ok);


        // name = getArguments().getString("NAME");
        //  et_fname.setText(name);


        return view;


    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);

        if (CommonUtils.getPreferencesString(getActivity(), AppConstants.FNAME).equalsIgnoreCase("status")) {
            recyview_status.setVisibility(View.VISIBLE);
            ll_select_status.setVisibility(View.VISIBLE);
            ll_cancel_ok.setVisibility(View.GONE);
            et_fname.setEnabled(false);
            tv_title_name.setText("Update Your Status !");
            if (CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_STATUS) != null) {
                et_fname.setText(CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_STATUS));
            }
           /* iv_edit_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyview_status.setVisibility(View.GONE);
                    et_fname.setCursorVisible(true);
                    et_fname.setEnabled(false);
                    ll_select_status.setVisibility(View.GONE);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

                }
            });

*/
            statusDataList = new ArrayList<>();

            data = new StatusData();
            data.setStatus("Available");
            statusDataList.add(data);

            data = new StatusData();
            data.setStatus("Busy");
            statusDataList.add(data);

            data = new StatusData();
            data.setStatus("In a meeting");
            statusDataList.add(data);

            data = new StatusData();
            data.setStatus("Can't talk, text only");
            statusDataList.add(data);

            data = new StatusData();
            data.setStatus("Battery about to die");
            statusDataList.add(data);

            SeparatorDecoration decoration = new SeparatorDecoration(getActivity(), Color.parseColor("#FFF9F7F7"), 1.5f);

            recyview_status.addItemDecoration(decoration);

            statusAdapter = new StatusAdapter(getActivity(), statusDataList);
            recyview_status.setLayoutManager(new GridLayoutManager(getActivity(), 1));
            recyview_status.setAdapter(statusAdapter);


            recyview_status.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyview_status, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    StatusData data = statusDataList.get(position);
                    status = data.getStatus();
                    updateStatus(status);
                    et_fname.setText(status);
                    Toast.makeText(getActivity(), data.getStatus() + " is selected!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));


        }
        if (CommonUtils.getPreferencesString(getActivity(), AppConstants.FNAME).equalsIgnoreCase("fname")) {

            recyview_status.setVisibility(View.GONE);
            ll_select_status.setVisibility(View.GONE);
            et_fname.setEnabled(true);
            et_fname.setClickable(true);
            tv_title_name.setText("Enter Your Name ");
            et_fname.setHint("Your name");
            et_fname.setText(getArguments().getString("NAME"));
            et_fname.setSelection(et_fname.getText().length());

            CommonUtils.showSoftKeyboard(getActivity());
        }


        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = et_fname.getText().toString();
                if (CommonUtils.getPreferencesString(getActivity(), AppConstants.FNAME).equalsIgnoreCase("fname")) {
                    updatefield(name);

                }
                if (CommonUtils.getPreferencesString(getActivity(), AppConstants.FNAME).equalsIgnoreCase("status")) {

                  //  updatefield(name);
                    updateStatus(name);
                }


            }
        });


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().finish();


            }
        });
    }


    private void updatefield(String name) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_ID));
        String header = "bearer " + CommonUtils.getPreferencesString(getActivity(), AppConstants.LOGIN_AUTHENTICATE);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call = service.updatefName(header, name, FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                String str = null;
                try {
                    if (response != null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {

                        CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");
                        //CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,false);
                        //   CommonUtils.inActivieDailog(context);
                    }
                } catch (Exception e) {
                    // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;

                try {

                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }


                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }


                // DepartmentPojo DepartmentPojo = response.body();


                try {


                    try {

                        if (str != null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MyProfilePOJO myProfilePOJO = new MyProfilePOJO();
                    Gson gson = new Gson();
                    myProfilePOJO = gson.fromJson(jsonTop.toString(), MyProfilePOJO.class);



                    if (myProfilePOJO != null) {

                        try {
                            if (myProfilePOJO.getResponse().getSuccess() != null) {

                                Toast.makeText(getActivity(), myProfilePOJO.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getActivity(), ProfileActivity.class);
                                startActivity(i);
                                getActivity().finish();


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }


        });
    }
    private void updateStatus(String name) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_ID));
        String header = "bearer " + CommonUtils.getPreferencesString(getActivity(), AppConstants.LOGIN_AUTHENTICATE);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call = service.updateStatus(header, name, FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                String str = null;
                try {
                    if (response != null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {

                        CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");
                        //CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,false);
                        //   CommonUtils.inActivieDailog(context);
                    }
                } catch (Exception e) {
                    // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;

                try {

                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }


                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }


                // DepartmentPojo DepartmentPojo = response.body();


                try {


                    try {

                        if (str != null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MyProfilePOJO myProfilePOJO = new MyProfilePOJO();
                    Gson gson = new Gson();
                    myProfilePOJO = gson.fromJson(jsonTop.toString(), MyProfilePOJO.class);



                    if (myProfilePOJO != null) {

                        try {
                            if (myProfilePOJO.getResponse().getSuccess() != null) {

                                Toast.makeText(getActivity(), myProfilePOJO.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getActivity(), ProfileActivity.class);
                                startActivity(i);
                                getActivity().finish();


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }


        });
    }


}
