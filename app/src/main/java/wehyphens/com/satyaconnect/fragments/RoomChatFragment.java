package wehyphens.com.satyaconnect.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.RoomChatAdapter;
import wehyphens.com.satyaconnect.db.MyApplication;
import wehyphens.com.satyaconnect.db.dao.UserInfoViewModel;
import wehyphens.com.satyaconnect.db.database.AppDatabase;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class RoomChatFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;

    RecentchatModel model;
    RoomChatAdapter recentchatAdapter;
    EditText et_search;
    TextView tv_toolbar_title,tv_content;
    private String token="";
    private ImageView iv_profile_pic;
    private  String  profile_pic="";
    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;
    private List<UserInfoDto> deaprtmentList;
    private UserInfoViewModel userInfoViewModel;





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_recent_chat, container, false);
        context=getActivity();
        mAppDb= MyApplication.getDb();
        deaprtmentList= new ArrayList<>();

        recy_chat_list=view.findViewById(R.id.recy_chat_list);
        tv_toolbar_title=view.findViewById(R.id.tv_toolbar_title);
        tv_content=view.findViewById(R.id.tv_content);
        iv_profile_pic=view.findViewById(R.id.iv_profile_pic);

        tv_toolbar_title.setText("Recent chats");
        profile_pic= CommonUtils.getPreferences(context, AppConstants.PROFILE_PIC);
        if(profile_pic!=null&&!TextUtils.isEmpty(profile_pic)){

            Picasso.with(context).load(profile_pic).error(R.drawable.green_user).into(iv_profile_pic);

        }
        else {

            iv_profile_pic.setImageResource(R.drawable.green_user);

        }




        token= CommonUtils.getPreferences(context, AppConstants.LOGIN_AUTHENTICATE);
        setObserver();

        if(deaprtmentList !=null&& deaprtmentList.size()>0) {

            recentchatAdapter = new RoomChatAdapter(deaprtmentList, context);
            recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
            recy_chat_list.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
            recy_chat_list.setAdapter(recentchatAdapter);

            //   CommonUtils.snackBar(DepartmentPojo.getResponse().getMessage(), recy_chat_list);
            // Toast.makeText(context, DepartmentPojo.getResponse().getMessage() + "", Toast.LENGTH_SHORT).show();


        }
        else {
            tv_content.setVisibility(View.VISIBLE);
        }










        return view;
    }


    private void setObserver() {
        if (userInfoViewModel == null) {
            userInfoViewModel = ViewModelProviders.of(getActivity()).get(UserInfoViewModel.class);

            /*1 : Req user*/
            userInfoViewModel.getAllChatObs().observe(getActivity(), res -> {


                deaprtmentList.addAll(res);
               Log.e("ressize",res.size()+"");
            });
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }
}
