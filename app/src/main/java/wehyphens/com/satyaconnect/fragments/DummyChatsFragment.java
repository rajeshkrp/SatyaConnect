package wehyphens.com.satyaconnect.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.intentservice.chatui.fab.FloatingActionButton;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.LoginActivity;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.data.GroupDB;
import wehyphens.com.satyaconnect.data.StaticConfig;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.ListFriend;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.service.ServiceUtils;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class DummyChatsFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    ArrayList<RecentchatModel> arrayList;
    RecentchatAdapter recentchatAdapter;
    private ArrayList<String> listFriendID = null;
    private ListFriend dataListFriend = null;
    private CountDownTimer detectFriendOnline;
    public static int ACTION_START_CHAT = 1;
    private ListFriendsAdapter adapter;
    private ImageView fab;

    DatabaseReference databaseReference;
    public static final String ACTION_DELETE_FRIEND = "com.android.rivchat.DELETE_FRIEND";

    private BroadcastReceiver deleteFriendReceiver;
    private LovelyProgressDialog dialogFindAllFriend;
    private static final String SIGNOUT_LABEL = "Sign out";
    private ImageView llgout;







    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.dummy_chat_frag, container, false);
        recy_chat_list=view.findViewById(R.id.recy_chat_list);
        fab=view.findViewById(R.id.fab);
   //     llgout = (ImageView) view.findViewById(R.id.llgout);
        context=getActivity();
        arrayList=new ArrayList<>();
        CommonUtils.hide_keyboard((Activity)context);

      /*  llgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    FirebaseAuth.getInstance().signOut();
                    FriendDB.getInstance(context).dropDB();
                    GroupDB.getInstance(context).dropDB();
                    ServiceUtils.stopServiceFriendChat(context.getApplicationContext(), true);
                    startActivity(new Intent(context, LoginActivity.class));

            }
        });*/









        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new LovelyTextInputDialog(context, R.style.EditTextTintTheme)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle("Add friend")
                        .setMessage("Enter friend email")
                        .setIcon(R.drawable.ic_spinner2)
                        .setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .setInputFilter("Email not found", new LovelyTextInputDialog.TextFilter() {
                            @Override
                            public boolean check(String text) {
                                Pattern VALID_EMAIL_ADDRESS_REGEX =
                                        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(text);
                                return matcher.find();
                            }
                        })
                        .setConfirmButton(android.R.string.ok, new LovelyTextInputDialog.OnTextInputConfirmListener() {
                            @Override
                            public void onTextInputConfirmed(String text) {
                                //Tim id user id
                                findIDEmail(text);
                                //Check xem da ton tai ban ghi friend chua
                                //Ghi them 1 ban ghi
                            }
                        })
                        .show();

            }
        });




        detectFriendOnline = new CountDownTimer(System.currentTimeMillis(), StaticConfig.TIME_TO_REFRESH) {
            @Override
            public void onTick(long l) {
                ServiceUtils.updateFriendStatus(context, dataListFriend);
                ServiceUtils.updateUserStatus(context);
            }

            @Override
            public void onFinish() {

            }
        };
        if (dataListFriend == null) {
            dataListFriend = FriendDB.getInstance(context).getListFriend();
            if (dataListFriend.getListFriend().size() > 0) {
                listFriendID = new ArrayList<>();
                for (Friend friend : dataListFriend.getListFriend()) {
                    listFriendID.add(friend.id);
                }
                detectFriendOnline.start();
            }
        }







        if(dataListFriend!=null) {

            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recy_chat_list.setLayoutManager(linearLayoutManager);
            adapter = new ListFriendsAdapter(context, dataListFriend, this);
            recy_chat_list.setAdapter(adapter);
        }

        else {
            getListFriendUId();
        }





        dialogFindAllFriend = new LovelyProgressDialog(context);
        if (listFriendID == null) {
            listFriendID = new ArrayList<>();
           /* dialogFindAllFriend.setCancelable(false)
                    .setIcon(R.drawable.man_t)
                    .setTitle("Get all friend....")
                    .setTopColorRes(R.color.colorPrimary)
                    .show();*/
            getListFriendUId();
        }

        deleteFriendReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String idDeleted = intent.getExtras().getString("idFriend");
                for (Friend friend : dataListFriend.getListFriend()) {
                    if(idDeleted.equals(friend.id)){
                        ArrayList<Friend> friends = dataListFriend.getListFriend();
                        friends.remove(friend);
                        break;
                    }
                }
                adapter.notifyDataSetChanged();
            }
        };

        IntentFilter intentFilter = new IntentFilter(ACTION_DELETE_FRIEND);
        context.registerReceiver(deleteFriendReceiver, intentFilter);



        return view;
    }


    private void findAllUser() {

        FirebaseDatabase.getInstance().getReference().child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()!= null) {

                    String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                    HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
                    Friend user = new Friend();
                    user.name = (String) userMap.get("name");
                    user.email = (String) userMap.get("email");
                    user.avata = (String) userMap.get("avata");
                    user.id = id;
                    user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();

                    dataListFriend.getListFriend().add(user);
                    FriendDB.getInstance(context).addFriend(user);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void findIDEmail(String email) {

        FirebaseDatabase.getInstance().getReference().child("users").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //email not found
                    new LovelyInfoDialog(context)
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_spinner2)
                            .setTitle("Fail")
                            .setMessage("Email not found")
                            .show();
                } else {
                    String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                    if (id.equals(StaticConfig.UID)) {
                        new LovelyInfoDialog(context)
                                .setTopColorRes(R.color.colorAccent)
                                .setIcon(R.drawable.ic_spinner2)
                                .setTitle("Fail")
                                .setMessage("Email not valid")
                                .show();
                    } else {
                        HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
                        Friend user = new Friend();
                        user.name = (String) userMap.get("name");
                        user.email = (String) userMap.get("email");
                        user.avata = (String) userMap.get("avata");
                        user.id = id;
                        user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();
                        checkBeforAddFriend(id, user);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Lay danh sach friend cua một UID
     */
    private void checkBeforAddFriend(final String idFriend, Friend userInfo) {



        new LovelyInfoDialog(context).setCancelable(false)
                .setIcon(R.drawable.ic_spinner2)
                .setTitle("Add friend....")
                .setTopColorRes(R.color.colorPrimary)
                .show();



        //Check xem da ton tai id trong danh sach id chua
        if (listFriendID.contains(idFriend)) {
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_spinner2)
                    .setTitle("Friend")
                    .setMessage("User "+userInfo.email + " has been friend")
                    .show();

        } else {
            addFriend(idFriend, true);
            listFriendID.add(idFriend);
            dataListFriend.getListFriend().add(userInfo);
            FriendDB.getInstance(context).addFriend(userInfo);

            adapter.notifyDataSetChanged();
        }
        }

    private void addFriend(final String idFriend, boolean isIdFriend) {
        if (idFriend != null) {
            if (isIdFriend) {
                FirebaseDatabase.getInstance().getReference().child("friend/" + StaticConfig.UID).push().setValue(idFriend)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    addFriend(idFriend, false);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_spinner2)
                                        .setTitle("False")
                                        .setMessage("False to add friend success")
                                        .show();
                            }
                        });
            } else {
                FirebaseDatabase.getInstance().getReference().child("friend/" + idFriend).push().setValue(StaticConfig.UID).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            addFriend(null, false);
                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_spinner2)
                                        .setTitle("False")
                                        .setMessage("False to add friend success")
                                        .show();
                            }
                        });
            }
        } else {
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_spinner2)
                    .setTitle("Success")
                    .setMessage("Add friend success")
                    .show();
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }



    private void getListFriendUId() {
        FirebaseDatabase.getInstance().getReference().child("friend/" + StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapRecord = (HashMap) dataSnapshot.getValue();
                    Iterator listKey = mapRecord.keySet().iterator();
                    while (listKey.hasNext()) {
                        String key = listKey.next().toString();
                        listFriendID.add(mapRecord.get(key).toString());





                    }
                    getAllFriendInfo(0);
                } else {
                    dataListFriend.getListFriend().clear();
                    dialogFindAllFriend.dismiss();
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    /**
     * Truy cap bang user lay thong tin id nguoi dung
     */
    private void getAllFriendInfo(final int index) {

        if (listFriendID != null && listFriendID.size() > 0) {
            if (index == listFriendID.size()) {
                //save list friend
                adapter.notifyDataSetChanged();
                dialogFindAllFriend.dismiss();
                detectFriendOnline.start();
            } else {
                final String id = listFriendID.get(index);
                FirebaseDatabase.getInstance().getReference().child("users/" + id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Friend user = new Friend();
                            HashMap mapUserInfo = (HashMap) dataSnapshot.getValue();
                            user.name = (String) mapUserInfo.get("name");
                            user.email = (String) mapUserInfo.get("email");
                            user.avata = (String) mapUserInfo.get("avata");
                            user.id = id;
                            user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();
                            dataListFriend.getListFriend().add(user);
                            FriendDB.getInstance(context).addFriend(user);
                        }
                        getAllFriendInfo(index + 1);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(deleteFriendReceiver);
    }
}

/**/