package wehyphens.com.satyaconnect.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.LoginActivity;
import wehyphens.com.satyaconnect.adapter.CurrentchatAdapter;
import wehyphens.com.satyaconnect.adapter.GroupchatAdapter;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.models.ChangePassModel;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.models.Group;
import wehyphens.com.satyaconnect.models.GroupDeatailsModel;
import wehyphens.com.satyaconnect.models.GroupDetailList;
import wehyphens.com.satyaconnect.models.GroupModel;
import wehyphens.com.satyaconnect.models.GroupModel;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.models.UserModelPoJo;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class GroupsFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    List<Group> groupList;
    List<Group> searchGroupList;
    List<UserModelPoJo> groupUserList;
    RelativeLayout rl_root;
    RecentchatModel model;
    GroupchatAdapter recentchatAdapter;
    private LinearLayout llSearch;
    TextView  tv_toolbar_title,tvGroup;
    private String token="";
    private  String  profile_pic="";
    private ImageView iv_profile_pic,ivProgress;
    EditText et_search;
    BottomNavigationView bottomNavigationView;
    private String fireBaseKey="";
    SharedPreferences sharedPreferences ;
    static SharedPreferences.Editor editor;
    boolean isSearch=false;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_groups, container, false);
        context=getActivity();
        getToken();

        llSearch=view.findViewById(R.id.llSearch);
        iv_profile_pic=view.findViewById(R.id.iv_profile_pic);
        et_search=view.findViewById(R.id.et_search);
        groupList=new ArrayList<>();
        groupUserList=new ArrayList<>();
        recy_chat_list=view.findViewById(R.id.recy_chat_list);
        llSearch.setVisibility(View.VISIBLE);
        tv_toolbar_title=view.findViewById(R.id.tv_toolbar_title);
        tvGroup=view.findViewById(R.id.tvGroup);
        tv_toolbar_title.setText("Groups");
        profile_pic= CommonUtils.getPreferences(context, AppConstants.PROFILE_PIC);
        ivProgress=view.findViewById(R.id.ivProgress);
        Animation animation1 = AnimationUtils.loadAnimation(context, R.anim.move);
        ivProgress.startAnimation(animation1);
        rl_root=view.findViewById(R.id.rl_root);

        bottomNavigationView=getActivity().findViewById(R.id.navigation);

        sharedPreferences=context.getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        setupUI(rl_root);

       // checkKeyBoardUp();
        if(profile_pic!=null&&!TextUtils.isEmpty(profile_pic)){

            Picasso.with(context).load(profile_pic).error(R.drawable.green_user).fit().centerCrop().into(iv_profile_pic);
        }
        else {

            iv_profile_pic.setImageResource(R.drawable.green_user);
        }

        et_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                et_search.setCursorVisible(true);
            }
        });

      /*  List<Group> groupList= getArrayList("key");
        if(groupList!=null && groupList.size()>0){
            String imageurl=CommonUtils.getPreferencesString(getActivity(),AppConstants.IMAGE_URL);
            String group_user=CommonUtils.getPreferencesString(getActivity(),AppConstants.SENDER_USER_NAME);
            recentchatAdapter = new GroupchatAdapter(groupList, context,imageurl,group_user);
            // recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
            recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
            // recy_chat_list.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
            recy_chat_list.setAdapter(recentchatAdapter);
        }*/

        String serializedObject = sharedPreferences.getString("GROUPDATA", null);
        if(serializedObject!=null){
            Gson gson1 = new Gson();
            Type type=new TypeToken<List<Group>>(){}.getType();
            groupList=gson1.fromJson(serializedObject, type);
             String imageurl=CommonUtils.getPreferencesString(getActivity(),AppConstants.IMAGE_URL);
            String group_user=CommonUtils.getPreferencesString(getActivity(),AppConstants.SENDER_USER_NAME);
            recentchatAdapter = new GroupchatAdapter(groupList, context,imageurl,group_user);
            recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
            recy_chat_list.setAdapter(recentchatAdapter);
        }

        token=CommonUtils.getPreferences(context, AppConstants.LOGIN_AUTHENTICATE);

            if (token != null && !token.equalsIgnoreCase("")){
                Log.e("GroupToken","GroupToken::"+token);
             //   CommonUtils.showProgress(context);
               // ivProgress.setVisibility(View.VISIBLE);
                if(groupList.size()==0){
                    //CommonUtils.showProgress(context);
                     ivProgress.setVisibility(View.VISIBLE);
                }
                if(CommonUtils.isInternetAvailable(getActivity()))
                {
                    getGroupData(token);
                }else {
                    CommonUtils.noInternetConnection(getActivity());
                }

        }


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                et_search.setCursorVisible(false);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                et_search.setCursorVisible(true);
                if (charSequence.toString().isEmpty()) {
                    isSearch = false;
                    setAdapterr(groupList);
                    recentchatAdapter.notifyDataSetChanged();
                } else {
                    isSearch = true;
                    searchGroupList = getSearchGroupList(charSequence.toString());
                    setAdapterr(searchGroupList);
                    recentchatAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                et_search.setCursorVisible(false);
                //after the change calling the method and passing the search input
               // filter(editable.toString());
            }
        });


        return view;
    }


    public List<Group> getSearchGroupList(String charString) {
        // String charString=charSequence.toString();
        List<Group> filteredGroupList = new ArrayList<>();
        for (int i = 0; i <= groupList.size() - 1; i++) {
            Group obj = new Group();
            obj = groupList.get((i));
            if (groupList.get((i)).getGroupDetail().getGroupName() != null) {
                if (groupList.get((i)).getGroupDetail().getGroupName().toLowerCase().contains(charString.toLowerCase())) {
                    filteredGroupList.add(obj);
                }
            }
        }

        return filteredGroupList;
    }



    public void setAdapterr(List<Group> groupList) {
        String imageurl=CommonUtils.getPreferencesString(getActivity(),AppConstants.IMAGE_URL);
        String group_user=CommonUtils.getPreferencesString(getActivity(),AppConstants.SENDER_USER_NAME);
        recentchatAdapter = new GroupchatAdapter(groupList, context,imageurl,group_user);
        recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
        recy_chat_list.setHasFixedSize(true);
        recy_chat_list.setAdapter(recentchatAdapter);
        recentchatAdapter.notifyDataSetChanged();
    }




    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {

                    //hideSoftKeyboard(getActivity());
                    CommonUtils.hideKeyPad(getActivity());
                   // et_search.setCursorVisible(false);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);


            }
        }
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(context, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(context, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    private void getGroupData(String token) {

        tvGroup.setVisibility(View.VISIBLE);

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getGroup(token,FirebaseInstanceId.getInstance().getToken(),"m");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);
                tvGroup.setVisibility(View.GONE);

                String group_user="";
                String  str = null;
                try {
                    if(response!=null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                      Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if(jObjError.getString("message").equalsIgnoreCase("inactive")) {
                          CommonUtils.inActivieDailog(context,jObjError.getString("message"),"inactive");
                        ///   CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,false);
                        //   CommonUtils.inActivieDailog(context);
                    }
                } catch (Exception e) {
                //    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg="";
                JSONObject jsonTop=null;
                int status = 0;

                try {

                    if(str!=null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status==420){
                    CommonUtils.logoutNotificationApi(context);
                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");
                }

                if(status==409){
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(context,msg,"inactive");
                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");

                }

                if(status==421){
                    if((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(context, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }
                }

                try {

                    try {

                        if(str!=null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    GroupModel GroupModel =new GroupModel();
                    Gson gson=new Gson();
                    GroupModel= gson.fromJson(jsonTop.toString(),GroupModel.class);

                    if (GroupModel != null) {
                        if (GroupModel.getResponse()!=null) {


                            if (GroupModel.getResponse().getSuccess()) {
                                String imageurl=GroupModel.getResponse().getImageurl();
                                CommonUtils.savePreferencesString(getActivity(),AppConstants.IMAGE_URL,imageurl);

                                Log.e("MSG",GroupModel.getResponse().getMessage());

                                groupList=GroupModel.getResponse().getData().getGroup();

                                setList("GROUPDATA",groupList);



                              //  saveArrayList(groupList,"key");



                                for (int i = 0; i < GroupModel.getResponse().getData().getGroup().size(); i++) {
                                    groupUserList= GroupModel.getResponse().getData().getGroup().get(i).getUser();
                                    for (int j = 0; j < groupUserList.size(); j++) {
                                        if(i==0){
                                            group_user=groupUserList.get(0).getFirstName();
                                        }
                                        else {
                                            group_user=group_user+", "+groupUserList.get(0).getFirstName();
                                        } } }

                                    Log.e("group_nameFragment","group_nameFragment"+group_user);

                                if(groupList!=null&&groupList.size()>0) {

                                    recentchatAdapter = new GroupchatAdapter(groupList, context,imageurl,group_user);
                                   // recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                                    recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
                                   // recy_chat_list.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
                                    recy_chat_list.setAdapter(recentchatAdapter);


                                   // CommonUtils.snackBar(GroupModel.getResponse().getMessage(), recy_chat_list);
                                    //Toast.makeText(context, GroupModel.getResponse().getMessage() + "", Toast.LENGTH_SHORT).show();

                                }
                                else {
                                    tvGroup.setVisibility(View.VISIBLE);
                                }
                            } else {
                                //CommonUtils.snackBar(GroupModel.getResponse().getMessage(), recy_chat_list);
                            }

                        }

                        else {

                            CommonUtils.snackBar(msg, recy_chat_list);
                        }
                    }

                    else {
                        tvGroup.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public<T> void setList(String key,List<Group> list)
    {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }
    public static void set(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }




    private void saveArrayList(List<Group> groupList, String key) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(key);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public List<Group> getArrayList(String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<Group>>() {}.getType();
        return gson.fromJson(json, type);
    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
      List<Group>  filterList = new ArrayList();

        //looping through existing elements
        for (Group s : groupList) {
            //if the existing elements contains the search input
            if (s.getGroupDetail().getGroupName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        if(filterList!=null&&filterList.size()>0) {
            recentchatAdapter.filterList(filterList);
        }
    }






}
