package wehyphens.com.satyaconnect.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.intentservice.chatui.models.ChatMessage;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.CurrentchatAdapter;
import wehyphens.com.satyaconnect.adapter.RecentchatAdapter;
import wehyphens.com.satyaconnect.firebase.MyfirebaseInstanceidService;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.models.DepartmentListPOJO;
import wehyphens.com.satyaconnect.models.DepartmentPojo;
import wehyphens.com.satyaconnect.models.LatestMessage;
import wehyphens.com.satyaconnect.models.Message;
import wehyphens.com.satyaconnect.models.RecentchatModel;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;


public class CurrentChatFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    ArrayList<RecentchatModel> arrayList;
    RelativeLayout rl_root;
    RecentchatModel model;
    CurrentchatAdapter recentchatAdapter;
    FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    EditText et_search;
    TextView tv_toolbar_title, tvGroup;
    private String token = "";
    private ImageView iv_profile_pic, ivProgress;
    List<LatestMessage> latestMessageList = new ArrayList<>();
    List<LatestMessage> updatemsgMessageList;
    List<LatestMessage> searchMessageList;
    private String profile_pic = "";
    private String senderid = "";
    private String firebasetoken = "";
    BottomNavigationView bottomNavigationView;
    SwipeController swipeController = null;
    boolean isSearch=false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        context = getActivity();
        recy_chat_list = view.findViewById(R.id.recy_chat_list);
        ivProgress = view.findViewById(R.id.ivProgress);
        tv_toolbar_title = view.findViewById(R.id.tv_toolbar_title);
        tvGroup = view.findViewById(R.id.tvGroup);
        iv_profile_pic = view.findViewById(R.id.iv_profile_pic);
        et_search = view.findViewById(R.id.et_search);
        tv_toolbar_title.setText("Recent chats");
        tvGroup.setVisibility(View.VISIBLE);
        tvGroup.setText("Recent Chat will come soon here!");
        ivProgress.setVisibility(View.GONE);
        recy_chat_list.setVisibility(View.GONE);

        //   latestMessageList.clear();

        rl_root = view.findViewById(R.id.rl_root);

        bottomNavigationView = getActivity().findViewById(R.id.navigation);
        // checkKeyBoardUp();

        if (CommonUtils.getPreferences(context, AppConstants.FIREBASE_KEY) != null) {

            firebasetoken = CommonUtils.getPreferences(context, AppConstants.FIREBASE_KEY);
        } else {
            firebasetoken = FirebaseInstanceId.getInstance().getToken();
        }
        if(latestMessageList.size()>0){
            tvGroup.setVisibility(View.GONE);
        }else {
            tvGroup.setVisibility(View.VISIBLE);
        }

        senderid = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        updatemsgMessageList = new ArrayList<>();
        recentchatAdapter = new CurrentchatAdapter(latestMessageList, context);
        Animation animation1 =
                AnimationUtils.loadAnimation(context, R.anim.move);
        ivProgress.startAnimation(animation1);

        et_search.setCursorVisible(false);
        setupUI(rl_root);
        profile_pic = CommonUtils.getPreferences(context, AppConstants.PROFILE_PIC);

        if (profile_pic != null && !TextUtils.isEmpty(profile_pic)) {

            Picasso.with(context).load(profile_pic).noFade().error(R.drawable.green_user).fit().centerCrop()
                    .into(iv_profile_pic);
        } else {
            iv_profile_pic.setImageResource(R.drawable.green_user);

        }

        FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(senderid).orderByChild("timestamp").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //   Toast.makeText(context, "onChildAdded", Toast.LENGTH_SHORT).show();

                LatestMessage latestMessage = null;

                int firebaseObjectCount = 0;
                tvGroup.setVisibility(View.GONE);
                ivProgress.setVisibility(View.GONE);
                recy_chat_list.setVisibility(View.VISIBLE);
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        latestMessage = postSnapshot.getValue(LatestMessage.class);
                        firebaseObjectCount = (int) postSnapshot.getChildrenCount();

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            latestMessageList.add(latestMessage);
                        }
                    }

                    if (latestMessageList != null && latestMessageList.size() > 0) {
                        Collections.sort(latestMessageList, Collections.reverseOrder());
                        recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                        recy_chat_list.setAdapter(recentchatAdapter);
/*

                        swipeController=new SwipeController(new SwipeControllerActions() {
                            @Override
                            public void onLeftClicked(int position) {
                                super.onLeftClicked(position);
                            }

                            @Override
                            public void onRightClicked(int position) {
                                super.onRightClicked(position);
                            }
                        });*/
/* {
                            @Override
                            public void onLeftClicked(int position) {
                                super.onLeftClicked(position);
                            }
                            @Override
                            public void onRightClicked(int position) {
                                super.onRightClicked(position);
                                recentchatAdapter.notifyItemRemoved(position);
                                recentchatAdapter.notifyItemRangeChanged(position, recentchatAdapter.getItemCount());
                            }
                        });*//*

                        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
                        itemTouchhelper.attachToRecyclerView(recy_chat_list);

                        recy_chat_list.addItemDecoration(new RecyclerView.ItemDecoration() {
                            @Override
                            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                                swipeController.onDraw(c);
                            }
                        });
*/

                    } else {
                        tvGroup.setVisibility(View.VISIBLE);
                    }
                } else {
                    tvGroup.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //    Toast.makeText(context, "change", Toast.LENGTH_SHORT).show();
                String oponent_id = "";
                String Serveroponent_id = "";
                String message = "";
                LatestMessage post = null;

                LatestMessage message1 = new LatestMessage();
                int firebaseObjectCount = (int) dataSnapshot.getChildrenCount();
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        post = postSnapshot.getValue(LatestMessage.class);
                        firebaseObjectCount = (int) postSnapshot.getChildrenCount();

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            Serveroponent_id = post.getOponent_id();
                        }
                        if (latestMessageList.size() == 0) {
                            latestMessageList.add(post);
                            Collections.sort(latestMessageList, Collections.reverseOrder());
                            recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                            recy_chat_list.setAdapter(recentchatAdapter);
                            swipeController=new SwipeController(new SwipeControllerActions() {
                                @Override
                                public void onLeftClicked(int position) {
                                    super.onLeftClicked(position);
                                }
                                @Override
                                public void onRightClicked(int position) {
                                    super.onRightClicked(position);
                                    recentchatAdapter.notifyItemRemoved(position);
                                    recentchatAdapter.notifyItemRangeChanged(position, recentchatAdapter.getItemCount());
                                }
                            });

                            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
                            itemTouchhelper.attachToRecyclerView(recy_chat_list);

                            recy_chat_list.addItemDecoration(new RecyclerView.ItemDecoration() {
                                @Override
                                public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                                    swipeController.onDraw(c);
                                }
                            });

                        }
                    }
                    Log.e("Currentpost", "Currentpost::" + post);
                    LatestMessage changedMsg = null;
                    int possition = 0;
                    if (latestMessageList != null && latestMessageList.size() > 0) {
                        try {
                            for (int i = 0; i < latestMessageList.size(); i++) {
                                if (latestMessageList.get(i).getOponent_id() != null || Serveroponent_id != null) {
                                    if (latestMessageList.get(i).oponent_id.equalsIgnoreCase(post.getOponent_id())) {
                                        Log.e("latestMessageList", "latestMessageList::" + possition);
                                        Log.e("latestMessageList", "latestMessageList::" + possition);
                                        Log.e("latestMessageList", "latestMessageList::" + message);
                                        Log.e("latestMessageList", "latestMessageList::" + oponent_id);
                                        changedMsg = post;
                                        Log.e("latestMessageList", "latestMessageList::" + changedMsg);
                                        possition = i;
                                        break;
                                    } else {
                                        //  Toast.makeText(context, "not::"+latestMessageList.get(i).getOponent_id(), Toast.LENGTH_SHORT).show();

                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            try {
                                if (message1 != null) {
                                    latestMessageList.set(possition, changedMsg);
                                    Collections.sort(latestMessageList, Collections.reverseOrder());
                                    recentchatAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {


                //    Toast.makeText(context, "change", Toast.LENGTH_SHORT).show();
                String oponent_id = "";
                String Serveroponent_id = "";
                String message = "";
                LatestMessage post = null;

                LatestMessage message1 = new LatestMessage();
              //  int firebaseObjectCount = (int) dataSnapshot.getChildrenCount();
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        post = postSnapshot.getValue(LatestMessage.class);

                            Serveroponent_id = post.getOponent_id();

                    }
                    Log.e("Currentpost", "Currentpost::" + post);
                   // LatestMessage changedMsg = null;
                    int possition = 0;
                    if (latestMessageList != null && latestMessageList.size() > 0) {
                        try {
                            for (int i = 0; i < latestMessageList.size(); i++) {
                                if (latestMessageList.get(i).getOponent_id() != null || Serveroponent_id != null) {
                                    if (latestMessageList.get(i).oponent_id.equalsIgnoreCase(post.getOponent_id())) {
                                        Log.e("latestMessageList", "latestMessageList::" + possition);
                                       // Log.e("latestMessageList", "latestMessageList::" + message);
                                        Log.e("latestMessageList", "latestMessageList::" + oponent_id);
                                       // latestMessageList.remove(post);
                                        latestMessageList.remove(i);
                                        possition = i;
                                        Collections.sort(latestMessageList, Collections.reverseOrder());
                                        recentchatAdapter.notifyDataSetChanged();

                                        break;

                                    } else {

                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        }

                    }

                }
            //}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        et_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (b==true){
                et_search.setCursorVisible(true);}else {
                    et_search.setCursorVisible(false);
                }
            }
        });


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            et_search.setCursorVisible(false);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                et_search.setCursorVisible(true);
                if (charSequence.toString().isEmpty()) {
                    isSearch = false;
                    setAdapterr(latestMessageList);

                } else {
                    isSearch = true;
                    searchMessageList = getMessagelist(charSequence.toString());
                    setAdapterr(searchMessageList);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
               // filter(editable.toString());
            }
        });
        return view;
    }

    public List<LatestMessage> getMessagelist(String charString) {
        List<LatestMessage> filterList = new ArrayList();
        //looping through existing elements

        for (int i = 0; i <latestMessageList.size()-1 ; i++) {
            LatestMessage obj=new LatestMessage();
            obj=latestMessageList.get(i);
            if (latestMessageList.get(i).oponent_name.toLowerCase().contains(charString.toLowerCase())){
                filterList.add(obj);
            }
        }

        return filterList;
    }



    public void setAdapterr(List<LatestMessage> messageList) {
       // String imageurl = CommonUtils.getPreferencesString(getActivity(), AppConstants.DEPARTMENT_IMAGE_URL);
        recentchatAdapter = new CurrentchatAdapter(messageList, getActivity());
        recy_chat_list.setLayoutManager(new LinearLayoutManager(context));
        recy_chat_list.setHasFixedSize(true);
        recy_chat_list.setAdapter(recentchatAdapter);

    }



    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    //hideSoftKeyboard(getActivity());
                    CommonUtils.hideKeyPad(getActivity());
                    // et_search.setCursorVisible(false);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    public void checkKeyBoardUp() {
        rl_root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rl_root.getWindowVisibleDisplayFrame(r);
                int heightDiff = rl_root.getRootView().getHeight() - (r.bottom - r.top);

                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    //ok now we know the keyboard is up...
                    bottomNavigationView.setVisibility(View.INVISIBLE);

                } else {
                    //ok now we know the keyboard is down...
                    bottomNavigationView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        List<LatestMessage> filterList = new ArrayList();

        //looping through existing elements
        for (LatestMessage s : latestMessageList) {
            //if the existing elements contains the search input
            if (s.getOponent_name().toLowerCase().startsWith(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list

        if (filterList != null && filterList.size() > 0) {
            recentchatAdapter.filterList(filterList);
        }

    }


}



