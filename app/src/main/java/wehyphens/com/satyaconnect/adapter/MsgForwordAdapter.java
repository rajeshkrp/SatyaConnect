package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.List;
import java.util.concurrent.ExecutorService;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.data.StaticConfig;
import wehyphens.com.satyaconnect.db.MyApplication;
import wehyphens.com.satyaconnect.db.database.AppDatabase;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.handler.ExecutorHandler;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.Group;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class MsgForwordAdapter extends RecyclerView.Adapter<MsgForwordAdapter.ViewHolder> {

    DepartmentListDetailPojo departmentListDetailPojo;
    ExecutorService mExecutor;
    List<Group> groupList;
    List<Group> groupListFiltered;
    List<DepartmentListDetailPojo> contactList;
      Context context;
      private AppDatabase mAppDb;
    private String imageurl="";
    final int VIEW_TYPE_DEPARTMENT = 0;
    final int VIEW_TYPE_GROUP = 1;
    boolean isSelected=true;
    List<DepartmentListDetailPojo> contactListFiltered;
    int selectedPosition=-1;


public MsgForwordAdapter(List<DepartmentListDetailPojo> depList, Context context, String imageurl, List<Group> groupList){
    this.contactList=depList;
    this.context=context;
    this.imageurl=imageurl;
    this.groupList=groupList;
    this.contactListFiltered=depList;
    this.groupListFiltered=groupList;
    mAppDb = MyApplication.getDb();
    mExecutor = ExecutorHandler.getInstance().getExecutor();
}
    @Override
    public MsgForwordAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       /* if(viewType==VIEW_TYPE_DEPARTMENT){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_forword_row_item, parent, false);
        return new ViewHolder(view);
        }
        if(viewType==VIEW_TYPE_GROUP){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_forword_row_item, parent, false);
            return new ViewHolder(view);
        }
        return null;

*/
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_forword_row_item, parent, false);
        return new ViewHolder(view);

    }

   /* @Override
    public int getItemViewType(int position) {
      *//*  if(position < contactList.size()){
            return VIEW_TYPE_DEPARTMENT;
        }
        if(position - contactList.size() < groupList.size()){
            return VIEW_TYPE_GROUP;
        }

*//*
        return -1;
    }
*/
    @Override
    public long getItemId(int position) {
        return position;

    }



    @Override
    public void onBindViewHolder(MsgForwordAdapter.ViewHolder holder, int position) {
        if(selectedPosition==position){
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.row_bg));
            holder.civ_selected_row_item.setVisibility(View.VISIBLE);}
        else {
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.civ_selected_row_item.setVisibility(View.GONE);
        }

        if(position==0){
            holder.tv_section_title.setVisibility(View.VISIBLE);
            if(contactList.size()==0){
                holder.tv_section_title.setText("Groups");
            }else { holder.tv_section_title.setText("Contacts");}

        }
        else if(position == contactList.size()){
            holder.tv_section_title.setVisibility(View.VISIBLE);
            holder.tv_section_title.setText("Groups");
        }
        else {
            holder.tv_section_title.setVisibility(View.GONE);
        }
        if(position % 2==1){
           // holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.row_bg));
        }
        else {
              //  holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if(position<=(contactList.size()-1)){
           // holder.user_pic.setImageDrawable(R.drawable.green_user);
            holder.user_pic.setImageResource(R.drawable.green_user);
        if(contactList.get(position).getUser().getAvatar()!=null&&(!TextUtils.isEmpty(contactList.get(position).getUser().getAvatar()))) {
            Picasso.with(context).load(imageurl+contactList.get(position).getUser().getAvatar()).placeholder(R.drawable.green_user).fit().centerCrop()
                    .into(holder.user_pic);
        }

        if(!TextUtils.isEmpty(contactList.get(position).getUser().getFirstName())) {
            holder.userName.setText(contactList.get(position).getUser().getFirstName());
            holder.chatTitle.setText(contactList.get(position).getUser().getMyStatus() + "");
        }
        if(contactList.get(position).getUser().getCreated()!=null&&!TextUtils.isEmpty(contactList.get(position).getUser().getCreated())){
          String date[]=contactList.get(position).getUser().getCreated().split("//+");
            Log.e("date",date[0]);
            holder.lastSeen.setText(CommonUtils.getformattedDateWithTime(date[0]));
        }
        }
        else if(position>(contactList.size()-1)){
            holder.user_pic.setImageResource(R.drawable.group_icon);
            if (groupList.get(position-contactList.size()).getGroupDetail().getAvatar() != null && (!TextUtils.isEmpty(groupList.get(position-contactList.size()).getGroupDetail().getAvatar()))) {
                Picasso.with(context).load(imageurl + groupList.get(position-contactList.size()).getGroupDetail().getAvatar()).fit().centerCrop().placeholder(R.drawable.group_icon).error(R.drawable.group_icon).into(holder.user_pic);
            }

            if (!TextUtils.isEmpty(groupList.get(position-contactList.size()).getGroupDetail().getGroupName())) {
                holder.tv_groupName.setText(groupList.get(position-contactList.size()).getGroupDetail().getGroupName());
            }
        }
        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition=position;
                notifyDataSetChanged();

            }
        });


        holder.llUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {


                return false;
            }
        });
    }
    @Override
    public int getItemCount() {
       // return contactList == null ? 0 : contactList.size();
        return contactList.size()+groupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView userName,chatTitle,lastSeen,tv_section_title,tv_groupName;
        RelativeLayout llUser;
        LinearLayout ll_holder_view;
        CircleImageView civ_selected_row_item;
       // LinearLayout ll_row_container;

        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);
            civ_selected_row_item=itemView.findViewById(R.id.civ_selected_row_item);
            tv_section_title=itemView.findViewById(R.id.tv_section_title);
            tv_groupName=itemView.findViewById(R.id.tv_groupName);
            ll_holder_view=itemView.findViewById(R.id.ll_holder_view);
          //  ll_row_container=itemView.findViewById(R.id.ll_row_container);
        }
    }




    }
