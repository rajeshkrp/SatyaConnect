package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class RoomChatAdapter extends RecyclerView.Adapter<RoomChatAdapter.ViewHolder> {

    DepartmentListDetailPojo DepartmentListDetailPojo;
    List<UserInfoDto> arrayList;
      Context context;
    private String imageurl="";


public RoomChatAdapter(List<UserInfoDto> arrayList, Context context){

    this.arrayList=arrayList;
    this.context=context;
    this.imageurl=imageurl;


}

    @Override
    public RoomChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_chat_row_item, parent, false);

        return new ViewHolder(view);


    }


    @Override
    public void onBindViewHolder(RoomChatAdapter.ViewHolder holder, int position) {

    /*    holder.user_pic.setImageResource(arrayList.get(position).getUser_pic());
        holder.userName.setText(arrayList.get(position).getUserName());
        holder.chatTitle.setText(arrayList.get(position).getChattitle());
        holder.lastSeen.setText(arrayList.get(position).getLastSeen());*/


        if(arrayList.get(position).getUserPic()!=null&&(!TextUtils.isEmpty(arrayList.get(position).getUserPic()))) {
            Picasso.with(context).load(imageurl+arrayList.get(position).getUserPic()).into(holder.user_pic);
        }

        if(!TextUtils.isEmpty(arrayList.get(position).getUserName())) {
            holder.userName.setText(arrayList.get(position).getUserName());
            holder.chatTitle.setText(arrayList.get(position).getUserName() + "");
        }


        if(arrayList.get(position).getUserTime()!=null&&!TextUtils.isEmpty(arrayList.get(position).getUserTime())){
          String date[]=arrayList.get(position).getUserTime().split("//+");
            Log.e("date",date[0]);
            holder.lastSeen.setText(CommonUtils.getformattedDateWithTime(date[0]));

        }



        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(new Intent(context, ChatActivity.class));
                intent.putExtra(AppConstants.RECEIVER__PIC,imageurl+arrayList.get(position).getUserPic());
                intent.putExtra(AppConstants.RECEIVER__USER_NAME2,arrayList.get(position).getUserName());
                intent.putExtra(AppConstants.RECEIVER_ID,arrayList.get(position).getUserId());

                Log.e("receiver_id","receiver_id"+arrayList.get(position).getUserId());
                context.startActivity(intent);
            }
        });
      /*  holder.user_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ChatProfileActivity.class));
            }
        });*/



    }





    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView userName,chatTitle,lastSeen;
        RelativeLayout llUser;


        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);

        }
    }



    }
