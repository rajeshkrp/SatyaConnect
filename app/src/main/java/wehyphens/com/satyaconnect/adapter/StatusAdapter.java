package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.StatusData;

public class StatusAdapter  extends RecyclerView.Adapter<StatusAdapter.ProductViewHolder> {


    //this context we will use to inflate the otp_varification
    private Context mCtx;

    //we are storing all the products in a list
    private List<StatusData> statusList;


    //getting the context and product list with constructor
    public StatusAdapter(Context mCtx, List<StatusData> statusList) {
        this.mCtx = mCtx;
        this.statusList = statusList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.status_row_item, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position
        StatusData statusData = statusList.get(position);

        //binding the data with the viewholder views
        holder.tv_status.setText(statusData.getStatus());

    }


    @Override
    public int getItemCount() {
        return statusList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView tv_status;
        ImageView iv_selected;

        public ProductViewHolder(View itemView) {
            super(itemView);

            tv_status = itemView.findViewById(R.id.tv_status);
            iv_selected = itemView.findViewById(R.id.iv_selected);

        }
    }


    public void setView(ImageView imageView){

                imageView.setVisibility(View.VISIBLE);

    }


}
