package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.AllUserListDetailsModel;
import wehyphens.com.satyaconnect.utils.CommonUtils;

/**
 * Created by Admin on 10/31/2017.
 */

public class NewUserChatAdapter extends RecyclerView.Adapter<NewUserChatAdapter.ViewHolder> {
    List<AllUserListDetailsModel> arrayList = new ArrayList<>();
    Context context;
    LinearLayout root_layout;
    public NewUserChatAdapter(List<AllUserListDetailsModel> arrayList, Context activity) {
        this.arrayList = arrayList;
        this.context = activity;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_user_chat_item, parent, false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);
        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (arrayList != null && arrayList.size() > 0) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)holder.root_layout.getLayoutParams();
if(arrayList.size()==1){
    params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
    holder.root_layout.setLayoutParams(params);
}else if(arrayList.size()==2){

    if(position==0){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==1){
        params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }
}
else if(arrayList.size()==3){
    if(position==0){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==1){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==2){
        params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }


}else if(arrayList.size()==4){
    if(position==0){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==1){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==2){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==3){
        params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }
}else if(arrayList.size()>=5){
    if(position==0){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==1){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==2){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==3){
        params.setMargins(-20, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }else if(position==4){
        params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
        holder.root_layout.setLayoutParams(params);
    }
}



            AllUserListDetailsModel modelDetail = arrayList.get(position);

               holder.profileImage.setImageResource(modelDetail.getUserPic());

               }
        else {
           CommonUtils.snackBar("No user found",holder.profileImage);
        }

    }

    @Override
    public int getItemCount() {
        if(arrayList.size()==1) {
            return arrayList == null ? 0 : 1;

        }
        if(arrayList.size()==2) {
            return arrayList == null ? 0 : 2;
        }
        if(arrayList.size()==3) {
            return arrayList == null ? 0 : 3;
        }
        if(arrayList.size()==4) {
            return arrayList == null ? 0 : 4;
        }
        else {

            return arrayList == null ? 0 : 5;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profileImage;
LinearLayout root_layout;
        public ViewHolder(View itemView) {
            super(itemView);
            profileImage = (CircleImageView) itemView.findViewById(R.id.chat_profile_image);
            root_layout=itemView.findViewById(R.id.root_layout);

        }

    }
}
