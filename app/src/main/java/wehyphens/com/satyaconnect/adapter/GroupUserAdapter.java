package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.models.Group;
import wehyphens.com.satyaconnect.models.GroupDeatailsModel;
import wehyphens.com.satyaconnect.models.UserModelPoJo;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class GroupUserAdapter extends RecyclerView.Adapter<GroupUserAdapter.ViewHolder> {

    GroupDeatailsModel GroupDeatailsModel;
    List<UserModelPoJo> arrayList;
    Context context;
    private String imageurl="";
    private int user_id=0;


    public GroupUserAdapter(List<UserModelPoJo> arrayList, Context context, String imageurl){

        this.arrayList=arrayList;
        this.context=context;
        this.imageurl=imageurl;
        user_id=Integer.valueOf(CommonUtils.getPreferencesString(context,AppConstants.USER_ID));


    }

    @Override
    public GroupUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_chat_row_item, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(GroupUserAdapter.ViewHolder holder, int position) {

         holder.user_pic.setImageResource(R.drawable.group_icon);
        if(arrayList.get(position).getAvatar()!=null&&(!TextUtils.isEmpty(arrayList.get(position).getAvatar()))) {
            Picasso.with(context).load(imageurl+arrayList.get(position).getAvatar()).error(R.drawable.green_user).fit().centerCrop().into(holder.user_pic);
        }

        if(!TextUtils.isEmpty(arrayList.get(position).getFirstName())) {
            holder.userName.setText(arrayList.get(position).getFirstName());
        }
        holder.chatTitle.setText(arrayList.get(position).getMyStatus());
        if(arrayList.get(position).getCreated()!=null) {

            String date[]=arrayList.get(position).getCreated().split("//+");
            Log.e("date",date[0]);
            holder.lastSeen.setText(CommonUtils.getformattedDateWithTime(date[0]));


        }

        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (user_id == arrayList.get(position).getId()) {
                        Log.e("userMatchPositoio", "userMatchPositoio" + position);
                        }
                    else {
                        Intent intent=new Intent(new Intent(context, ChatActivity.class));
                        intent.putExtra(AppConstants.RECEIVER__PIC,imageurl+arrayList.get(position).getAvatar());
                        intent.putExtra(AppConstants.RECEIVER__USER_NAME2,arrayList.get(position).getFirstName());
                        intent.putExtra(AppConstants.RECEIVER_ID,String.valueOf(arrayList.get(position).getId()));
                        CommonUtils.savePreferencesString(context, AppConstants.RECEIVER_ID, String.valueOf(arrayList.get(position).getId()));
                        Log.e("receiver_id","recent_receiver_id"+arrayList.get(position).getId());
                        context.startActivity(intent);
                }
            }
        });


        if(position % 2==1){
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.row_bg));
        }
        else {
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));

        }

    }
    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView userName,chatTitle,lastSeen;
        RelativeLayout llUser;



        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);

        }
    }

    public void filterList(List<UserModelPoJo> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }

}