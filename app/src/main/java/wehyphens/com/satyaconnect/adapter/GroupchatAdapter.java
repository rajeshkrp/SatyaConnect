package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.data.StaticConfig;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.Group;
import wehyphens.com.satyaconnect.models.GroupDeatailsModel;
import wehyphens.com.satyaconnect.models.GroupDetailList;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class GroupchatAdapter extends RecyclerView.Adapter<GroupchatAdapter.ViewHolder> {

    GroupDeatailsModel GroupDeatailsModel;
    List<Group> arrayList;
    Context context;
    private String imageurl="",group_user="";

public GroupchatAdapter(List<Group> arrayList, Context context, String imageurl, String group_user){

    this.arrayList=arrayList;
    this.context=context;
    this.imageurl=imageurl;
    this.group_user=group_user;

}

    @Override
    public GroupchatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_chat_row_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(GroupchatAdapter.ViewHolder holder, int position) {

       // holder.user_pic.setImageResource(arrayList.get(position).getUser_pic());

        if(position % 2==1){
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.row_bg));
        }
        else {
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));

        }

        if(arrayList.get(position).getGroupDetail().getAvatar()!=null&&(!TextUtils.isEmpty(arrayList.get(position).getGroupDetail().getAvatar()))) {
            Picasso.with(context).load(imageurl+arrayList.get(position).getGroupDetail().getAvatar()).fit().centerCrop().error(R.drawable.group_icon).into(holder.user_pic);
        }

        if(!TextUtils.isEmpty(arrayList.get(position).getGroupDetail().getGroupName())) {
            holder.tv_groupName.setText(arrayList.get(position).getGroupDetail().getGroupName());
        }
    //    holder.chatTitle.setText(arrayList.get(position).getGroup().getStatus());
        if(arrayList.get(position).getGroupDetail().getCreated()!=null) {

            String date[]=arrayList.get(position).getGroupDetail().getCreated().split("//+");
            Log.e("date",date[0]);
            holder.lastSeen.setText(CommonUtils.getformattedDateWithTime(date[0]));

        }

        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    CommonUtils.savePreferencesString(context,AppConstants.USER_GROUP,"group");
                    Intent intent=new Intent(new Intent(context, ChatActivity.class));
                    intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());
                    intent.putExtra(AppConstants.GROUP_PIC,imageurl+arrayList.get(position).getGroupDetail().getAvatar());
                    intent.putExtra(AppConstants.GROUP_NAME,arrayList.get(position).getGroupDetail().getGroupName());
                    intent.putExtra(AppConstants.GROUP_ID,String.valueOf(arrayList.get(position).getGroupDetail().getId()));
                    intent.putExtra(AppConstants.GROUP__USER_NAME,group_user);
                    intent.putExtra(AppConstants.FROM_GROUP,"from_group");
                    context.startActivity(intent);
                    Log.e("node_id","node_id"+arrayList.get(position).getGroupDetail().getNode());
                    Log.e("group_user","group_user"+group_user);
                    Log.e("getIdnode_id","getIdnode_id:::"+String.valueOf(arrayList.get(position).getGroupDetail().getId()));

                    }
        });
    }
    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView userName,chatTitle,lastSeen,tv_groupName;
        RelativeLayout llUser;

        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);
            tv_groupName=itemView.findViewById(R.id.tv_groupName);

        }
    }

        public void filterList(List<Group> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }

    }
