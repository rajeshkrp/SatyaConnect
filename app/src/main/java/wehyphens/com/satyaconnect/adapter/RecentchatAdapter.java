package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.data.StaticConfig;
import wehyphens.com.satyaconnect.db.MyApplication;
import wehyphens.com.satyaconnect.db.database.AppDatabase;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.handler.ExecutorHandler;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class RecentchatAdapter extends RecyclerView.Adapter<RecentchatAdapter.ViewHolder> {

    DepartmentListDetailPojo departmentListDetailPojo;
    ExecutorService mExecutor;
    List<DepartmentListDetailPojo> arrayList;
      Context context;
      private AppDatabase mAppDb;
    private String imageurl="";

public RecentchatAdapter(List<DepartmentListDetailPojo> arrayList, Context context, String imageurl){

    this.arrayList=arrayList;
    this.context=context;
    this.imageurl=imageurl;
    mAppDb = MyApplication.getDb();
    mExecutor = ExecutorHandler.getInstance().getExecutor();


}

    @Override
    public RecentchatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_chat_row_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public int getItemViewType(int position) {
        return position;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecentchatAdapter.ViewHolder holder, int position) {

        if(position % 2==1){
            // holder.vView.setVisibility(View.GONE);
            // holder.ll_row_container.setBackgroundColor(Color.parseColor("#f1ecec"));
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.row_bg));

        }
        else {
            //  holder.vView.setVisibility(View.VISIBLE);
                holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));

        }

       // departmentListDetailPojo =arrayList.get(position);


    /*    holder.user_pic.setImageResource(arrayList.get(position).getUser_pic());
        holder.userName.setText(arrayList.get(position).getUserName());
        holder.chatTitle.setText(arrayList.get(position).getChattitle());
        holder.lastSeen.setText(arrayList.get(position).getLastSeen());*/

holder.user_pic.setImageResource(R.drawable.green_user);
        if(arrayList.get(position).getUser().getAvatar()!=null&&(!TextUtils.isEmpty(arrayList.get(position).getUser().getAvatar()))) {
            Picasso.with(context).load(imageurl+arrayList.get(position).getUser().getAvatar()).placeholder(R.drawable.green_user).fit().centerCrop()
                    .into(holder.user_pic);
        }



        if(!TextUtils.isEmpty(arrayList.get(position).getUser().getFirstName())) {
            holder.userName.setText(arrayList.get(position).getUser().getFirstName());
            holder.chatTitle.setText(arrayList.get(position).getUser().getMyStatus() + "");
        }

        if(arrayList.get(position).getUser().getCreated()!=null&&!TextUtils.isEmpty(arrayList.get(position).getUser().getCreated())){
          String date[]=arrayList.get(position).getUser().getCreated().split("//+");
            Log.e("date",date[0]);
            holder.lastSeen.setText(CommonUtils.getformattedDateWithTime(date[0]));

        }

        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(new Intent(context, ChatActivity.class));
                intent.putExtra(AppConstants.RECEIVER__PIC,imageurl+arrayList.get(position).getUser().getAvatar());
                intent.putExtra(AppConstants.RECEIVER__USER_NAME2,arrayList.get(position).getUser().getFirstName());
                intent.putExtra(AppConstants.RECEIVER_ID,String.valueOf(arrayList.get(position).getUser().getId()));
                CommonUtils.savePreferencesString(context, AppConstants.RECEIVER_ID, String.valueOf(arrayList.get(position).getId()));

                Log.e("receiver_id","recent_receiver_id"+arrayList.get(position).getUser().getId());
                context.startActivity(intent);
              //  saveUser();
               // findIDEmail(arrayList.get(position).getUser().getFirstName());
            }
        });

      /*  holder.user_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ChatProfileActivity.class));
            }
        });*/



    }

    private void saveUser() {
        UserInfoDto table = new UserInfoDto(
                String.valueOf(departmentListDetailPojo.getUser().getId()),
                departmentListDetailPojo.getUser().getFirstName(),
                departmentListDetailPojo.getUser().getAvatar(),
                departmentListDetailPojo.getUser().getMyStatus(),
                CommonUtils.getDate((System.currentTimeMillis())));

        mExecutor.execute(() -> mAppDb.userDao().insert(table));
    }

    private void findIDEmail(String email) {

        FirebaseDatabase.getInstance().getReference().child("users").orderByChild("name").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //email not found
                    new LovelyInfoDialog(context)
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_spinner2)
                            .setTitle("Fail")
                            .setMessage("Name not found")
                            .show();
                } else {


                    String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();

                    HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
                    Friend user = new Friend();
                    user.name = (String) userMap.get("name");
                    user.email = (String) userMap.get("email");
                    user.avata = (String) userMap.get("avata");
                    user.id = id;


                    addFriend(id, true);
                   // listFriendID.add(id);
                  //  dataListFriend.getListFriend().add(user);
                    FriendDB.getInstance(context).addFriend(user);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addFriend(final String idFriend, boolean isIdFriend) {
        if (idFriend != null) {
            if (isIdFriend) {
                FirebaseDatabase.getInstance().getReference().child("friend/" + StaticConfig.UID).push().setValue(idFriend)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    addFriend(idFriend, false);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_spinner2)
                                        .setTitle("False")
                                        .setMessage("False to add friend success")
                                        .show();
                            }
                        });
            } else {
                FirebaseDatabase.getInstance().getReference().child("friend/" + idFriend).push().setValue(StaticConfig.UID).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            addFriend(null, false);
                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_spinner2)
                                        .setTitle("False")
                                        .setMessage("False to add friend success")
                                        .show();
                            }
                        });
            }
        } else {
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_spinner2)
                    .setTitle("Success")
                    .setMessage("Add friend success")
                    .show();
        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView userName,chatTitle,lastSeen;
        RelativeLayout llUser;
       // LinearLayout ll_row_container;


        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);
          //  ll_row_container=itemView.findViewById(R.id.ll_row_container);
        }
    }

    public void filterList(List<DepartmentListDetailPojo> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }

   /* private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<DepartmentListDetailPojo> filterList=new ArrayList<DepartmentListDetailPojo>();
                for(int i=0;i<arrayList.size();i++){
                    if((filterList.get(i).getUser().getFirstName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        DepartmentListDetailPojo contacts = new DepartmentListDetailPojo();

                        for (DepartmentListDetailPojo s : arrayList) {
                            //if the existing elements contains the search input
                            if (s.getUser().getFirstName().toUpperCase().contains(constraint.toString().toUpperCase())) {
                                //adding the element to filtered list
                                filterList.add(s);
                            }
                        }

                        *//*contacts.(bothList.get(i).getUserName());
                        contacts.setUserMobile(bothList.get(i).getUserMobile());
                        filterList.add(contacts);
*//*
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=bothList.size();
                results.values=bothList;
            }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            arrayList=(ArrayList<DepartmentListDetailPojo>) results.values;
            notifyDataSetChanged();
        }
    }

*/

}
