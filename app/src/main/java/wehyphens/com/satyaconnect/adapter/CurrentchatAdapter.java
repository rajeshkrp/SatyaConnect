package wehyphens.com.satyaconnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import de.hdodenhof.circleimageview.CircleImageView;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.ChatActivity;
import wehyphens.com.satyaconnect.data.FriendDB;
import wehyphens.com.satyaconnect.data.StaticConfig;
import wehyphens.com.satyaconnect.db.MyApplication;
import wehyphens.com.satyaconnect.db.database.AppDatabase;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.handler.ExecutorHandler;
import wehyphens.com.satyaconnect.models.DepartmentListDetailPojo;
import wehyphens.com.satyaconnect.models.Friend;
import wehyphens.com.satyaconnect.models.LatestMessage;
import wehyphens.com.satyaconnect.models.Message;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class CurrentchatAdapter extends RecyclerView.Adapter<CurrentchatAdapter.ViewHolder> {

    LatestMessage LatestMessage;
    ExecutorService mExecutor;
    List<LatestMessage> arrayList;
    Context context;
    private AppDatabase mAppDb;
    private String imageurl = "", group_user_name = "", msgCount = "";
    private String user_id = "";

    public CurrentchatAdapter(List<LatestMessage> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;
        this.imageurl = imageurl;
        mAppDb = MyApplication.getDb();
        mExecutor = ExecutorHandler.getInstance().getExecutor();

        user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);

    }

    @Override
    public CurrentchatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.current_chat_row_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(CurrentchatAdapter.ViewHolder holder, int position) {

        LatestMessage = arrayList.get(position);

        if ((arrayList.get(position).getOponent_img() != null) && (!TextUtils.isEmpty(arrayList.get(position).getOponent_img()))) {
            Picasso.with(context).load(imageurl + arrayList.get(position).getOponent_img()).error(R.drawable.green_user).fit().noFade().centerCrop().into(holder.user_pic);
        }

        if (!TextUtils.isEmpty(arrayList.get(position).getOponent_name())) {
            //    holder.userName.setText(CommonUtils.NameCaps(arrayList.get(position).getOponent_name()));
            holder.userName.setText(arrayList.get(position).getOponent_name());
        }

        if (arrayList.get(position).getMsgCount() != 0) {
            holder.tvMsgCount.setVisibility(View.VISIBLE);
            holder.tvMsgCount.setText(arrayList.get(position).getMsgCount() + "");
        } else {
            holder.tvMsgCount.setVisibility(View.GONE);
        }

      /*  if(arrayList.get(position).countCheck){

        }


        else {
            if (arrayList.get(position).getMsgcount() != null) {
            //    Toast.makeText(context, "msg::"+arrayList.get(position).countCheck, Toast.LENGTH_SHORT).show();

            }
        }*/




         /*if (arrayList.get(position).getType().equalsIgnoreCase("0")) {

            if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
               // holder.chatTitle.setText("File");
                holder.ivImage.setVisibility(View.VISIBLE);

                holder.ivImage.setImageResource(R.drawable.unavailable);


            }

        }*/

        if (!TextUtils.isEmpty(arrayList.get(position).getType())) {
            if (arrayList.get(position).getType().equalsIgnoreCase("0")) {

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.ivImage.setVisibility(View.VISIBLE);
                    holder.tvMsgCount.setVisibility(View.GONE);
                    holder.ivImage.setImageResource(R.drawable.unavailable);
                    holder.chatTitle.setText(arrayList.get(position).getMessage());
                   // holder.chatTitle.setText("This message was deleted");
                }

            } else if (arrayList.get(position).getType().equalsIgnoreCase("1")) {

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.ivImage.setVisibility(View.GONE);
                    holder.chatTitle.setText(arrayList.get(position).getMessage() + "");
                }

            } else if (arrayList.get(position).getType().equalsIgnoreCase("2")) {

                holder.chatTitle.setText("Photos");
                holder.ivImage.setVisibility(View.VISIBLE);
                holder.ivImage.setImageResource(R.drawable.ic_camera_alt_black_24dp);

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                   /* holder.chatTitle.setText("Photos");
                    holder.ivImage.setVisibility(View.VISIBLE);
                    holder.ivImage.setImageResource(R.drawable.ic_camera_alt_black_24dp);*/
                }

            } else if (arrayList.get(position).getType().equalsIgnoreCase("3")) {

                holder.chatTitle.setText("File");
                holder.ivImage.setVisibility(View.VISIBLE);
                holder.ivImage.setImageResource(R.drawable.file);

              /*  if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.chatTitle.setText("File");
                    holder.ivImage.setVisibility(View.VISIBLE);
                    holder.ivImage.setImageResource(R.drawable.file);
                }*/

            }

        }

        if (arrayList.get(position).getTime() != null && !TextUtils.isEmpty(arrayList.get(position).getTime())) {
            String date[] = arrayList.get(position).getTime().split("-");
            Log.e("date", date[0]);
            holder.lastSeen.setText(date[1]);

        }


        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (arrayList.get(position).getChatType() != null && arrayList.get(position).getChatType().equalsIgnoreCase("Group")) {
                    Intent intent = new Intent(new Intent(context, ChatActivity.class));
                    // intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());
                    intent.putExtra(AppConstants.GROUP_PIC, imageurl + arrayList.get(position).getOponent_img());
                    intent.putExtra(AppConstants.GROUP_NAME, arrayList.get(position).getOponent_name());
                    intent.putExtra(AppConstants.GROUP_NODE, arrayList.get(position).getOponent_id());
                    intent.putExtra(AppConstants.GROUP_ID, arrayList.get(position).chatId);
                    intent.putExtra(AppConstants.FIREBASE_GROUP_ID, arrayList.get(position).group_id);
                    intent.putExtra(AppConstants.FROM_GROUP, "from_recent_group");
                    context.startActivity(intent);

                    Log.e("CurrentchatId", "CurrentchatId:::" + arrayList.get(position).chatId);
                    Log.e("Currentgroup_id:::", "Currentgroup_id:::" + arrayList.get(position).group_id);
                    CommonUtils.savePreferencesString(context, AppConstants.RECEIVER_ID, arrayList.get(position).getOponent_id());
                    Log.e("groupreceiver_id", "groupCurrent_receiver_id" + arrayList.get(position).getOponent_id());


                   /* LatestMessage changedMsg=new LatestMessage();
                    changedMsg.setMsgcount("0");
                    arrayList.set(position,changedMsg);
                    notifyDataSetChanged();*/

                    //  callUpdateMsgListUserGroup(position);

                } else {
                    Intent intent = new Intent(new Intent(context, ChatActivity.class));
                    intent.putExtra(AppConstants.RECEIVER__PIC, imageurl + arrayList.get(position).getOponent_img());
                    intent.putExtra(AppConstants.RECEIVER__USER_NAME2, arrayList.get(position).getOponent_name());
                    intent.putExtra(AppConstants.RECEIVER_ID, arrayList.get(position).getOponent_id());
                    context.startActivity(intent);
                    CommonUtils.savePreferencesString(context, AppConstants.RECEIVER_ID, arrayList.get(position).getOponent_id());

                    //  callUpdateMsgListUser(position);
                 /*   LatestMessage changedMsg=new LatestMessage();
                    changedMsg.setMsgcount("0");
                    arrayList.set(position,changedMsg);
                    notifyDataSetChanged();*/

                }
                Log.e("receiver_id", "Current_receiver_id" + arrayList.get(position).getOponent_id());

            }

        });


        if (position % 2 == 1) {
            // holder.vView.setVisibility(View.GONE);
            // holder.ll_row_container.setBackgroundColor(Color.parseColor("#f1ecec"));
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.view));


        } else {
            //  holder.vView.setVisibility(View.VISIBLE);
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));

        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        ImageView ivImage;
        TextView userName, chatTitle, lastSeen, tvMsgCount;
        RelativeLayout llUser;
        View vView;


        public ViewHolder(View itemView) {
            super(itemView);
            user_pic = itemView.findViewById(R.id.civ_user_pic);
            userName = itemView.findViewById(R.id.tv_userName);
            chatTitle = itemView.findViewById(R.id.tv_chat_title);
            lastSeen = itemView.findViewById(R.id.tv_last_seen);
            llUser = itemView.findViewById(R.id.llUser);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvMsgCount = itemView.findViewById(R.id.tvMsgCount);

        }
    }

    public void filterList(List<LatestMessage> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }

}
