package wehyphens.com.satyaconnect.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.fragments.RecentChatFragment;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;


public class PrivacyPolicyActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_agree_nd_continue, tv_term_of_services, tv_policies;
    LinearLayout btn_agree;
    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        mContext = PrivacyPolicyActivity.this;

        tv_agree_nd_continue = findViewById(R.id.tv_agree_nd_continue);
        tv_policies = findViewById(R.id.tv_policies);
        tv_term_of_services = findViewById(R.id.tv_term_of_services);
        btn_agree = findViewById(R.id.btn_agree);


        btn_agree.setOnClickListener(this);
        tv_term_of_services.setOnClickListener(this);
        tv_policies.setOnClickListener(this);
        tv_agree_nd_continue.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_term_of_services:
                CommonUtils.savePreferencesString(mContext, AppConstants.VIEW_TYPE, "term_of_services");
                startActivity(new Intent(mContext, HelpActivity.class));

                break;
            case R.id.tv_agree_nd_continue:
                CommonUtils.savePreferencesString(mContext, AppConstants.VIEW_TYPE, "privacy_policy");

                startActivity(new Intent(mContext, HelpActivity.class));
                break;

            case R.id.tv_policies:
                CommonUtils.savePreferencesString(mContext, AppConstants.VIEW_TYPE, "privacy_policy");
                startActivity(new Intent(mContext, HelpActivity.class));
                break;

            case R.id.btn_agree:
                startActivity(new Intent(mContext, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();

                break;
        }
    }
}
