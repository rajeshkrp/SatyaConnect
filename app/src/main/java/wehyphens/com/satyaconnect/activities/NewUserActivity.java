package wehyphens.com.satyaconnect.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.NewUserChatAdapter;
import wehyphens.com.satyaconnect.models.AllUserListDetailsModel;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;


public class NewUserActivity extends Activity {

    private TextView tvContactDetails, tvContactDetails2;
    private FloatingActionButton fabChat;
    ImageView iv_back;
    private RecyclerView rvUserList;
    private NewUserChatAdapter chatAdapter;
    private List<AllUserListDetailsModel> userList = new ArrayList<>();
    private Context mContext;
    private Activity activity;
    private int user_id = 0;
    private int[] imageList = {R.drawable.man_t, R.drawable.man_t, R.drawable.man_t, R.drawable.man_t};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_user);
        mContext = NewUserActivity.this;
        fabChat = findViewById(R.id.fabNewUser1);
        iv_back = findViewById(R.id.iv_back);
        rvUserList = findViewById(R.id.rvUserList);
        tvContactDetails2 = findViewById(R.id.tvContactDetails2);
        tvContactDetails = findViewById(R.id.tvContactDetails);
        chatAdapter = new NewUserChatAdapter(userList, mContext);
        rvUserList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
        rvUserList.setAdapter(chatAdapter);

        for (int i = 0; i < imageList.length; i++) {

            AllUserListDetailsModel detailsModel = new AllUserListDetailsModel();
            detailsModel.setUserPic(imageList[i]);
            userList.add(detailsModel);

        }

        fabChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, DashBoardActivity.class));
                CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, true);

            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

    }

}
