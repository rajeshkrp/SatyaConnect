package wehyphens.com.satyaconnect.activities;

import android.app.Notification;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import wehyphens.com.satyaconnect.R;

public class NotificationActivity extends AppCompatActivity {
    ImageView iv_back;
    CheckBox cb_msg_tone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        iv_back=findViewById(R.id.iv_back);
        cb_msg_tone=findViewById(R.id.cb_msg_tone);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cb_msg_tone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    cb_msg_tone.setSoundEffectsEnabled(false);

                }

            }
        });

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("Title")
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true);
    }

}
