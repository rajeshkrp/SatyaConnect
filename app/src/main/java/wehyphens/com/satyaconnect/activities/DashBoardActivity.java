package wehyphens.com.satyaconnect.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.fragments.CurrentChatFragment;
import wehyphens.com.satyaconnect.fragments.RoomChatFragment;
import wehyphens.com.satyaconnect.fragments.GroupsFragment;
import wehyphens.com.satyaconnect.fragments.RecentChatFragment;
import wehyphens.com.satyaconnect.fragments.SettingsFragment;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class DashBoardActivity extends AppCompatActivity {

    private TextView mTextMessage;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    private Context mContext;
    TextView tv_toolbar_title;
    ImageView iv_menu;
    Toolbar toolbar;
    private String profile_pic = "";
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        mContext = DashBoardActivity.this;

        CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, true);
        CommonUtils.hideKeyPad(DashBoardActivity.this);
        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);

        fragmentManager = getSupportFragmentManager();
        CurrentChatFragment currentChatFragment = new CurrentChatFragment();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_container, currentChatFragment);
        transaction.commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        CommonUtils.hide_keyboard((Activity) mContext);
        disableShiftMode(navigation);

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_recent_chat:
                    CurrentChatFragment recentChatFragment = new CurrentChatFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_container, recentChatFragment);
                    transaction.commit();

                    return true;
                case R.id.navigation_contact:
                    RecentChatFragment chatsFragment = new RecentChatFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_container, chatsFragment);
                    transaction.commit();

                    return true;
                case R.id.navigation_groups:

                    GroupsFragment groupsFragment = new GroupsFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_container, groupsFragment);
                    transaction.commit();

                    return true;
                case R.id.navigation_settings:
                    SettingsFragment settingsFragment = new SettingsFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.frame_container, settingsFragment);
                    transaction.commit();
                    return true;

            }
            return false;
        }
    };

    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //  item.setShiftingMode(false);
                item.setShifting(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            //Timber.e(e, "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            //Timber.e(e, "Unable to change value of shift mode");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, false);
                startActivity(new Intent(mContext, LoginActivity.class));
                //Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}


