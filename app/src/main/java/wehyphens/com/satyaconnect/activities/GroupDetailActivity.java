package wehyphens.com.satyaconnect.activities;


import android.content.Context;
import android.os.Bundle;

import android.support.design.widget.CollapsingToolbarLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.adapter.GroupUserAdapter;
import wehyphens.com.satyaconnect.adapter.GroupchatAdapter;
import wehyphens.com.satyaconnect.models.GRoupDetailModel;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.LatestMessage;
import wehyphens.com.satyaconnect.models.User;
import wehyphens.com.satyaconnect.models.UserModelPoJo;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class GroupDetailActivity extends AppCompatActivity {
    List<UserModelPoJo> groupList;
    List<UserModelPoJo> groupDetailsList;
    GroupUserAdapter recentchatAdapter;
    RecyclerView recy_contact_list;
    String group_id = "";
    String token = "";
    Context mContext;
    private ImageView ivGroupName;
    private String imageUrl = "";
    private TextView tvParticipateCount;
    CollapsingToolbarLayout ctl;
    private String fireBaseKey = "";
    private int user_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        mContext = GroupDetailActivity.this;

        user_id = Integer.valueOf(CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID));
        getToken();
        recy_contact_list = findViewById(R.id.recy_contact_list);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.z_toolbar);
        ivGroupName = (ImageView) findViewById(R.id.ivGroupName);
        tvParticipateCount = (TextView) findViewById(R.id.tvParticipateCount);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myToolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        ctl.setCollapsedTitleTextAppearance(R.style.coll_toolbar_title);
        ctl.setExpandedTitleTextAppearance(R.style.exp_toolbar_title);

        groupList = new ArrayList<>();
        groupDetailsList = new ArrayList<>();

        if (getIntent().getStringExtra("CHAT_GROUP_ID") != null) {
            group_id = getIntent().getStringExtra("CHAT_GROUP_ID");
            Log.e("CHAT_GROUP_ID", "CHAT_GROUP_ID::" + group_id);
        }
        token = CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE);
        Log.e("token", "token::" + token);
        getGroupData(group_id);

    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                return;
            }

        }

    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.z_menu, menu);
        return true;
    }*/


   /* private void dynamicToolbarColor() {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.homec);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(R.attr.colorPrimary));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.attr.colorPrimaryDark));

            }
        });
    }*/


    private void getGroupData(String group_id) {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getGroupMember(token, FirebaseInstanceId.getInstance().getToken(), group_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                int totalParticipant = 0;
                String str = null;
                try {
                    if (response != null) {
                        if (response.body() != null) {
                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //  Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");

                    CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, false);
                    //   CommonUtils.inActivieDailog(context);
                } catch (Exception e) {
                    //  Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;
                try {
                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 500) {
                    Log.e("send", "send::" + status);
                    // Log.e("send","Please provide your valid device token");
                    CommonUtils.snackBar(msg, ivGroupName);

                }


                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    // Log.e("send","Please provide your valid device token");
                    // CommonUtils.snackBar(msg, etEmail);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");

                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }


                try {

                    try {

                        if (str != null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // GRoupDetailModel group_detail_model = response.body();

                    GRoupDetailModel group_detail_model = new GRoupDetailModel();
                    Gson gson = new Gson();
                    group_detail_model = gson.fromJson(jsonTop.toString(), GRoupDetailModel.class);

                    if (group_detail_model != null) {
                        if (group_detail_model.getResponse() != null) {
                            if (group_detail_model.getResponse().getSuccess()) {
                                Log.e("MSG", group_detail_model.getResponse().getMessage());
                                groupList = group_detail_model.getResponse().getData().getGroup().get(0).getUser();
                                String groupName = group_detail_model.getResponse().getData().getGroup().get(0).getGroupDetail().getGroupName();
                                String imagePath = group_detail_model.getResponse().getData().getGroup().get(0).getGroupDetail().getAvatar();
                                //  Toast.makeText(GroupDetailActivity.this, "sucess::+"+group_detail_model.getResponse().getData().getGroup().get(0).getUser().size(), Toast.LENGTH_SHORT).show();

                                imageUrl = group_detail_model.getResponse().getImageurl();

                                ctl.setTitle(CommonUtils.NameCaps(groupName));
                                if (imagePath != null) {

                                    Picasso.with(mContext).load(imageUrl + imagePath).fit().centerCrop().error(R.drawable.group_icon).into(ivGroupName);
                                }

                                if (group_detail_model.getResponse().getData().getGroup().get(0).getUser() != null &&
                                        group_detail_model.getResponse().getData().getGroup().get(0).getUser().size() > 0) {
                                    totalParticipant = group_detail_model.getResponse().getData().getGroup().get(0).getUser().size();
                                    tvParticipateCount.setText(totalParticipant + " participants");
                                }


                                Log.e("gropsize", "gropsize1:::" + groupList.size());

                                if (groupList != null && groupList.size() > 0) {
                                    Log.e("gropsize", "gropsize2::" + groupDetailsList.size());

                                    //  UserModelPoJo changedMsg = null;

                                    for (int i = 0; i < groupList.size(); i++) {
                                        if (user_id == groupList.get(i).getId()) {
                                            // UserModelPoJo changedMsg = new UserModelPoJo();
                                            UserModelPoJo changedMsg = new UserModelPoJo();
                                            changedMsg.setFirstName("You");
                                            changedMsg.setMyStatus(groupList.get(i).getMyStatus());
                                            changedMsg.setAvatar(groupList.get(i).getAvatar());
                                            changedMsg.setId(groupList.get(i).getId());
                                            groupList.set(i, changedMsg);

                                           // groupList.add(i,changedMsg);

                                        }
                                    }

                                    Log.e("gropsizeLooout", "gropsizeLooout::" + groupList.size());
                                    recentchatAdapter = new GroupUserAdapter(groupList, mContext, imageUrl);
                                    recy_contact_list.setLayoutManager(new LinearLayoutManager(mContext));
                                    recy_contact_list.setAdapter(recentchatAdapter);
                                } else {
                                    CommonUtils.snackBar("No User Found", tvParticipateCount);
                                }
                            } else {
                                CommonUtils.snackBar(group_detail_model.getResponse().getMessage(), recy_contact_list);
                            }

                        } else {

                            CommonUtils.snackBar(msg, recy_contact_list);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
