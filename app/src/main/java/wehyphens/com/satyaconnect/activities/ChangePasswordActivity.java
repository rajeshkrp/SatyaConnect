package wehyphens.com.satyaconnect.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.ChangePassModel;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText et_con_pass, et_old_pass, et_new_pass;
    LinearLayout ll_change_pass;
    Context mContext;
    boolean isChecked = false;
    ImageView iv_back, iv_old_pass_show_hide_pass, iv_new_pass_show_hide_pass, iv_c_pass_show_hide_pass;
    private String fireBaseKey = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        mContext = ChangePasswordActivity.this;
        ll_change_pass = findViewById(R.id.ll_change_pass);
        iv_old_pass_show_hide_pass = findViewById(R.id.iv_old_pass_show_hide_pass);
        iv_new_pass_show_hide_pass = findViewById(R.id.iv_new_pass_show_hide_pass);
        iv_c_pass_show_hide_pass = findViewById(R.id.iv_c_pass_show_hide_pass);
        et_con_pass = findViewById(R.id.et_con_pass);
        et_old_pass = findViewById(R.id.et_old_pass);
        et_new_pass = findViewById(R.id.et_new_pass);
        iv_back = findViewById(R.id.iv_back);

        getToken();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        iv_old_pass_show_hide_pass.setOnClickListener((View v) -> {
            // do something here
            showHidePassword(et_old_pass, iv_old_pass_show_hide_pass);

        });

        iv_new_pass_show_hide_pass.setOnClickListener((View v) -> {

            showHidePassword(et_new_pass, iv_new_pass_show_hide_pass);
        });

        iv_c_pass_show_hide_pass.setOnClickListener((View v) -> {
            showHidePassword(et_con_pass, iv_c_pass_show_hide_pass);
        });


        ll_change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_old_pass.getText().toString().isEmpty()) {
                    CommonUtils.snackBar("Please Enter Old Password", et_old_pass);
                } else if (et_new_pass.getText().toString().isEmpty()) {
                    CommonUtils.snackBar("Please Enter New Password", et_old_pass);
                } else if (et_new_pass.getText().length() < 6) {
                    CommonUtils.snackBar("Password Must Be Atleast Six character", et_old_pass);
                } else if (et_con_pass.getText().toString().isEmpty()) {
                    CommonUtils.snackBar("Please Re Enter Password", et_old_pass);
                } else if (!et_con_pass.getText().toString().equals(et_new_pass.getText().toString())) {


                    CommonUtils.snackBar("Password does not match", et_old_pass);


                } else {
                    CommonUtils.showProgress(mContext);
                    change_pass();

                }


            }
        });

    }

    private void showHidePassword(EditText etPass, ImageView iv_show_hide_pass) {
        if (isChecked) {
            etPass.setTransformationMethod(new PasswordTransformationMethod());
            etPass.getSelectionEnd();
            etPass.setSelection(etPass.length());
            iv_show_hide_pass.setImageResource(R.drawable.ic_eye1);

            isChecked = false;
        } else {
            etPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etPass.getSelectionEnd();
            etPass.setSelection(etPass.length());

            iv_show_hide_pass.setImageResource(R.drawable.ic_eye);

            isChecked = true;
        }

    }


    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    private void change_pass() {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.resetPassword(CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE), et_old_pass.getText().toString(), et_new_pass.getText().toString(), FirebaseInstanceId.getInstance().getToken(), et_con_pass.getText().toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();
                String str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    // CommonUtils.inActivieDailog(mContext, jObjError.getString("message"));
                    CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, false);
                    //   CommonUtils.inActivieDailog(context);

                } catch (Exception e) {
                    //Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;
                try {
                    JSONObject json = new JSONObject(str);
                    msg = json.getString("message");
                    status = json.getInt("code");
                    Log.e("STATUS_CODE", status + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");


                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "You are Deactivated  by Admin");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }

                try {

                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ChangePassModel changePassModel = new ChangePassModel();
                    Gson gson = new Gson();
                    changePassModel = gson.fromJson(jsonTop.toString(), ChangePassModel.class);

                    if (changePassModel != null) {
                        if (changePassModel.getResponse() != null) {

                            if (changePassModel.getResponse().getSuccess()) {

                                Log.e("MSSG", changePassModel.getResponse().getMessage());
                                CommonUtils.snackBar(changePassModel.getResponse().getMessage() + "  Please Login", et_con_pass);
                                // Toast.makeText(ChangePasswordActivity.this,changePassModel.getResponse().getMessage()+"", Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(mContext, DashBoardActivity.class));
                                        finish();
                                    }
                                }, 2000);


                            } else {

                                CommonUtils.inActivieDailog(mContext, msg, "suspend");
                            }

                        } else {

                            CommonUtils.snackBar("Response null", et_con_pass);

                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
