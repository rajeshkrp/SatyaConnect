package wehyphens.com.satyaconnect.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;
import wehyphens.com.satyaconnect.utils.JustifiedTextView;
import wehyphens.com.satyaconnect.utils.TextJustification;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class HelpActivity extends AppCompatActivity {
    ImageView iv_back;
    Context mContext;
    TextView title;
    com.uncopt.android.widget.text.justify.JustifiedTextView tv_content;
    View vieew;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        mContext = HelpActivity.this;
        iv_back = findViewById(R.id.iv_back);
        tv_content = findViewById(R.id.tv_content);
        title = findViewById(R.id.title);
        vieew = findViewById(R.id.vieew);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        String view_type = CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE);
        if (CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE).equalsIgnoreCase("term_of_services")) {
            title.setText("Terms of service");

        } else if (CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE).equalsIgnoreCase("privacy_policy")) {
            title.setText("Privacy policies");
        } else if (CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE).equalsIgnoreCase("privacy_policy")) {
            title.setText("Privacy policies");
        } else if (CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE).equalsIgnoreCase("about_us")) {
            title.setText("About us");
        } else if (CommonUtils.getPreferencesString(mContext, AppConstants.VIEW_TYPE).equalsIgnoreCase("help")) {
            title.setText("Help");
        }
        if (view_type.equalsIgnoreCase("term_of_services")) {
            CommonUtils.showProgress(mContext);
            setViewsApi("1");

        } else if (view_type.equalsIgnoreCase("privacy_policy")) {
            CommonUtils.showProgress(mContext);
            setViewsApi("2");

        } else if (view_type.equalsIgnoreCase("privacy_policy")) {
            CommonUtils.showProgress(mContext);
            setViewsApi("2");

        } else if (view_type.equalsIgnoreCase("help")) {
            CommonUtils.showProgress(mContext);
            setViewsApi("4");
        } else if (view_type.equalsIgnoreCase("about_us")) {
            CommonUtils.showProgress(mContext);
            setViewsApi("3");
        }
    }


    private void setViewsApi(String page_id) {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getViews(page_id, FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                String str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //  Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");
                    CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, false);
                    //   CommonUtils.inActivieDailog(context);

                } catch (Exception e) {
                    // Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;
                try {
                    JSONObject json = new JSONObject(str);
                    msg = json.getString("message");
                    status = json.getInt("code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }
                try {
                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    HelpData helpData = new HelpData();
                    Gson gson = new Gson();
                    helpData = gson.fromJson(jsonTop.toString(), HelpData.class);
                    if (helpData != null) {
                        if (helpData.getResponse() != null) {

                            if (helpData.getResponse().getSuccess()) {

                                if (helpData.getResponse().getData().get(0).getContent() != null) {


                                    tv_content.setText(Html.fromHtml(helpData.getResponse().getData().get(0).getContent()));
                                    CommonUtils.dismissProgress();
                                    vieew.setVisibility(View.VISIBLE);

                                }
                            } else {

                                CommonUtils.snackBar(helpData.getResponse().getMessage(), tv_content);

                            }

                        } else {

                            CommonUtils.snackBar("Response null", tv_content);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }


}
