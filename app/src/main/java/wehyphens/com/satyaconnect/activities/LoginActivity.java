package wehyphens.com.satyaconnect.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.HttpException;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

import static android.view.View.GONE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_forgot_pass,tv_remember_pass,tv_login_please;
    LinearLayout ll_login;
    Context mContext;
    private EditText etEmail,etPass;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    LinearLayout rl_parent;
    String password="";
    ImageView iv_show_hide_pass;
    boolean isChecked=false;
    CheckBox check_password;
    private String fireBaseKey="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext=LoginActivity.this;
        getToken();
        rl_parent=findViewById(R.id.rl_parent);
        tv_forgot_pass=findViewById(R.id.tv_forgot_pass);
        tv_remember_pass=findViewById(R.id.tv_remember_pass);
        tv_login_please=findViewById(R.id.tv_login_please);
        etEmail=findViewById(R.id.etEmail);
        etPass=findViewById(R.id.etPass);
        ll_login=findViewById(R.id.ll_login);
        iv_show_hide_pass=findViewById(R.id.iv_show_hide_pass);
        check_password=findViewById(R.id.check_password);
        tv_forgot_pass.setOnClickListener(this);
        ll_login.setOnClickListener(this);
        // tv_forgot_pass.setOnClickListener(this);
        etPass.setOnClickListener(this);
        etEmail.setOnClickListener(this);
        iv_show_hide_pass.setOnClickListener(this);
        password=etPass.getText().toString();
        setHideSoftKeyboard(etEmail);
        setHideSoftKeyboard(etPass);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/avenir_proregular.otf");
        tv_forgot_pass.setTypeface(face);
        //   tv_login_please.setTypeface(face);
        tv_remember_pass.setTypeface(face);


        rl_parent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });

        CommonUtils.hideKeyPad(LoginActivity.this);
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v == etEmail) {
                    if (hasFocus) {
                        // Open keyboard

                        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(etEmail, InputMethodManager.SHOW_FORCED);
                    } else {
                        // Close keyboard

                        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(etEmail.getWindowToken(), 0);
                    }
                }
            }
        });



        etPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v == etPass) {
                    if (hasFocus) {
                        // Open keyboard

                        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(etPass, InputMethodManager.SHOW_FORCED);
                    } else {
                        // Close keyboard
                        ((InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(etPass.getWindowToken(), 0);
                    }
                }
            }
        });
    }


    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;

            }

        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tv_forgot_pass:
                startActivity(new Intent(mContext,ForgotPasswordActivity.class));
                finish();
                break;
            case R.id.ll_login:
                String email=etEmail.getText().toString();
                String pass=etPass.getText().toString();
                if(email.length()>0&&pass.length()>0)

                {
                    checkValidation();

                }
                else {
                    CommonUtils.snackBar("Please enter Employee id ",etPass);
                }

                // startActivity(new Intent(mContext, NewUserActivity.class));

                // checkValidation();

                break;

            case R.id.rl_parent:

                break;

            case R.id.etEmail:
                etEmail.setCursorVisible(true);

                break;

            case R.id.etPass:
                etPass.setCursorVisible(true);
                break;
            case R.id.iv_show_hide_pass:
                showHidePassword();
                break;
        }
    }
    private void setHideSoftKeyboard(EditText editText){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void calRegidterApi(String email, String pass,String deviceToken) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Log.e("tokenLogin::","tokenLogin::"+deviceToken);
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<ResponseBody> call = service.getLogin( email,pass,deviceToken);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();

                //  Toast.makeText(LoginActivity.this, "Login response"+response.body().toString(), Toast.LENGTH_SHORT).show();
                //  String  str = response.body().toString();
                String  str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    // CommonUtils.inActivieDailog(mContext,jObjError.getString("message"));
                    //   CommonUtils.inActivieDailog(context);

                } catch (Exception e) {
                    //   Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                String msg="";
                JSONObject jsonTop=null;
                int status = 0;
                try {
                    JSONObject json= new JSONObject(str);
                    msg = json.getString("message");
                    status = json.getInt("code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status==500){
                    // CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send","send::"+status);
                    // Log.e("send","Please provide your valid device token");
                    CommonUtils.snackBar(msg, etEmail);

                }

                if(status==409){
                    // CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send","send::"+status);
                    // Log.e("send","Please provide your valid device token");
                    // CommonUtils.snackBar(msg, etEmail);
                    CommonUtils.inActivieDailog(mContext,msg,"");

                }


                if(status==420){
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");

                }

                if(status==421){
                    if((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("send", "Please provide your valid device token");
                    }

                }



                try {

                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LoginModel  loginModel =new LoginModel();
                    Gson gson=new Gson();
                    loginModel= gson.fromJson(jsonTop.toString(),LoginModel.class);

                    if (loginModel != null) {
                        if (loginModel.getResponse()!=null) {

                            if (loginModel.getResponse().getSuccess()) {
                                Log.e("Authentication :: ",CommonUtils.getPreferences(mContext,AppConstants.LOGIN_AUTHENTICATE));
                                Intent i=new Intent(mContext,OtpVarificationActivity.class);
                                i.putExtra("EMP_ID",email);
                                i.putExtra("PASSWORD",pass);
                                i.putExtra("TOKEN",deviceToken);
                                startActivity(new Intent(i));
                                finish();
                                Log.e("tokenLogin","tokenLogin"+deviceToken);

                            } else {

                                // CommonUtils.inActivieDailog(mContext, msg, "suspend");
                                CommonUtils.snackBar(loginModel.getResponse().getData().getMessage(), etEmail);
                            } }
                        else {

                            CommonUtils.snackBar("Response null", etEmail);

                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                CommonUtils.dismissProgress();
                String msg="";
                if(t instanceof SocketTimeoutException){
                    msg="Please try again!";
                }
                else if (t instanceof IOException) {
                    msg = "Please check your internet connection!";
                } else {
                    msg ="Server exception please communicate with your admin!";
                }
                CommonUtils.snackBar(msg, etEmail);
               // Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("TAG", t.toString() + "");

            }
        });
    }
    private void showHidePassword(){
        if (isChecked) {
            etPass.setTransformationMethod(new PasswordTransformationMethod());
            etPass.getSelectionEnd();
            etPass.setSelection(etPass.length());
            iv_show_hide_pass.setImageResource(R.drawable.ic_eye1);
            //showPassowrd.setVisibility(View.VISIBLE);
            //showpasswordgraysignconfiram.setVisibility(View.GONE);
            isChecked = false;
        } else {
            etPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            etPass.getSelectionEnd();
            etPass.setSelection(etPass.length());

            iv_show_hide_pass.setImageResource(R.drawable.ic_eye);
            // showpasswordgraysignconfiram.setVisibility(View.VISIBLE);
            // showPassowrd.setVisibility(GONE);
            isChecked = true;
        }

    }
    private void checkValidation() {
        String email= etEmail.getText().toString();
        String pass= etPass.getText().toString();
        if (etEmail.getText().toString().length() ==0) {
            CommonUtils.snackBar("Please Enter Employee id.", etEmail);
        }
        else if (etPass.getText().toString().length()==0) {
            CommonUtils.hide_keyboard((Activity) mContext);
            CommonUtils.snackBar("Please Enter Password.", etEmail);
        }
        else {
            CommonUtils.showProgress(mContext);
            calRegidterApi(email,pass,FirebaseInstanceId.getInstance().getToken());
        }
    }}
