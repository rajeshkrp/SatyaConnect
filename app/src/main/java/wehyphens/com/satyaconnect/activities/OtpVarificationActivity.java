package wehyphens.com.satyaconnect.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.LoginData;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.models.MyProfilePOJO;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;
import wehyphens.com.satyaconnect.utils.Pinview;
import wehyphens.com.satyaconnect.utils.SmsReceiverM;

import static wehyphens.com.satyaconnect.utils.IConstants.IApp.ALL_PERMISSION;

public class OtpVarificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_resend, tvotpMsg;
    String emp_id, password, device_token;
    Context mContext;
    Activity mActivity;
    Pinview txt_pin_entry;

    String opt = "";
    private String getPinValue = "";
    BroadcastReceiver receiver;
    LinearLayout ll_login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_varification);
        txt_pin_entry = findViewById(R.id.pinview1);
        mContext = OtpVarificationActivity.this;
        mActivity = OtpVarificationActivity.this;
        tv_resend = findViewById(R.id.tv_dint_recieve);
        tvotpMsg = findViewById(R.id.tvotpMsg);
        ll_login = findViewById(R.id.ll_login);
        ll_login.setOnClickListener(this);
        CommonUtils.hide_keyboard((Activity) mContext);
        hideKeyBoard();
        CommonUtils.hideKeyPad(mActivity);
        if (getIntent() != null) {
            emp_id = getIntent().getStringExtra("EMP_ID");
            password = getIntent().getStringExtra("PASSWORD");
            device_token = getIntent().getStringExtra("TOKEN");
            Log.e("LOGIN_TIME_DATA", emp_id + " " + password + " " + device_token);
            // recieveOtp(emp_id,password,device_token);
        }
        getPermission();
        SmsReceiverM.bindListener(new SmsReceiverM.SmsListener() {
            @Override
            public void messageReceived(String msg) {
                CommonUtils.hide_keyboard((Activity) mContext);
                txt_pin_entry.setValue(msg);
                if (getPinValue.length() > 5)
                    recieveOtp(emp_id, password, device_token, msg);
            }
        });
        txt_pin_entry.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                CommonUtils.hide_keyboard((Activity) mContext);
                getPinValue = pinview.getValue();
                if (getPinValue.length() > 5)
                    CommonUtils.showProgress(mContext);
                    recieveOtp(emp_id, password, device_token, getPinValue);
            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideKeyBoard();
                CommonUtils.hideKeyPad(mActivity);
                CommonUtils.hide_keyboard((Activity) mContext);
                CommonUtils.snackBar("Resend OTP", tv_resend);
                CommonUtils.showProgress(mContext);
                calRegidterApi(emp_id, password, device_token);


                // Toast.makeText(mContext, "not recieve it"+txt_pin_entry.getText().toString(), Toast.LENGTH_SHORT).show();

            }
        });
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                tvotpMsg.setVisibility(View.GONE);
                tv_resend.setText("Resend sms after: " + millisUntilFinished / 1000 + " second");
            }

            public void onFinish() {
                tvotpMsg.setVisibility(View.VISIBLE);
                tv_resend.setText("Resend OTP");
            }
        }.start();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");
                    Log.e("messageRegister", message);

                }
            }
        };

    }

    private void recieveOtp(String user_name, String pass, String deviceToken, String otp) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        Log.e("ConfirmTicket", "ConfirmTicket" + otp);
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<ResponseBody> call = service.getOtp(user_name, pass, deviceToken, otp);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                CommonUtils.hideKeyPad((Activity) mContext);
                CommonUtils.hide_keyboard((Activity) mContext);
                CommonUtils.dismissProgress();
                Log.e("ConfirmTicketd", "ConfirmTicketdeviceToken" + deviceToken);

                String str = null;
                try {
                    if (response != null) {
                        if (response.body() != null) {
                            str = response.body().string();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {
                        CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");

                    }
                } catch (Exception e) {

                }

                String msg = "", imageurl = "", token = "", avatat = "", user_name = "", user_status = "", first_name = "", suecessmsg = "";
                boolean booleansatus = false;
                int id = 0;


                JSONObject jsonTop = null;
                JSONObject dataJsonObect = null;
                JSONObject alldataJsonObect = null;
                int status = 0;

                try {
                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (status == 500) {
                    // CommonUtils.logoutNotificationApi(context);
                    Log.e("send", "send::" + status);
                    // Log.e("send","Please provide your valid device token");
                    CommonUtils.snackBar(msg, tv_resend);

                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");
                    CommonUtils.snackBar("An Internal Server Error", tv_resend);

                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }


                try {


                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // LoginModel loginModel =new LoginModel();
                    Gson gson = new Gson();

                    try {
                        if (str != null) {

                            dataJsonObect = jsonTop.getJSONObject("response");

                            booleansatus = dataJsonObect.getBoolean("success");
                            imageurl = dataJsonObect.getString("imageurl");

                            alldataJsonObect = dataJsonObect.getJSONObject("data");

                            id = alldataJsonObect.getInt("id");
                            token = alldataJsonObect.getString("token");
                            user_name = alldataJsonObect.getString("username");
                            first_name = alldataJsonObect.getString("first_name");
                            avatat = alldataJsonObect.getString("avatar");
                            user_status = alldataJsonObect.getString("my_status");
                            suecessmsg = alldataJsonObect.getString("message");

                            if (booleansatus) {

                                //  CommonUtils.savePreferencesString(mContext, AppConstants.PASSWORD, pass);
                                // CommonUtils.savePreferencesString(mContext, AppConstants.Registerd_MOBILE_NO, etEmail.getText().toString());
                                CommonUtils.savePreferencesString(mContext, AppConstants.LOGIN_AUTHENTICATE, "bearer " + token);
                                CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, String.valueOf(id));
                                CommonUtils.savePreferencesString(mContext, AppConstants.PROFILE_PIC, imageurl + avatat);
                                CommonUtils.savePreferencesString(mContext, AppConstants.USER_NAME, first_name);
                                CommonUtils.savePreferencesString(mContext, AppConstants.USER_STATUS, user_status);

                                Log.e("Authentication :: ", CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE));
                                CommonUtils.snackBar(suecessmsg, tv_resend);


                                try {
                                    changePassDialog();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                       /* startActivity(new Intent(mContext, OtpVarificationActivity.class));
                                        finish();*/


                            } else {

                                CommonUtils.snackBar("Not Valid User.", tv_resend);

                            }

                        } else {

                            CommonUtils.snackBar("Response null", tv_resend);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    private void changePassDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = ((Activity) mContext).getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        // dialog.getWindow().setLayout(width, height);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialoge_change_pass);
        dialog.show();

        TextView change = dialog.findViewById(R.id.tv_change_pass);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(mContext, ChangePasswordActivity.class);
                intent.putExtra("StringName", password);
                startActivity(intent);
            }
        });
        TextView skip = dialog.findViewById(R.id.tv_skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                CommonUtils.hideKeyPad((Activity) mContext);
                CommonUtils.hide_keyboard((Activity) mContext);
                startActivity(new Intent(mContext, DashBoardActivity.class));
                finish();
            }
        });


    }


    private void calRegidterApi(String email, String pass, String deviceToken) {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        Log.e("tokenLogin::", "tokenLogin::" + deviceToken);
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<ResponseBody> call = service.getLogin(email, pass, deviceToken);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();

                Log.e("otpVerify::", "otpVerify::" + deviceToken);

                //  String  str = response.body().toString();
                String str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;


                try {
                    JSONObject json = new JSONObject(str);
                    msg = json.getString("message");
                    status = json.getInt("code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");


                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }


                try {

                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LoginModel loginModel = new LoginModel();
                    Gson gson = new Gson();
                    loginModel = gson.fromJson(jsonTop.toString(), LoginModel.class);
                    if (loginModel != null) {
                        if (loginModel.getResponse() != null) {

                            if (loginModel.getResponse().getSuccess()) {
                                Log.e("Authentication :: ", CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE));
                                Intent i = new Intent(mContext, OtpVarificationActivity.class);
                                i.putExtra("EMP_ID", email);
                                i.putExtra("PASSWORD", pass);
                                i.putExtra("TOKEN", deviceToken);
                                startActivity(new Intent(i));
                                finish();
                                Log.e("tokenLogin", "tokenLogin" + deviceToken);

                            } else {

                                CommonUtils.snackBar(loginModel.getResponse().getData().getMessage(), tv_resend);
                            }
                        } else {

                            CommonUtils.snackBar("Response null", tv_resend);

                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    private void getPermission() {
        List permission = new ArrayList();
        if (isPerGiven(Manifest.permission.RECEIVE_SMS)) {
        } else {
            permission.add(Manifest.permission.RECEIVE_SMS);
        }

        if (isPerGiven(Manifest.permission.READ_SMS)) {
        } else {
            permission.add(Manifest.permission.READ_SMS);
        }

        if (permission.size() > 0) {
            String[] str = new String[permission.size()];
            isPermissionRequired((String[]) permission.toArray(str));
        }
    }

    protected boolean isPermissionRequired(String[] requestPer) {
        List<String> requiredPerm = new ArrayList<>();
        for (String per : requestPer) {
            if (ContextCompat.checkSelfPermission(mContext, per) != PackageManager.PERMISSION_GRANTED) {
                requiredPerm.add(per);
            }
        }

        if (requiredPerm.size() > 0) {
            String[] perArray = new String[requiredPerm.size()];
            perArray = requiredPerm.toArray(perArray);
            ActivityCompat.requestPermissions(mActivity, perArray, ALL_PERMISSION);
        } else {
            return false;
        }
        return true;
    }

    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(mContext, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public void hideKeyBoard() {
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, new IntentFilter("otp"));

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_login:
                recieveOtp(emp_id, password, device_token, getPinValue);
                break;
        }
    }
}
