package wehyphens.com.satyaconnect.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.fragments.UpdateProfileFragment;
import wehyphens.com.satyaconnect.image_crop.CropActivity;
import wehyphens.com.satyaconnect.image_crop.GlobalAccess;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.MyProfilePOJO;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.MultipartFileUploader;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandlerImage;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv_back, iv_edit_emp_name, iv_edit_eid, iv_edit_status, iv_edit_email;
    CircleImageView civ_user_pic;
    TextView tv_emp_id, et_emp_name, et_status;
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String picturePath = "";
    private boolean isFileImg;
    View bottom_view;
    Context context;
    String mCurrentPhotoPath = "";
    ImageView civ_pro_pic_update, ivProgress;
    String imageClick = "";

    private ArrayList<String> imagelist = new ArrayList<>();
    private String profile_pic = "";

    Bitmap bitmap;
    String USER_ID = "";
    private String fireBaseKey = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context = ProfileActivity.this;
        getToken();

        profile_pic = CommonUtils.getPreferences(context, AppConstants.PROFILE_PIC);
        CommonUtils.hide_keyboard(ProfileActivity.this);
        ivProgress = findViewById(R.id.ivProgress);
        iv_back = findViewById(R.id.iv_back);
        iv_edit_emp_name = findViewById(R.id.iv_edit_emp_name);
        bottom_view = findViewById(R.id.bottom_view);
        tv_emp_id = findViewById(R.id.tv_emp_id);


        et_emp_name = findViewById(R.id.et_emp_name);
        civ_pro_pic_update = (ImageView) findViewById(R.id.civ_pro_pic_update);
        iv_edit_status = findViewById(R.id.iv_edit_status);
        et_status = findViewById(R.id.et_status);
        civ_user_pic = findViewById(R.id.civ_user_pic);


        iv_edit_emp_name.setOnClickListener(this);
        //  iv_edit_eid.setOnClickListener(this);
        civ_pro_pic_update.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        iv_edit_status.setOnClickListener(this);


        Animation animation1 =
                AnimationUtils.loadAnimation(context, R.anim.move);
        ivProgress.startAnimation(animation1);
        ivProgress.setVisibility(View.VISIBLE);

        if (getIntent().getStringExtra("FROM_CHAT_USER") != null && getIntent().getStringExtra("FROM_CHAT_USER").equalsIgnoreCase("FROM_CHAT_USER")) {


            if (getIntent().getStringExtra("FROM_CHAT_USER") != null) {
                USER_ID = getIntent().getStringExtra("CHAT_USER");
                Log.e("CHAT_USER::", "CHAT_USER::" + USER_ID);
                iv_edit_emp_name.setVisibility(View.GONE);
                civ_pro_pic_update.setVisibility(View.GONE);
                iv_edit_status.setVisibility(View.GONE);


                if(CommonUtils.isInternetAvailable(ProfileActivity.this))
                {
                    chatmy_profile(USER_ID);
                }else {
                    CommonUtils.noInternetConnection(ProfileActivity.this);
                }

            }


        } else {

            if (profile_pic != null && !TextUtils.isEmpty(profile_pic)) {
                Picasso.with(context).load(profile_pic).error(R.drawable.green_user).fit().noFade().centerCrop().into(civ_user_pic);
            } else {
                civ_user_pic.setImageResource(R.drawable.green_user);

            }

            if(CommonUtils.isInternetAvailable(ProfileActivity.this))
            {
                my_profile(USER_ID);
            }else {
                CommonUtils.noInternetConnection(ProfileActivity.this);
            }



        }


    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(context, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(context, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    private void my_profile(String user_id) {


        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        retrofit2.Call<ResponseBody> call = service.getMyProfile(CommonUtils.getPreferences(context, AppConstants.LOGIN_AUTHENTICATE), FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);

                String str = null;
                try {


                    if (response != null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {
                        // Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                        CommonUtils.inActivieDailog(context, jObjError.getString("message"), "inactive");

                    }
                } catch (Exception e) {
                    //Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;

                try {
                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (status == 500) {
                    Log.e("send", "send::" + status);
                    // Log.e("send","Please provide your valid device token");
                    CommonUtils.snackBar(msg, et_emp_name);

                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(context);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");
                    CommonUtils.snackBar("An Internal Server Error", et_emp_name);

                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(context, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(context, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }

                try {


                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MyProfilePOJO myProfilePOJO = new MyProfilePOJO();
                    Gson gson = new Gson();
                    myProfilePOJO = gson.fromJson(jsonTop.toString(), MyProfilePOJO.class);
                    if (myProfilePOJO != null) {
                        if (myProfilePOJO.getResponse() != null) {


                            myProfilePOJO.getResponse().getData().getFirstName();

                            if (myProfilePOJO.getResponse().getSuccess()) {

                                if (myProfilePOJO.getResponse().getData().getFirstName() != null) {
                                    et_emp_name.setText(myProfilePOJO.getResponse().getData().getFirstName() + "");
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_NAME, myProfilePOJO.getResponse().getData().getFirstName());

                                }
                                if (myProfilePOJO.getResponse().getData().getMyStatus() != null) {
                                    et_status.setText(myProfilePOJO.getResponse().getData().getMyStatus() + "");

                                    CommonUtils.savePreferencesString(context, AppConstants.USER_STATUS, myProfilePOJO.getResponse().getData().getMyStatus());

                                }
                                if (myProfilePOJO.getResponse().getData().getEmpId() != null) {
                                    tv_emp_id.setText(myProfilePOJO.getResponse().getData().getEmpId());
                                }


                                if (myProfilePOJO.getResponse().getData().getAvatar() != null) {

                                    String image = myProfilePOJO.getResponse().getImageurl() + myProfilePOJO.getResponse().getData().getAvatar();

                                    Log.e("NAME", myProfilePOJO.getResponse().getData().getFirstName());
                                    Log.e("Avtar", myProfilePOJO.getResponse().getImageurl() + myProfilePOJO.getResponse().getData().getAvatar());
                                    Log.e("Avtar2", image);
                                    Picasso.with(context).load(image).error(R.drawable.green_user).fit().noFade().centerCrop().into(civ_user_pic);
                                    //  Picasso.with(context).load(image).error(R.drawable.green_user).into(civ_user_pic);
                                    CommonUtils.savePreferencesString(context, AppConstants.PROFILE_PIC, myProfilePOJO.getResponse().getImageurl() + myProfilePOJO.getResponse().getData().getAvatar());

                                }

                                Log.e("user_id", myProfilePOJO.getResponse().getData().getEmpId());
                            } else {

                                CommonUtils.snackBar(msg, et_emp_name);

                            }

                        } else {

                            CommonUtils.snackBar(msg, et_emp_name);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_emp_name:
                CommonUtils.savePreferencesString(context, AppConstants.FNAME, "fname");
                Bundle bundle = new Bundle();
                bundle.putString("NAME", et_emp_name.getText().toString());
                bottom_view.setVisibility(View.GONE);
                UpdateProfileFragment fragment = new UpdateProfileFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.ll_profile_container, fragment);
                transaction.commit();

                break;

            case R.id.civ_pro_pic_update:
                openCameraGalleryDialog();
                break;
            case R.id.iv_back:
                finish();
                break;

            case R.id.iv_edit_status:

                CommonUtils.savePreferencesString(context, AppConstants.FNAME, "status");
                UpdateProfileFragment fragment2 = new UpdateProfileFragment();
                FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                transaction2.replace(R.id.ll_profile_container, fragment2);
                transaction2.commit();

                break;


        }
    }


    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraPermissionMethod() {


        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            imageClick = "Camera";
            activityForCamera();
        }
    }

    private void gallleryPermissionMethod() {
        imageClick = "Gallery";

        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }

    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }

    protected boolean checkPermission(String[] permission) {
        boolean isPermission = true;

        for (String s : permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this, s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }

    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(context, getApplicationContext()
                        .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList = getPackageManager()
                            .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "), REQUEST_GALLERY);
    }

    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                activityForGallery();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    imagelist.clear();
                    // imagelist.add(mCurrentPhotoPath);
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);

                    break;
                case REQUEST_GALLERY:
                    imagelist.clear();
                    galleryOperation(data);
                    //  uploadProfile();

                    Log.e("galleryIsha", picturePath);

                    break;
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {

                        imagelist.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");
                        imagelist.add(mCurrentPhotoPath);
                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        Log.e("numerofimages", imagelist.toString());
                        Log.e("sizenumerofimages", String.valueOf(imagelist.size()));
                        isFileImg = true;
                        ivProgress.setVisibility(View.VISIBLE);
                        uploadProfile();
                        Picasso.with(context)
                                .load("file://" + mCurrentPhotoPath)
                                .placeholder(R.drawable.green_user)
                                .error(R.drawable.green_user)
                                .into(civ_user_pic);

                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;

            }
        }
    }

    private void profileViewDialog(Bitmap image) {

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = ((Activity) context).getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.activity_chat_ui_profile);
        dialog.show();

        TextView done = dialog.findViewById(R.id.tv_done);
        TextView skip = dialog.findViewById(R.id.tv_skip);
        ImageView iv_profile_pic = dialog.findViewById(R.id.iv_profile_pic);
        iv_profile_pic.setImageBitmap(image);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                civ_user_pic.setImageBitmap(image);
                CommonUtils.showProgress(context);

                //   uploadProfile();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


    }

    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    private void uploadProfile() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("device_token", FirebaseInstanceId.getInstance().getToken());
        Map<String, String> headermap = new HashMap<String, String>();
        String header = "bearer " + CommonUtils.getPreferencesString(context, AppConstants.LOGIN_AUTHENTICATE);
        // headermap.put("Authorization",+"bearer"  CommonUtils.getPreferencesString(context, AppConstants.LOGIN_AUTHENTICATE));
        headermap.put("Authorization", header);

        MultipartFileUploader uploader = new MultipartFileUploader();
        uploader.uploadFile(params, imagelist, "avatar", context);
        FileUploadInterface service = RetrofitHandlerImage.getInstance().getApi();
        Call<ResponseBody> call = service.uploadFileProfile(headermap, uploader.getPartMap(), uploader.getPartBodyArr());
        //  Call<ResponseBody> call = service.uploadFileProfileile(uploader.getPartMap(), uploader.getPartBodyArr());


        for (String s : imagelist) {
            Log.e("madhuImage", s);
            Log.e("LOGIN_AUTHENTICATE", CommonUtils.getPreferencesString(context, AppConstants.LOGIN_AUTHENTICATE));
        }
        Log.e("jsonUpdateProfle", params.toString());
        Log.e("headermap", header);


        // Call<ResponseBody> call = service.uploadFileFromLib(uploader.getPartMap(),uploader.getPartBody());
        call.enqueue(new Callback<ResponseBody>() {
            public boolean status;

            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {

                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);
                String str = "";

                try {
                    if (response != null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(context, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {

                        CommonUtils.inActivieDailog(context, jObjError.getString("message"), "inactive");

                    }
                } catch (Exception e) {
                    //  Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                Log.e("retoImageupload", str);

                String msg = "";

                int error_status = 0;

                try {

                    JSONObject json = new JSONObject(str);
                    msg = json.getString("message");
                    error_status = json.getInt("code");
                    Log.e("MSG", "Profile_message" + msg);
                    Log.e("STATUS", "statua ---> " + error_status);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (error_status == 420) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.logoutDailog(context);
                    Log.e("send", "send::" + error_status);
                    Log.e("send", "Please provide your valid device token");

                }
                if (error_status == 409) {
                    // CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    // Log.e("send","Please provide your valid device token");
                    // CommonUtils.snackBar(msg, et_emp_name);
                    CommonUtils.inActivieDailog(context, msg, "inactive");


                }
                if (error_status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(context, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }
                try {
                    JSONObject jsonTop = new JSONObject(str);
                    JSONObject jsonObject = jsonTop.getJSONObject("response");
                    boolean status = jsonObject.getBoolean("success");
                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    String avatar = jsonData.getString("avatar");
                    String imageurl = jsonObject.getString("imageurl");
                    String message = jsonObject.getString("message");
                    String profileuser = imageurl + avatar;
                    Log.e("profile_pic", profileuser);

                    if (profileuser != null) {

                        CommonUtils.savePreferencesString(context, AppConstants.PROFILE_PIC, profileuser);

                    }
                    if (status) {

                        AppConstants.isProfileData = true;

                        CommonUtils.snackBar(message, et_emp_name);

                    } else {
                        // CommonUtils.snackBar(message,et_emp_name);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e("server exception", t.getMessage() + "");

                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                //   dialog.dismiss();
               /* String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = "Socket Time Out";
                } else if (t instanceof IOException) {
                    msg = "IO exception";

                } else if (t instanceof HttpException) {
                    msg = "http exception";
                } else {
                    msg = "EXCEPTION_MSG";
                }
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");*/
            }
        });
    }

    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(context, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }


    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        if (selectedImage == null) {
            Toast.makeText(context, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }

            if (picturePath != null) {
                GlobalAccess.setImagePath(picturePath);
                Intent i = new Intent(context, CropActivity.class);
                startActivityForResult(i, CROP_REQUEST_CODE);
            }
        }

    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

    }

    private void chatmy_profile(String user_id) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = service.getChatMyProfile(CommonUtils.getPreferences(context, AppConstants.LOGIN_AUTHENTICATE), user_id, FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                ivProgress.clearAnimation();
                ivProgress.setVisibility(View.GONE);

                String str = null;
                try {
                    if (response != null) {

                        if (response.body() != null) {
                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //  Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {
                        CommonUtils.inActivieDailog(context, jObjError.getString("message"), "inactive");

                    }

                } catch (Exception e) {
                    // Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;

                try {
                    if (str != null) {
                        JSONObject json = new JSONObject(str);
                        msg = json.getString("message");
                        status = json.getInt("code");
                        Log.e("MSG", "Profile_message" + msg);
                        Log.e("STATUS", "statua ---> " + status);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.logoutNotificationApi(context);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }


                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(context, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(context, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }

                try {

                    try {

                        if (str != null) {
                            jsonTop = new JSONObject(str);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MyProfilePOJO myProfilePOJO = new MyProfilePOJO();
                    Gson gson = new Gson();
                    myProfilePOJO = gson.fromJson(jsonTop.toString(), MyProfilePOJO.class);
                    if (myProfilePOJO != null) {
                        if (myProfilePOJO.getResponse() != null) {

                            myProfilePOJO.getResponse().getData().getFirstName();

                            if (myProfilePOJO.getResponse().getSuccess()) {

                                if (myProfilePOJO.getResponse().getData().getFirstName() != null) {
                                    et_emp_name.setText(myProfilePOJO.getResponse().getData().getFirstName() + "");

                                }
                                if (myProfilePOJO.getResponse().getData().getMyStatus() != null) {
                                    et_status.setText(myProfilePOJO.getResponse().getData().getMyStatus() + "");
                                }
                                if (myProfilePOJO.getResponse().getData().getEmpId() != null) {
                                    tv_emp_id.setText(myProfilePOJO.getResponse().getData().getEmpId());
                                }
                                if (myProfilePOJO.getResponse().getData().getAvatar() != null) {

                                    Picasso.with(context).load(myProfilePOJO.getResponse().getImageurl() + myProfilePOJO.getResponse().getData().getAvatar()).error(R.drawable.green_user).into(civ_user_pic);


                                }

                                Log.e("NAME", myProfilePOJO.getResponse().getData().getFirstName());
                                Log.e("Avtar", myProfilePOJO.getResponse().getImageurl() + myProfilePOJO.getResponse().getData().getAvatar());

                                Log.e("user_id", myProfilePOJO.getResponse().getData().getEmpId());

                            } else {

                                CommonUtils.snackBar(msg, et_emp_name);

                            }

                        } else {

                            CommonUtils.snackBar(msg, et_emp_name);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
