package wehyphens.com.satyaconnect.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.models.ForgetPasswordPOJO;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.retrofit.FileUploadInterface;
import wehyphens.com.satyaconnect.retrofit.RetrofitHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    TextView tv_back_to_login;
    private EditText et_emp_id;
    private LinearLayout btn_forgot_pass;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String fireBaseKey = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mContext = ForgotPasswordActivity.this;
        tv_back_to_login = findViewById(R.id.tv_back_to_login);
        et_emp_id = findViewById(R.id.et_emp_id);
        btn_forgot_pass = findViewById(R.id.btn_forgot_pass);

        getToken();
        tv_back_to_login.setOnClickListener(this);
        btn_forgot_pass.setOnClickListener(this);

    }

    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.USER_ID, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back_to_login:
                startActivity(new Intent(mContext, LoginActivity.class));
                break;

            case R.id.btn_forgot_pass:
                checkValidation();
                break;
        }

    }

    private void checkValidation() {

        String email = et_emp_id.getText().toString();

        if (et_emp_id.getText().toString().length() == 0) {
            CommonUtils.snackBar("Please enter valid Employee ID.", et_emp_id);
        } else {
            CommonUtils.showProgress(mContext);
            forgetPassword(et_emp_id.getText().toString());
            //  startActivity(new Intent(mContext, LoginActivity.class));

        }

    }

    private void forgetPassword(String emp_id) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<ResponseBody> call = service.forget_password(emp_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {


                CommonUtils.dismissProgress();

                String str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    // CommonUtils.inActivieDailog(mContext, jObjError.getString("message"));
                    // CommonUtils.savePreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN, false);
                    //   CommonUtils.inActivieDailog(context);

                } catch (Exception e) {
                    //  Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                String msg = "";
                JSONObject jsonTop = null;
                int status = 0;


                try {
                    JSONObject json = new JSONObject(str);
                    msg = json.getString("message");
                    status = json.getInt("code");
                    Log.e("STATUS_CODE", status + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");
                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                }


                try {
                    JSONObject jsonData = new JSONObject(str);
                    JSONObject json = jsonData.getJSONObject("response");
                    String user_msg = json.getString("message");
                    boolean user_status = json.getBoolean("success");
                    Log.e("STATUS_CODE", status + "");
                    if (user_status) {

                    } else {
                        CommonUtils.snackBar(user_msg, et_emp_id);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {

                    try {
                        jsonTop = new JSONObject(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ForgetPasswordPOJO forgetPassPOJO = new ForgetPasswordPOJO();
                    Gson gson = new Gson();
                    forgetPassPOJO = gson.fromJson(jsonTop.toString(), ForgetPasswordPOJO.class);

                    //countryList1.clear();
                    // countryCodeList.clear();
                    if (forgetPassPOJO != null) {
                        if (forgetPassPOJO.getResponse() != null) {

                            if (forgetPassPOJO.getResponse().getSuccess() != null) {

                                if (forgetPassPOJO.getResponse().getSuccess()) {

                                    if (forgetPassPOJO.getResponse().getMessage() != null) {

                                        CommonUtils.snackBar(forgetPassPOJO.getResponse().getMessage(), et_emp_id);

                                    }
                                } else {
                                    CommonUtils.snackBar(forgetPassPOJO.getResponse().getMessage(), et_emp_id);

                                }

                                // startActivity(new Intent(mContext, DashBoardActivity.class));
                                //finish();

                            } else {

                                //  CommonUtils.inActivieDailog(mContext, msg, "suspend");

                                CommonUtils.snackBar(forgetPassPOJO.getResponse().getMessage(), et_emp_id);

                            }

                        } else {

                            CommonUtils.snackBar("Response null", et_emp_id);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}
