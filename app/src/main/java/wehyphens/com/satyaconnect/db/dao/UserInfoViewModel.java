package wehyphens.com.satyaconnect.db.dao;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.ExecutorService;

import wehyphens.com.satyaconnect.db.MyApplication;
import wehyphens.com.satyaconnect.db.database.AppDatabase;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.handler.ExecutorHandler;
import wehyphens.com.satyaconnect.models.ChatMessageDto;

public class UserInfoViewModel extends AndroidViewModel {
    private LiveData<List<UserInfoDto>> deaprtmentList;
    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;



    private LiveData<List<UserInfoDto>> allChatObs;
    public UserInfoViewModel(@NonNull Application application) {
        super(application);
        mAppDb= MyApplication.getDb();
          deaprtmentList=mAppDb.userDao().getAll();

        mExecutor= ExecutorHandler.getInstance().getExecutor();

    }

    public LiveData<List<UserInfoDto>> getAllChatObs() {
        return deaprtmentList;
    }
}
