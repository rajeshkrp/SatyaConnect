package wehyphens.com.satyaconnect.db.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import wehyphens.com.satyaconnect.db.dao.UserDao;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;

import static wehyphens.com.satyaconnect.utils.IConstants.IDb.USER_DB_VERSION;


/**
 * Created by abul on 23/11/17.
 */

@Database(entities = {

        UserInfoDto.class},
        version = USER_DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();
}
