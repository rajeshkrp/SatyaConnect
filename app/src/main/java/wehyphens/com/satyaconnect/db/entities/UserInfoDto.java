package wehyphens.com.satyaconnect.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName ="User_Info" )
public class UserInfoDto {

    @PrimaryKey
    @NonNull
    private String userId;

    @Ignore
    public UserInfoDto(@NonNull String userId, String userName, String userPic, String userStatus, String userTime) {
        this.userId = userId;
        this.userName = userName;
        this.userPic = userPic;
        this.userStatus = userStatus;
        this.userTime = userTime;
    }

    private String userName;
    private String userPic;

    public UserInfoDto() {
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserTime() {
        return userTime;
    }

    public void setUserTime(String userTime) {
        this.userTime = userTime;
    }

    private String userMsg;
    private String userStatus;
    private String userTime;









}
