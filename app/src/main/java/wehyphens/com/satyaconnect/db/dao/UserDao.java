package wehyphens.com.satyaconnect.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import wehyphens.com.satyaconnect.db.entities.UserInfoDto;
import wehyphens.com.satyaconnect.utils.IConstants;

/**
 * Created by abul on 23/11/17.
 */

@Dao
public interface UserDao extends IConstants {

    @Query("SELECT * FROM User_Info")
    LiveData<List<UserInfoDto>> getAll();



    @Query("SELECT * FROM User_Info")
    Flowable<List<UserInfoDto>> getAllRx();

    @Query("SELECT * FROM User_Info WHERE userId = :uid")
    LiveData<UserInfoDto> getById(String uid);

    @Query("SELECT * FROM User_Info WHERE userId = :uid")
    Flowable<UserInfoDto> getByIdRx(String uid);


    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/



    @Query("UPDATE User_Info SET userMsg=:msg WHERE userId=:id")
    void updateData(String id, String msg);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserInfoDto> users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserInfoDto users);

    @Delete
    void delete(UserInfoDto user);

    @Query("DELETE FROM User_Info")
    public void nukeTable();
}

