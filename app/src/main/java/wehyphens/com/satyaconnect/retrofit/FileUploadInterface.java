package wehyphens.com.satyaconnect.retrofit;


import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import wehyphens.com.satyaconnect.models.ChangePassModel;
import wehyphens.com.satyaconnect.models.DepartmentPojo;
import wehyphens.com.satyaconnect.models.ForgetPasswordPOJO;
import wehyphens.com.satyaconnect.models.GRoupDetailModel;
import wehyphens.com.satyaconnect.models.GroupModel;
import wehyphens.com.satyaconnect.models.HelpData;
import wehyphens.com.satyaconnect.models.LoginModel;
import wehyphens.com.satyaconnect.models.MyProfilePOJO;
import wehyphens.com.satyaconnect.models.NotificationModel;
import wehyphens.com.satyaconnect.models.UserTokenModel;
import wehyphens.com.satyaconnect.retrofit.model.fcmRes.FcmResponseDto;
import wehyphens.com.satyaconnect.retrofit.model.notif.DataNotifDto;

public interface FileUploadInterface {


    @FormUrlEncoded
    @POST("users/token.json")
    Call<ResponseBody> getLogin(@Field("username") String username,
                              @Field("password") String password,
                              @Field("device_token") String device_token

    );


    @FormUrlEncoded
    @POST("users/token.json")
    Call<ResponseBody> getOtp(@Field("username") String username,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("otp") String otp
    );


    @FormUrlEncoded
    @POST("users/logout.json")
    Call<ResponseBody> getLogout(@Header("Authorization") String authorization,
                                 @Field("device_token") String device_token,
                                 @Field("type") String type
    );

    @FormUrlEncoded
    @POST("users/sendnotification.json")
    Call<ResponseBody> getNotification(@Header("Authorization") String authorization,
                                            @Field("type") String type,
                                            @Field("to") String to,
                                            @Field("message_body") String message_body,
                                            @Field("device_token") String device_token,
                                            @Field("title") String title);
    @FormUrlEncoded
    @POST("pages/view.json")
    Call<ResponseBody> getViews(@Field("page_id") String page_id,
                               @Field("device_token") String device_token);



    @FormUrlEncoded
    @POST("users/ChangePassword.json")
    Call<ResponseBody> resetPassword(@Header("Authorization") String authorization,
                                        @Field("current_password") String current_pass,
                                        @Field("password") String password,
                                        @Field("device_token") String device_token,
                                        @Field("verify_password") String verify_pass);


    @FormUrlEncoded
    @POST("users/mygroups.json")
    Call<ResponseBody> getGroup(@Header("Authorization") String authorization,
                              @Field("device_token") String device_token,
                              @Field("mygroups") String mygroups);


    @FormUrlEncoded
    @POST("users/mygroups.json")
    Call<ResponseBody> getGroupMember(@Header("Authorization") String authorization,
                                          @Field("device_token") String device_token,
                                           @Field("groups_id") String group_id);

    @FormUrlEncoded
    @POST("users/mydepartments.json")
    Call<ResponseBody> getDepartment(@Header("Authorization") String authorization,
                                       @Field("device_token") String device_token,
                                       @Field("mydepartments") String mydepartments);

    @POST("/fcm/send")
    Observable<FcmResponseDto> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body DataNotifDto message);






    @FormUrlEncoded
    @POST("userDetail.php")
   // Observable<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);
    Call<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/myprofile.json")
    Call<ResponseBody> getMyProfile(@Header("Authorization") String authorization,
                                     @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("users/myprofile.json")
    Call<ResponseBody> getChatMyProfile(@Header("Authorization") String authorization,
                                           @Field("users_id") String user_id,
                                         @Field("device_token") String device_token);




    @FormUrlEncoded
    @POST("users/view.json")
    Call<ResponseBody> checkUserStatus(@Header("Authorization") String authorization,
                                        @Field("users_id") String user_id);








    @Multipart
    @POST("users/saveprofile.json")
    Call<ResponseBody> uploadFileProfile(@HeaderMap() Map<String, String> headerMap,
                                            @PartMap() Map<String, RequestBody> partMap,
                                            @Part MultipartBody.Part[] file);


    @FormUrlEncoded
    @POST("users/saveprofile.json")
    Call<ResponseBody> updatefName(@Header("Authorization") String authorization,
                                         @Field("first_name") String fname,
                                       @Field("device_token") String device_token);



    @FormUrlEncoded
    @POST("users/saveprofile.json")
    Call<ResponseBody> updateStatus(@Header("Authorization") String authorization,
                                    @Field("my_status") String my_status,
                                     @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("users/forgetpassword.json")
    Call<ResponseBody> forget_password(@Field("emp_id") String emp_id);




 /*   @Multipart
    @POST("users/saveprofile.json")
    Call<ResponseBody> uploadFileProfileile(@HeaderMap() Map<String, String> headerMap,
                                            @PartMap() Map<String, RequestBody> partMap,
                                            @Part MultipartBody.Part[] file);*/






}
