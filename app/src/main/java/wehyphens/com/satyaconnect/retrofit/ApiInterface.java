package wehyphens.com.satyaconnect.retrofit;



import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import wehyphens.com.satyaconnect.models.UserTokenModel;

/**
 * Created by abul on 29/12/17.
 * "http://www.404coders.com/NetWrko/WebServices/chat/devicetoken_get.php"
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("/NetWrko/WebServices/chat/device_registration.php")
    Observable<ResponseBody> updateFcmKey(@Field("user_id") String user_id, @Field("token") String token);


    @FormUrlEncoded
    @POST("userDetail.php")
    Observable<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);









}
