package wehyphens.com.satyaconnect.retrofit;







import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import wehyphens.com.satyaconnect.retrofit.model.fcmRes.FcmResponseDto;
import wehyphens.com.satyaconnect.retrofit.model.notif.DataNotifDto;

/**
 * Created by abul on 14/11/17.
 */

public interface FcmApi {

    /*@POST("/fcm/send")
    Observable<ResponseBody> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body Message message);*/

    @POST("/fcm/send")
    Observable<FcmResponseDto> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body DataNotifDto message);
}
