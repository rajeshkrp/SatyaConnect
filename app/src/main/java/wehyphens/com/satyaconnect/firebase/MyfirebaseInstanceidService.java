package wehyphens.com.satyaconnect.firebase;

import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import wehyphens.com.satyaconnect.utils.AppConstants;
import wehyphens.com.satyaconnect.utils.CommonUtils;


/**
 * Created by mamta on 11-04-2017.
 */

public class MyfirebaseInstanceidService extends FirebaseInstanceIdService {

    private  static  final  String  TOKEN="gettoken";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token= FirebaseInstanceId.getInstance().getToken();
        Log.e(TOKEN,"Firebasetoken:::::::"+token);
        CommonUtils.savePreferencesString(MyfirebaseInstanceidService.this, AppConstants.FIREBASE_KEY,token);
    }
}
/*Legacy key*/
/*AIzaSyC_QW0rp1O2aWp32ee4g-dLECPyiJwMAbk*/