package wehyphens.com.satyaconnect.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import wehyphens.com.satyaconnect.R;
import wehyphens.com.satyaconnect.activities.DashBoardActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

          sendNotification(remoteMessage.getNotification().getBody());
        mContext=MyFirebaseMessagingService.this;

        Log.e("remoteMessage","remoteMessageBody::"+remoteMessage.getNotification().getBody());
        Log.e("remoteMessagegetData","remoteMessagegetData::"+remoteMessage.getData());

        String msg = "";
        String reply = "";
        Map<String, String> map = remoteMessage.getData();

        //  Log.e("remoteMessage","remoteMessage"+remoteMessage.toString());
        // Log.e("remoteMessage","remoteBody"+remoteMessage.getData());
//        Log.e("remoteMessage","remoteMessageBody"+remoteMessage.getNotification().getBody());
        /*map = new HashMap<Steering, String>();*/
        for (Map.Entry<String, String> entry : map.entrySet()) {
            /*Retrieve data as key:msg and value:json*/
            if (entry.getKey().trim().equals("msg")) {
                msg = entry.getValue();
            }
            if (entry.getKey().trim().equals("reply")) {
                reply = entry.getValue();
            }
        }


        for (Map.Entry<String, String> entry : map.entrySet()) {
            /*Retrieve data as key:msg and value:json*/
            if (entry.getKey().trim().equals("msg")) {
                msg = entry.getValue();
            }
        }


        //This method is only generating push notification
        //It is same as we did in earlier posts
    }

    private void sendNotification(String messageBody) {

        if(isNetworkAvailable(getApplicationContext())) {

            Intent resultIntent = new Intent(this , DashBoardActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                    0 /* Request code */, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setContentTitle("Satya Connects")
                    .setContentText(messageBody)
                    .setAutoCancel(false)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(resultPendingIntent);

            mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert mNotificationManager != null;
                mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
            assert mNotificationManager != null;
            mNotificationManager.notify(0 /* Request Code */, mBuilder.build());


            Intent intent = new Intent(this, DashBoardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.app_logo)
                    .setContentTitle("Satya Connects")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }



}
