package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetPasswordPOJO {

    @SerializedName("response")
    @Expose
    private ForgetPasswordeResponse response;

    public ForgetPasswordeResponse getResponse() {
        return response;
    }

    public void setResponse(ForgetPasswordeResponse response) {
        this.response = response;
    }

}
