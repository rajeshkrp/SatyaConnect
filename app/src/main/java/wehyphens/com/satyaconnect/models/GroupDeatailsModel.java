package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupDeatailsModel {
    @SerializedName("group")
    @Expose
    private List<Group> group = null;

    public List<Group> getGroup() {
        return group;
    }

    public void setGroup(List<Group> group) {
        this.group = group;
    }



}
