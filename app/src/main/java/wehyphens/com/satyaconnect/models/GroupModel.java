package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupModel {


    @SerializedName("response")
    @Expose
    private GroupResponseModel response;

    public GroupResponseModel getResponse() {
        return response;
    }

    public void setResponse(GroupResponseModel response) {
        this.response = response;
    }
}
