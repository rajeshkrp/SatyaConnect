package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyProfilePOJO {

    @SerializedName("response")
    @Expose
    private MyPojoResponse response;

    public MyPojoResponse getResponse() {
        return response;
    }

    public void setResponse(MyPojoResponse response) {
        this.response = response;
    }
}
