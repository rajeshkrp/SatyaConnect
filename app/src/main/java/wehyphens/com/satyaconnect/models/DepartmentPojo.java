package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.Response;

public class DepartmentPojo {

    @SerializedName("response")
    @Expose
    private DepartmentListPOJO response;

    public DepartmentListPOJO getResponse() {
        return response;
    }

    public void setResponse(DepartmentListPOJO response) {
        this.response = response;
    }

}
