package wehyphens.com.satyaconnect.models;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class MutableImage  extends ViewModel{
    private MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        stringMutableLiveData.setValue(image);
    }

    public MutableLiveData<String> getUpdateImage() {
        return stringMutableLiveData;
    }

    private  String image;
}
