package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {

    @SerializedName("group_detail")
    @Expose
    private GroupDetailList groupDetail;
    @SerializedName("user")
    @Expose
    private List<UserModelPoJo> user = null;

    public GroupDetailList getGroupDetail() {
        return groupDetail;
    }

    public void setGroupDetail(GroupDetailList groupDetail) {
        this.groupDetail = groupDetail;
    }

    public List<UserModelPoJo> getUser() {
        return user;
    }

    public void setUser(List<UserModelPoJo> user) {
        this.user = user;
    }




}
