package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpData {

    @SerializedName("response")
    @Expose
    private HelpResponse response;

    public HelpResponse getResponse() {
        return response;
    }

    public void setResponse(HelpResponse response) {
        this.response = response;
    }

}
