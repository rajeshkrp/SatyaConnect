package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 3/21/2018.
 */

public class LoginModel {

    @SerializedName("response")
    @Expose
    private LoginDetails response;

    public LoginDetails getResponse() {
        return response;
    }

    public void setResponse(LoginDetails response) {
        this.response = response;
    }
}


