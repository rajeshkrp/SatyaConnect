package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePassModel {
    @SerializedName("response")
    @Expose
    private ChangePassResponse response;

    public ChangePassResponse getResponse() {
        return response;
    }

    public void setResponse(ChangePassResponse response) {
        this.response = response;
    }
}
