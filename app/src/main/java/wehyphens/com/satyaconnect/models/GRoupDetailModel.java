package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GRoupDetailModel {

    @SerializedName("response")
    @Expose
    private GRoupDetailResponse response;

    public GRoupDetailResponse getResponse() {
        return response;
    }

    public void setResponse(GRoupDetailResponse response) {
        this.response = response;
    }


}
