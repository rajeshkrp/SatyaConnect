package wehyphens.com.satyaconnect.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepartmentListDetailPojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("departments_id")
    @Expose
    private Integer departmentsId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("department")
    @Expose
    private Department department;
    @SerializedName("user")
    @Expose
    private UserModelPoJo user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getDepartmentsId() {
        return departmentsId;
    }

    public void setDepartmentsId(Integer departmentsId) {
        this.departmentsId = departmentsId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public UserModelPoJo getUser() {
        return user;
    }

    public void setUser(UserModelPoJo user) {
        this.user = user;
    }

}






