package co.intentservice.chatui.models;

import android.net.Uri;
import android.text.format.DateFormat;

import java.util.concurrent.TimeUnit;


/**
 *
 * Chat Message model used when ChatMessages are required, either to be sent or received,
 * all messages that are to be shown in the chat-ui must be contained in this model.
 *
 */
public class ChatMessage {
    private String message;
    private String timestampStr;
    private long timestamp;
    private String filePath;

    public String getForwordStatus() {
        return forwordStatus;
    }

    public void setForwordStatus(String forwordStatus) {
        this.forwordStatus = forwordStatus;
    }

    private String forwordStatus;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private String fileName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;
    private String filesize;
    private String filetype;
    private Uri fileUri;
    private int readStatus;
    private int type;

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    private String imageType="";
    private String docType="";


    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }



    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }



    public ChatMessage(String message, long timestamp, int type){
        this.message = message;
        this.timestamp = timestamp;
        this.type = type;
    }

    public ChatMessage(String message, long timestamp, String filePath, int readStatus, int type,String forwordStatus) {
        this.message = message;
        this.timestamp = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
        this.forwordStatus=forwordStatus;
    }
    public ChatMessage(String userName,String message, long timestamp, String filePath, int readStatus, int type,String imageType ,String forwordStatus) {
        this.userName = userName;
        this.message = message;
        this.timestamp = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
        this.imageType = imageType;
        this.forwordStatus=forwordStatus;
    }

    public ChatMessage(String userName,String message, String timestamp, String filePath, int readStatus, int type) {
        this.userName = userName;
        this.message = message;
        this.timestampStr = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
    }

    public ChatMessage(String message, long timestamp, String filePath, int readStatus, int type,String fileType,String fileSize,String docType,String forwordStatus) {
        this.message = message;
        this.timestamp = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
        this.filetype = fileType;
        this.filesize = fileSize;
        this.fileName = fileName;
        this.docType = docType;
        this.forwordStatus=forwordStatus;
    }

    public ChatMessage(String userName,String message, long timestamp, String filePath, int readStatus, int type,String fileType,String fileSize,String docType,String forwordStatus) {
        this.userName = userName;
        this.message = message;
        this.timestamp = timestamp;
        this.filePath = filePath;
        this.readStatus = readStatus;
        this.type = type;
        this.filetype = fileType;
        this.filesize = fileSize;
        this.fileName = fileName;
        this.docType = docType;
        this.forwordStatus=forwordStatus;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getFormattedTime(){

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return DateFormat.format("hh:mm a", timestamp).toString();
    }
    public String getFormattedDate(){

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        int daysBetween = (int) (timeDifference / oneDayInMillis);
        if(daysBetween==0){
            return "Today";
        }else if(daysBetween==1){
            return "Yesterday";
        }else {
            return DateFormat.format("dd MMM yyyy", timestamp).toString();
        }
    }

    public interface Type{
        public static final int TYPE_VIEW_REC_MSG_TEXT_0 = 0;
        public static final int TYPE_VIEW_SEN_MSG_TEXT_1 = 1;
        public static final int TYPE_VIEW_REC_MSG_IMG_2 = 2;
        public static final int TYPE_VIEW_SEN_MSG_IMG_3 = 3;
        public static final int TYPE_VIEW_REC_MSG_VIDEO_4 = 4;
        public static final int TYPE_VIEW_SEN_MSG_VIDEO_5 = 5;
        public static final int TYPE_VIEW_REC_MSG_AUDIO_6 = 6;
        public static final int TYPE_VIEW_SEN_MSG_AUDIO_7 = 7;
        public static final int TYPE_VIEW_REC_MSG_DOC_8 = 8;
        public static final int TYPE_VIEW_SEN_MSG_DOC_9 = 9;
        public static final int TYPE_VIEW_ADMIN_ADD_DELETE = 10;
        public static final int TYPE_VIEW_REC_MSG_LINK_11 = 11;
        public static final int TYPE_VIEW_SEN_MSG_LINK_12 = 12;


    }
}
