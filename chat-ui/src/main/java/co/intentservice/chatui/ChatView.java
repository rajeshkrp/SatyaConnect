package co.intentservice.chatui;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.activity.AppConstants;
import co.intentservice.chatui.activity.CommonUtils;
import co.intentservice.chatui.activity.UIChatProfileActivity;
import co.intentservice.chatui.activity.VideoActivity;
import co.intentservice.chatui.adapters.GenericMultiViewAdapter;
import co.intentservice.chatui.databinding.AdminAddDeleteBinding;
import co.intentservice.chatui.databinding.ChatAudioItemRcvBinding;
import co.intentservice.chatui.databinding.ChatAudioItemSentBinding;
import co.intentservice.chatui.databinding.ChatDocItemRcvBinding;
import co.intentservice.chatui.databinding.ChatDocItemSentBinding;
import co.intentservice.chatui.databinding.ChatImageItemRcvBinding;
import co.intentservice.chatui.databinding.ChatImageItemSentBinding;
import co.intentservice.chatui.databinding.ChatTextItemRcvBinding;
import co.intentservice.chatui.databinding.ChatTextItemSentBinding;
import co.intentservice.chatui.databinding.ChatVideoItemRcvBinding;
import co.intentservice.chatui.databinding.ChatVideoItemSentBinding;
import co.intentservice.chatui.databinding.ChatViewBinding;

import co.intentservice.chatui.fab.FloatingActionsMenu;
import co.intentservice.chatui.lemda.Fun1ParamRet;
import co.intentservice.chatui.models.ChatMessage;
import madhu.MediaPlayerHandler;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;


public class ChatView extends RelativeLayout {

    private static final int FLAT = 0;
    private static final int ELEVATED = 1;
    AlertDialog.Builder builder;
    private ChatViewBinding binding;
    private CardView inputFrame;
    private RecyclerView chatListView;
    private EditText inputEditText;
    private AppCompatImageView ivCamera, ivAttachment, ivRecordVoice;
    private boolean statusAudiio;
    private boolean statusDelete;
    ArrayList<Integer> indexList=new ArrayList<Integer>();
    public  int rowSelectionCount=0;
    private int selectedRowColor=1;
    private FloatingActionsMenu actionsMenu;
    private RelativeLayout inputLayout;
    private boolean previousFocusState = false, useEditorAction, isTyping;
    private TextView textView,tvChatDate;



    public ChatViewBinding getBinding() {
        return binding;
    }

    private Runnable typingTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (isTyping) {
                isTyping = false;
                if (typingListener != null) typingListener.userStoppedTyping();
            }
        }
    };
    private TypingListener typingListener;
    private OnSentMessageListener onSentMessageListener;
    private DeteteListener onDeteteListener;
    private OnAttachmentClickListener attachmentListener;
    private EditTextClickListener editTextClickListener;

    private OnEmojiClickListener emojiClickListener;
    private GenericMultiViewAdapter<
            ChatTextItemRcvBinding, ChatTextItemSentBinding, ChatImageItemRcvBinding, ChatImageItemSentBinding,
            ChatVideoItemRcvBinding, ChatVideoItemSentBinding, ChatAudioItemRcvBinding, ChatAudioItemSentBinding,
            ChatDocItemRcvBinding, ChatDocItemSentBinding,AdminAddDeleteBinding,
            ChatMessage> chatViewListAdapter;

    private int inputFrameBackgroundColor, backgroundColor;
    private int inputTextSize, inputTextColor, inputHintColor;
    private int sendButtonBackgroundTint, sendButtonIconTint;

    private float bubbleElevation;
    private int bubbleBackgroundRcv, bubbleBackgroundSend; // Drawables cause cardRadius issues. Better to use background color
    private Drawable sendButtonIcon, buttonDrawable;
    private TypedArray attributes, textAppearanceAttributes;
    private Context context;
    private ImageView emoji_btn, ivUserPic;

    private  boolean isSlected;
    private int positionIndex=0;
    private int deleteIndex=0;


   public ChatView(Context context) {
        this(context, null);
    }


    public ChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);

    }


    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.chat_view, this, true);
//        LayoutInflater.from(getContext()).inflate(R.layout.chat_view, this, true);
        this.context = context;
        initializeViews();
        getXMLAttributes(attrs, defStyleAttr);
        setViewAttributes();
        setListAdapter();
        setButtonClickListeners();
        setUserTypingListener();
        setUserStoppedTypingListener();
        setUserClickListener();
      //  setDeleteClickListener(false);
        deslectbackground();
    }

    private void deslectbackground() {

    }


    private void initializeViews() {
        chatListView = (RecyclerView) findViewById(R.id.chat_list);
        inputFrame = (CardView) findViewById(R.id.input_frame);
        inputEditText = (EditText) findViewById(R.id.input_edit_text);
        actionsMenu = (FloatingActionsMenu) findViewById(R.id.sendButton);
        ivAttachment = findViewById(R.id.ivAttach);
        ivRecordVoice = (AppCompatImageView) findViewById(R.id.ivRecordVoice);
        ivCamera = (AppCompatImageView) findViewById(R.id.ivCam);
        emoji_btn = (ImageView) findViewById(R.id.emoji_btn);
        ivUserPic = (ImageView) findViewById(R.id.ivUserPic);
        inputLayout = findViewById(R.id.rlInput);

        tvChatDate=findViewById(R.id.tvChatDate);
        tvChatDate.animate().translationY(-tvChatDate.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));

        final View rootView = findViewById(R.id.rootView);
        builder = new AlertDialog.Builder(context);

        animateDate();

    }

    private boolean isVisible=false;
    private void animateDate(){
        chatListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("drag",newState+"" );
                if(newState==0){
                    //Log.e("drag",newState+"00000" );
                }

                if(newState==SCROLL_STATE_IDLE){
                    isVisible=false;

                    tvChatDate.animate().translationY(-tvChatDate.getHeight())
                            .setInterpolator(new AccelerateInterpolator(1));
                }else {
                    if(isVisible){
                        return;
                    }
                    isVisible=true;
                    tvChatDate.animate().translationY(0).setInterpolator
                            (new DecelerateInterpolator(1)).start();
                }
            }
        });

       /* chatListView.addOnScrollListener(new HidingScrollListener() {

            @Override
            public void onHide()
            {


            }

            @Override
            public void onShow()
            {

            }
        });*/
    }

    private void getXMLAttributes(AttributeSet attrs, int defStyleAttr) {
        attributes = context.obtainStyledAttributes(attrs, R.styleable.ChatView, defStyleAttr, R.style.ChatViewDefault);
        getChatViewBackgroundColor();
        getAttributesForBubbles();
        getAttributesForInputFrame();
        getAttributesForInputText();
        getAttributesForSendButton();
        getUseEditorAction();
        attributes.recycle();
    }

    Fun1ParamRet<Integer, Integer> fun = (pos) -> {
        return chatViewListAdapter.getList().get(pos).getType();
    };

    private void setListAdapter() {

        LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
       manager.setStackFromEnd(true);

        chatListView.setLayoutManager(manager);


        chatViewListAdapter = new GenericMultiViewAdapter<
                ChatTextItemRcvBinding, ChatTextItemSentBinding, ChatImageItemRcvBinding, ChatImageItemSentBinding,
                ChatVideoItemRcvBinding, ChatVideoItemSentBinding, ChatAudioItemRcvBinding, ChatAudioItemSentBinding,
                ChatDocItemRcvBinding, ChatDocItemSentBinding,AdminAddDeleteBinding,
                ChatMessage>(
                context,
                R.layout.chat_text_item_rcv,
                R.layout.chat_text_item_sent,
                R.layout.chat_image_item_rcv,
                R.layout.chat_image_item_sent,
                R.layout.chat_video_item_rcv,
                R.layout.chat_video_item_sent,
                R.layout.chat_audio_item_rcv,
                R.layout.chat_audio_item_sent,
                R.layout.chat_doc_item_rcv,
                R.layout.chat_doc_item_sent,
                R.layout.admin_add_delete, fun) {
            @Override
            public void commonFun(int position, List<ChatMessage> mList) {
                String date=mList.get(position).getFormattedDate();

                if(!tvChatDate.getText().toString().trim().equalsIgnoreCase(date)){
                    if(!date.contains("1970")){
                        tvChatDate.setText(date);
                    }
                }

            }

            @Override
            public void customFun0(ChatTextItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                if(mList.get(position).getFilePath()!=null&&!mList.get(position).getFilePath().equalsIgnoreCase(""))
                {
                    if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                        binding.ivDelete.setVisibility(VISIBLE);
                    }
                }
                else {
                    binding.ivDelete.setVisibility(GONE);
                }

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }

                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.ivForwordedview.setVisibility(VISIBLE);
                }else {
                    binding.ivForwordedview.setVisibility(GONE);
                }


                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if(mList.get(position).getFilePath()!=null){
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }

                        if(statusDelete){
                          //  binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position,mList.get(position).getDocType(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                            final Handler handler = new Handler();
                          //  binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                           /* handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                                    // Do something after 5s = 5000ms
                                }
                            }, 4000);*/
                        }

                        }
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                });

                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.RECEIVER__PIC);

                if (mList.get(position).getReadStatus() == 1) {
                    binding.tvReciverName.setVisibility(VISIBLE);
                    binding.tvReciverName.setText(CommonUtils.NameCaps(mList.get(position).getUserName()));
                } else {
                    binding.tvReciverName.setVisibility(GONE);
                }

                Picasso.with(context)
                        .load("file://" + profilepic)
                        .placeholder(R.drawable.placeholder_user)
                        .error(R.drawable.placeholder_user)
                        .into(binding.ivUserPic);
            }

            @Override
            public void customFun1(ChatTextItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                if(mList.get(position).getFilePath()!=null&&!mList.get(position).getFilePath().equalsIgnoreCase(""))
                {
                    if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                        binding.ivDelete.setVisibility(VISIBLE);
                    }
                }
                else {
                    binding.ivDelete.setVisibility(GONE);
                }

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }

                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.ivForwordedview.setVisibility(VISIBLE);
                }else {
                    binding.ivForwordedview.setVisibility(GONE);
                }



                if(mList.get(position).getForwordStatus()!=null){
                if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                    binding.ivForwordedview.setVisibility(VISIBLE);
                }else {
                    binding.ivForwordedview.setVisibility(GONE);
                }}
                //  TODO For long pres click listener

                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;

                        if (mList.get(position).getFilePath()!=null&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }

                        if(statusDelete){
                           // binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            selectedRowColor=0;
                            typingListener.userDeleteClick(position, mList.get(position).getDocType(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                           // binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            final Handler handler = new Handler();

                            }

                        return false;
                        }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                    });

                //  TODO For seting Details

                if(mList.get(position).getTimestamp()>System.currentTimeMillis()) {


                    //Toast.makeText(context, "time" + CommonUtils.getDate(System.currentTimeMillis()), Toast.LENGTH_SHORT).show();
                }

                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                Picasso.with(context)
                        .load("file://" + profilepic)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .placeholder(R.drawable.placeholder_user)
                        .error(R.drawable.placeholder_user)
                        .into(binding.ivUserPic);
            }

            @Override
            public void customFun2(ChatImageItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));


                if(mList.get(position).getForwordStatus()!=null){
                    if(mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.ivForwordedview.setVisibility(VISIBLE);
                    }else {
                        binding.ivForwordedview.setVisibility(GONE);
                    }
                }
                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                            if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, mList.get(position).getImageType(), view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);

                            }
                        return false;
                    }
                });
                Log.e("imageRece", "imageRece" + mList.get(position).getFilePath());
                if (mList.get(position).getReadStatus() == 1) {
                    binding.tvReciverName.setVisibility(VISIBLE);
                    binding.tvReciverName.setText(CommonUtils.NameCaps(mList.get(position).getUserName()));
                } else {
                    binding.tvReciverName.setVisibility(GONE);
                }
                if (mList.get(position).getFilePath() != null && !TextUtils.isEmpty(mList.get(position).getFilePath())) {
                    Picasso.with(context).load(mList.get(position).getFilePath()).error(R.drawable.preview_image).fit().centerCrop().into(binding.ivChat);
                }



                binding.flBackground.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(new Intent(context, UIChatProfileActivity.class));
                        // intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());

                        intent.putExtra("PATH",mList.get(position).getFilePath());
                        intent.putExtra("NAME",  mList.get(position).getUserName());
                        intent.putExtra("TIME",  mList.get(position).getFormattedTime());
                        intent.putExtra("TIMESTAMP",mList.get(position).getTimestamp());
                        intent.putExtra("READSATUS",   mList.get(position).getReadStatus());
                        context.startActivity(intent);
                        CommonUtils.savePreferencesString(context, AppConstants.RECEIVER_ID, mList.get(position).getUserName());
                        Log.e("groupreceiver_id", "groupCurrent_receiver_id" + mList.get(position).getReadStatus());
                        Log.e("groupCurrent_USErName", "groupCurrent_ReceiverName" + mList.get(position).getUserName());
                        //  Log.e("chatId", "chatId" + arrayList.get(position).chatId);
                    }
                });

            }

            @Override
            public void customFun3(ChatImageItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                    if(forwordStatus.equalsIgnoreCase("1")){
                        binding.ivForwordedview.setVisibility(VISIBLE);
                    }else {
                        binding.ivForwordedview.setVisibility(GONE);
                    }

                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                           // binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            selectedRowColor=0;
                            typingListener.userDeleteClick(position, mList.get(position).getImageType(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }
                        return false;
                    }
                });

                String path = "https://firebasestorage.googleapis.com/v0/b/satyaconnectapp.appspot.com/o/SatyaImage%2FSatyaImage?alt=media&token=1045c19c-3172-4b08-9a12-11cb4908dc69";
                Log.e("imagesend", "imagesend::" + mList.get(position).getFilePath());
                Log.e("imagesend", "imagesend::" + mList.get(position).getFilePath());
                if (mList.get(position).getFilePath() != null && !TextUtils.isEmpty(mList.get(position).getFilePath())) {
                  //  Picasso.with(context).load((mList.get(position).getFilePath())).into(binding.ivChat);
                    Picasso.with(context).load(mList.get(position).getFilePath()).error(R.drawable.preview_image).fit().centerCrop().into(binding.ivChat);
                    //   Picasso.with(context).load(path).into(binding.ivChat);
                }
                binding.flBackground.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(new Intent(context, UIChatProfileActivity.class));
                        // intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());
                        intent.putExtra("PATH",mList.get(position).getFilePath());
                        intent.putExtra("NAME",  mList.get(position).getUserName());
                        intent.putExtra("TIMESTAMP",mList.get(position).getTimestamp());
                        intent.putExtra("TIME",  mList.get(position).getFormattedTime());
                        intent.putExtra("READSATUS",   mList.get(position).getReadStatus());

                        Log.e("groupCurrent_USErName", "groupCurrent_USErName" + mList.get(position).getUserName());
                        Log.e("groupreceiver_id", "groupCurrent_receiver_id" + mList.get(position).getReadStatus());
                        Log.e("groupCurrent_USErName", "groupCurrent_USErName" + mList.get(position).getUserName());
                        context.startActivity(intent);
                    }
                });

            }

            @Override
            public void customFun4(ChatVideoItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                binding.play.setOnClickListener(v -> {
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath", mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });

                // binding. videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
                //  binding. videoView.setVideoPath(mList.get(position).getFilePath());
                //  binding. videoView.start();
                // Picasso.with(context).load(mList.get(position).getFilePath()).into(binding.VideoView);
            }

            @Override
            public void customFun5(ChatVideoItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                binding.play.setOnClickListener(v -> {
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath", mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });
            }

            @Override
            public void customFun6(ChatAudioItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));


                MediaPlayerHandler playerHandler = new MediaPlayerHandler();
                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        statusAudiio = true;
                        if (statusAudiio) {
                            playerHandler.startPlaying();
                            ;
                            statusAudiio = false;
                        } else {
                            playerHandler.pausePlaying();
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.blackpause);

                        }
                    }
                });


                playerHandler.setSeekLisner(mList.get(position)
                        .getFilePath(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                        binding.progessSeekBar.setMax(setLength);
                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        binding.progessSeekBar.setProgress(length);
                        binding.remiainTime.setText(length);
                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        //binding.progessSeekBar.setSta(isPlaying);

                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {

                        binding.totalTime.setText("" + duration);

                    }
                });
            }

            @Override
            public void customFun7(ChatAudioItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                // MediaPlayerHandler playerHandler=MediaPlayerHandler.getInstance();
                MediaPlayerHandler playerHandler = new MediaPlayerHandler();
                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        statusAudiio = true;
                        if (statusAudiio) {
                            playerHandler.startPlaying();
                            statusAudiio = false;
                        } else {
                            playerHandler.pausePlaying();

                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);

                        }
                    }
                });


                binding.poetDetailPlayBtn.setOnClickListener(v -> {
                    playerHandler.startPlaying();

                });
                playerHandler.setSeekLisner(mList.get(position).getFilePath(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                        binding.progessSeekBar.setMax(setLength);
                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        binding.progessSeekBar.setProgress(length);
                        binding.remiainTime.setText(length);
                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        //binding.progessSeekBar.setSta(isPlaying);
                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {
                        binding.totalTime.setText("" + duration);

                    }
                });
            }

            @Override
            public void customFun8(ChatDocItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                    if(forwordStatus.equalsIgnoreCase("1")){
                        binding.ivForwordedview.setVisibility(VISIBLE);
                    }
                    else {
                        binding.ivForwordedview.setVisibility(GONE);
                    }

                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, mList.get(position).getDocType(), view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }


                       /* statusDelete=true;
                        if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position);

                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));

                                    // Do something after 5s = 5000ms

                                }
                            }, 4000);
                        }*/
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                });

                if (mList.get(position).getReadStatus() == 1) {
                    binding.tvReciverName.setVisibility(VISIBLE);
                    binding.tvReciverName.setText(CommonUtils.NameCaps(mList.get(position).getUserName()));

                } else {

                    binding.tvReciverName.setVisibility(GONE);
                }
                String path = mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename = path.substring(path.lastIndexOf("/") + 1);
                binding.tvFileNmae.setText(mList.get(position).getMessage());
                binding.tvSize.setText(mList.get(position).getFilesize());


                binding.llDownload.setOnClickListener(v -> {
                    try {
                        openFile(context, Uri.parse(mList.get(position).getFilePath()),mList.get(position).getFiletype(),mList.get(position).getFilePath(),mList.get(position).getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });
                //  binding.tvType.setText(mList.get(position).getFiletype());
                Log.e("docpath", path);

                binding.llPdf.setOnClickListener(v -> {
                    try {
                        openFile(context, Uri.parse(mList.get(position).getFilePath()),mList.get(position).getFiletype(),mList.get(position).getFilePath(),mList.get(position).getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });

            }

            @Override
            public void customFun9(ChatDocItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                    if(forwordStatus.equalsIgnoreCase("1")){
                        binding.ivForwordedview.setVisibility(VISIBLE);
                    }
                    else {
                        binding.ivForwordedview.setVisibility(GONE);
                    }


                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){

                            selectedRowColor=0;
                            typingListener.userDeleteClick(position, mList.get(position).getDocType(),view);

                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);

                            final Handler handler = new Handler();
                          //  binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            /*handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));

                                    // Do something after 5s = 5000ms

                                }
                            }, 4000);*/
                        }
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                });

                String path = mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename = path.substring(path.lastIndexOf("/") + 1);
                binding.tvFileNmae.setText(mList.get(position).getMessage());
                binding.tvSize.setText(mList.get(position).getFilesize());

                binding.llDownload.setOnClickListener(v -> {

                    try {
                        openFile(context, Uri.parse(mList.get(position).getFilePath()),mList.get(position).getFiletype(),mList.get(position).getFilePath(),mList.get(position).getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });

                //   binding.tvType.setText(mList.get(position).getFiletype());

                Log.e("docpath", path);
                Log.e("docpath", path);
                Log.e("docpath", path);
                binding.llPdf.setOnClickListener(v -> {
                    try {
                        openFile(context, Uri.parse(mList.get(position).getFilePath()),mList.get(position).getFiletype(),mList.get(position).getFilePath(),mList.get(position).getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });


                //TODO for setting DeleteClick
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if(statusDelete){
                          //  binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));

                            selectedRowColor=0;
                            typingListener.userDeleteClick(position, mList.get(position).getDocType(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);

                            final Handler handler = new Handler();

                           /* handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));

                                    // Do something after 5s = 5000ms

                                }
                            }, 4000);*/
                        }
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                });


            }

            @Override
            public void customFun10(AdminAddDeleteBinding binding, int position, List<ChatMessage> mList) {

              //  Toast.makeText(context, "chatview ::"+mList.get(position).getMessage(), Toast.LENGTH_SHORT).show();

             //   binding.messageTextView.setText(mList.get(position).getMessage());
                binding.messageTextView.setText(mList.get(position).getMessage());
                Log.e("time",mList.get(position).getTimestamp()+"  "+mList.get(position).getFormattedDate());
                
            }
        };
//                bubbleBackgroundRcv, bubbleBackgroundSend, bubbleElevation);




        chatListView.setAdapter(chatViewListAdapter);
    }
    public int count(int pressCount){

        return pressCount;
    }
        public  void openFile(Context context, Uri uri,String url,String path,String fileName) throws IOException {
            // Create URI



            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url= "http://" + url;
            }
            
            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {


                Toast.makeText(context, "You  may have not proper app for viewing this content", Toast.LENGTH_SHORT).show();
                // Word document
                }

            else if(!url.toString().contains(".pdf")) {
                Toast.makeText(context, "You  may have not proper app for viewing this content", Toast.LENGTH_SHORT).show();

            }
            else if(url.toString().contains(".pdf")) {
                // PDF file
                try {
                    intent.setDataAndType(uri, "application/pdf");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } catch (Exception e) {

                    e.printStackTrace();
                }

                //  createImageFile();
              //   String[] filename=path.split("\\.");

                //Toast.makeText(context, "name::"+filename[0], Toast.LENGTH_SHORT).show();
               // saveFileInDownload(path,filename[0]);
                saveFileInDownload(path,fileName);

            }



            else if(url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            else if(url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }

            else if(url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if(url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }
        }

    private void saveFileInDownload(String path, String fileName) {
         DownloadManager downloadManager;
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(path);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, fileName);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS.toString(), fileName);
        Long reference = downloadManager.enqueue(request);
    }
    public File createImageFile() throws IOException {
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = context.getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, context.getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".pdf",         /* suffix */
                appFile      /* directory */
        );

        return image;
    }


    private void setViewAttributes() {
        setChatViewBackground();
        setInputFrameAttributes();
        setInputTextAttributes();
        setSendButtonAttributes();
        setUseEditorAction();
    }

    private void getChatViewBackgroundColor() {
        backgroundColor = attributes.getColor(R.styleable.ChatView_backgroundColor, -1);
    }

    private void getAttributesForBubbles() {

        float dip4 = context.getResources().getDisplayMetrics().density * 4.0f;
        int elevation = attributes.getInt(R.styleable.ChatView_bubbleElevation, ELEVATED);
        bubbleElevation = elevation == ELEVATED ? dip4 : 0;

        bubbleBackgroundRcv = attributes.getColor(R.styleable.ChatView_bubbleBackgroundRcv, ContextCompat.getColor(context, R.color.white));
        bubbleBackgroundSend = attributes.getColor(R.styleable.ChatView_bubbleBackgroundSend, ContextCompat.getColor(context, R.color.default_bubble_color_send));
    }


    private void getAttributesForInputFrame() {
        inputFrameBackgroundColor = attributes.getColor(R.styleable.ChatView_inputBackgroundColor, -1);
    }

    private void setInputFrameAttributes() {
        inputFrame.setCardBackgroundColor(inputFrameBackgroundColor);
    }

    private void setChatViewBackground() {
        this.setBackgroundColor(backgroundColor);
    }

    private void getAttributesForInputText() {
        setInputTextDefaults();
        if (hasStyleResourceSet()) {
            setTextAppearanceAttributes();
            setInputTextSize();
            setInputTextColor();
            setInputHintColor();
            textAppearanceAttributes.recycle();
        }
        overrideTextStylesIfSetIndividually();
    }

    private void setTextAppearanceAttributes() {
        final int textAppearanceId = attributes.getResourceId(R.styleable.ChatView_inputTextAppearance, 0);
        textAppearanceAttributes = getContext().obtainStyledAttributes(textAppearanceId, R.styleable.ChatViewInputTextAppearance);
    }

    private void setInputTextAttributes() {
        inputEditText.setTextColor(inputTextColor);
        inputEditText.setHintTextColor(inputHintColor);
        inputEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, inputTextSize);
    }

    private void getAttributesForSendButton() {
        sendButtonBackgroundTint = attributes.getColor(R.styleable.ChatView_sendBtnBackgroundTint, -1);
        sendButtonIconTint = attributes.getColor(R.styleable.ChatView_sendBtnIconTint, Color.WHITE);
        sendButtonIcon = attributes.getDrawable(R.styleable.ChatView_sendBtnIcon);
    }

    private void setSendButtonAttributes() {
        actionsMenu.getSendButton().setColorNormal(sendButtonBackgroundTint);
        actionsMenu.setIconDrawable(sendButtonIcon);
        actionsMenu.setMinimumHeight(30);
        actionsMenu.setMinimumWidth(30);



        buttonDrawable = actionsMenu.getIconDrawable();
        actionsMenu.setButtonIconTint(sendButtonIconTint);
    }

    private void getUseEditorAction() {
        useEditorAction = attributes.getBoolean(R.styleable.ChatView_inputUseEditorAction, false);
    }

    private void setUseEditorAction() {
        if (useEditorAction) {
            setupEditorAction();
        } else {
            inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }
    }

    private boolean hasStyleResourceSet() {
        return attributes.hasValue(R.styleable.ChatView_inputTextAppearance);
    }

    private void setInputTextDefaults() {
        inputTextSize = context.getResources().getDimensionPixelSize(R.dimen.default_input_text_size);
        inputTextColor = ContextCompat.getColor(context, R.color.black);
        inputHintColor = ContextCompat.getColor(context, R.color.main_color_gray);
    }

    private void setInputTextSize() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextSize)) {
            inputTextSize = attributes.getDimensionPixelSize(R.styleable.ChatView_inputTextSize, inputTextSize);
        }
    }

    private void setInputTextColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextColor)) {
            inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        }
    }

    private void setInputHintColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputHintColor)) {
            inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
        }
    }

    private void overrideTextStylesIfSetIndividually() {
        inputTextSize = (int) attributes.getDimension(R.styleable.ChatView_inputTextSize, inputTextSize);
        inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
    }

    private void setupEditorAction() {
        inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);
        inputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    long stamp = System.currentTimeMillis();
                    String message = inputEditText.getText().toString();

                    if (!TextUtils.isEmpty(message)) {
                        sendMessage(message, stamp, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setButtonClickListeners() {

        actionsMenu.getSendButton().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (actionsMenu.isExpanded()) {
                    actionsMenu.collapse();
                    return;
                }

                long stamp = System.currentTimeMillis();

                String message = inputEditText.getText().toString();
                if (!TextUtils.isEmpty(message)) {

                    // Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();
                    sendMessage(message, stamp, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                }

            }
        });

        actionsMenu.getSendButton().setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                actionsMenu.expand();
                return true;
            }
        });

        ivCamera.setOnClickListener(view -> attachmentListener.onCamClick(true));
        ivAttachment.setOnClickListener(view -> attachmentListener.onCamClick(false));
        emoji_btn.setOnClickListener(view -> emojiClickListener.onEmojiClick(true));
       // inputEditText.setOnClickListener(view -> editTextClickListener.clicked(true));
    }

    private void setUserTypingListener() {
        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {

                    if (!isTyping) {
                        isTyping = true;
                        if (typingListener != null) typingListener.userStartedTyping();
                    }

                    removeCallbacks(typingTimerRunnable);
                    postDelayed(typingTimerRunnable, 1500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUserStoppedTypingListener() {

        inputEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (previousFocusState && !hasFocus && typingListener != null) {
                    typingListener.userStoppedTyping();
                }
                previousFocusState = hasFocus;
            }
        });
    }

    private void setUserClickListener() {

        ivRecordVoice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                typingListener.userClick();

            }
        });
    }


    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
        return super.addViewInLayout(child, index, params);
    }

    public String getTypedMessage() {
        return inputEditText.getText().toString();
    }

    public void setTypingListener(TypingListener typingListener) {
        this.typingListener = typingListener;
    }

    public void setOnSentMessageListener(OnSentMessageListener onSentMessageListener) {
        this.onSentMessageListener = onSentMessageListener;
    }


    public void setOnDeleteListener(DeteteListener onSentMessageListener) {
        this.onDeteteListener = onSentMessageListener;
    }

    public void setEmojiClickListener(OnEmojiClickListener emojiClickListener) {
        this.emojiClickListener = emojiClickListener;
    }

    public void setAttachmentListener(OnAttachmentClickListener attachmentListener) {
        this.attachmentListener = attachmentListener;
    }

    public void sendMessage(String message, long stamp, int msgType) {

        ChatMessage chatMessage = new ChatMessage(message, stamp, msgType);
        if (onSentMessageListener != null && onSentMessageListener.sendMessage(chatMessage)) {
            chatViewListAdapter.addMessage(chatMessage);
            chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
            inputEditText.setText("");
        }
    }


    public void addMessage(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
       // chatListView.scrollToPosition(0);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }
    public void setAdapter(int deletePosition, ChatMessage chatMessage) {
        chatViewListAdapter.getList().set(deletePosition,chatMessage);
        chatViewListAdapter.notifyDataSetChanged();

       // chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);

    }








    public void addMessageImg(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void addMessages(ArrayList<ChatMessage> messages) {
        chatViewListAdapter.addMessages(messages);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void removeMessage(int position) {

      //  Toast.makeText(context, "deleteitem::"+position, Toast.LENGTH_SHORT).show();
        chatViewListAdapter.removeMessage(position);
     //   chatViewListAdapter.notifyDataSetChanged();
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }


    public void clearMessages() {
        chatViewListAdapter.clearMessages();
    }

    public EditText getInputEditText() {
        return inputEditText;
    }

    public FloatingActionsMenu getActionsMenu() {
        return actionsMenu;
    }

    public void hideInputLay() {
        inputLayout.setVisibility(View.GONE);
    }

    public void showInputLay() {
        inputLayout.setVisibility(View.VISIBLE);
    }

    public void addMessage(String path, long timestamp, int typeViewSenMsgImg3) {
    }

   /* @Override
    public void detete(int position) {

        Toast.makeText(context, "delete Interface", Toast.LENGTH_SHORT).show();

    }*/


    public interface TypingListener {

        void userStartedTyping();

        void userStoppedTyping();

        void userClick();
        //void userDeleteClick(int indexList, String b);
        void userDeleteClick(int indexList, String b,View view);
    }

    public interface OnSentMessageListener {
        boolean sendMessage(ChatMessage chatMessage);
    }

    public interface OnAttachmentClickListener {
        void onCamClick(boolean isCam);
    }

    public interface EditTextClickListener{
        void clicked(boolean isOpen);
    }

    public interface OnEmojiClickListener {
        void onEmojiClick(boolean emoji);
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }


    public interface DeteteListener {

        void detete(int position);
    }


}