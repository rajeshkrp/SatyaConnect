package co.intentservice.chatui.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.github.chrisbanes.photoview.OnMatrixChangedListener;
import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.OnSingleFlingListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import co.intentservice.chatui.R;
import co.intentservice.chatui.utils.Util;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UIChatProfileActivity extends AppCompatActivity  {
    Context mContext;
    String user_name="",filePath="",time="";
    int userstatus=0;
    TextView tvUser,tvStatusTime;
    PhotoView iv_profile_pic;
    ImageView leftarrow,iv_download;
    long timeStamp= 0;
    boolean ifDownloaded=false;
    String downloadImagePath="";
    Uri myUri;
    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */






    static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %% ID: %d";
    static final String SCALE_TOAST_STRING = "Scaled to: %.2ff";
    static final String FLING_LOG_STRING = "Fling velocityX: %.2f, velocityY: %.2f";

    private PhotoView mPhotoView;


    private Toast mCurrentToast;

    private Matrix mCurrentDisplayMatrix = null;


    private static final String TAG = "Touch";
    @SuppressWarnings("unused")
    private static final float MIN_ZOOM = 10f,MAX_ZOOM = 10f;

    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 10f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_ui_profile);


        mContext=UIChatProfileActivity.this;
        tvStatusTime=(TextView)findViewById(R.id.tvStatusTime);
        tvUser=findViewById(R.id.tvNameUser);
        iv_profile_pic=findViewById(R.id.iv_profile_pic);
        leftarrow=findViewById(R.id.leftarrow);
        iv_download=findViewById(R.id.iv_download);
              // iv_profile_pic.setOnTouchListener(this);


        if (getIntent().getStringExtra("PATH") != null) {
            filePath = getIntent().getStringExtra("PATH");


            if (filePath != null&&!TextUtils.isEmpty(filePath)) {
                Picasso.with(mContext).load(filePath).noFade().into(iv_profile_pic);
                // myUri = Uri.parse(filePath);
               // Glide.with(mContext).load(myUri).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(iv_profile_pic);

            }
        }
        if (getIntent().getStringExtra("NAME") != null) {

            user_name = getIntent().getStringExtra("NAME");
            tvUser.setText(CommonUtils.NameCaps(user_name));

        }
        if (getIntent().getStringExtra("TIME") != null) {
            time = getIntent().getStringExtra("TIME");
            tvStatusTime.setText(time);
        }
        if (getIntent().getStringExtra("READSATUS") != null) {

            userstatus = getIntent().getIntExtra("READSATUS",0);
        }
        if(getIntent().getLongExtra("TIMESTAMP",0)!=0){
            timeStamp=getIntent().getLongExtra("TIMESTAMP",0);
        }

        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // BitmapDrawable drawable = (BitmapDrawable) iv_profile_pic.getDrawable();
                Bitmap bitmap = ((BitmapDrawable)iv_profile_pic.getDrawable()).getBitmap();
                if(bitmap!=null){ saveImage(bitmap);}else {
                    Util.snackBar("Please wait for image loading",iv_profile_pic);
                }
            }
        });

        iv_profile_pic.setOnMatrixChangeListener(new MatrixChangeListener());
        iv_profile_pic.setOnPhotoTapListener(new PhotoTapListener());
        iv_profile_pic.setOnSingleFlingListener(new SingleFlingListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (filePath != null&&!TextUtils.isEmpty(filePath)) {
            Picasso.with(mContext).load(filePath).noFade().into(iv_profile_pic);
           // myUri = Uri.parse(filePath);
           // Glide.with(mContext).load(myUri).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(iv_profile_pic);
            //   Picasso.with(context).load(path).into(binding.ivChat);

        }
        // iv_profile_pic.setImageBitmap(BitmapFactory.decodeFile(filePath));


    }

    void saveImage(Bitmap bitMapImg) {
        File filename;
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            new File(path + "/Satya Connect").mkdirs();
            filename = new File(path + "/Satya Connects/"+timeStamp+".jpg");
            FileOutputStream out = new FileOutputStream(filename);
            bitMapImg.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), filename.getAbsolutePath(), filename.getName(), filename.getName());

           Util.snackBar("Images Download Succesfully",iv_profile_pic);
            // Toast.makeText(mContext, "", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * Called when a touch event is dispatched to a view. This allows listeners to
     * get a chance to respond before the target view.
     *
    // * @param v    The view the touch event has been dispatched to.
     * @param event The MotionEvent object containing full information about
     *              the event.
     * @return True if the listener has consumed the event, false otherwise.
     */

    private float spacing(MotionEvent event)
    {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }


    public File createImageFile() throws IOException {
       // mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                    imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
       // mCurrentPhotoPath = image.getAbsolutePath();
        downloadImagePath=image.getAbsolutePath();
        return image;
    }




    /*
     * --------------------------------------------------------------------------
     * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
     * Description: calculates the midpoint between the two fingers
     * ------------------------------------------------------------
     */




    private class PhotoTapListener implements OnPhotoTapListener {

        @Override
        public void onPhotoTap(ImageView view, float x, float y) {
            float xPercentage = x * 100f;
            float yPercentage = y * 100f;

           // showToast(String.format(PHOTO_TAP_TOAST_STRING, xPercentage, yPercentage, view == null ? 0 : view.getId()));
        }
    }

    private void showToast(CharSequence text) {
        if (mCurrentToast != null) {
            mCurrentToast.cancel();
        }

        mCurrentToast = Toast.makeText(UIChatProfileActivity.this, text, Toast.LENGTH_SHORT);
        mCurrentToast.show();
    }

    private class MatrixChangeListener implements OnMatrixChangedListener {

        @Override
        public void onMatrixChanged(RectF rect) {
           // mCurrMatrixTv.setText(rect.toString());
        }
    }

    private class SingleFlingListener implements OnSingleFlingListener {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.d("PhotoView", String.format(FLING_LOG_STRING, velocityX, velocityY));
            return true;
        }
    }
}
