package co.intentservice.chatui.utils;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ProgressBar;

import co.intentservice.chatui.R;

public class Util {
    private  static ProgressBar ivProgress;

    public static void snackBar(String s, View v) {

        try {
            Snackbar snackbar=  Snackbar.make(v, s, Snackbar.LENGTH_LONG)
                    .setAction("Action", null);
            snackbar.getView().setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.link_blue));
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
